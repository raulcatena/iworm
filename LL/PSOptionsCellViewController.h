//
//  PSOptionsCellViewController.h
//  WormGUIDES
//
//  Created by Raul Catena on 6/6/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cell.h"
#import "GAITrackedViewController.h"

@protocol PanelProtocol;

@interface PSOptionsCellViewController : GAITrackedViewController

@property (nonatomic, assign) BOOL second;
@property (nonatomic, strong) Cell *cell;
@property (nonatomic, weak) id<PanelProtocol>delegate;

@property (nonatomic, strong) UIPopoverController *popover;


@end

@protocol PanelProtocol <NSObject>

-(void)optionsClickedSearchCell:(PSOptionsCellViewController *)optionsPanel;

@end