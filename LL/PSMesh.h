//
//  PSMesh.h
//  WormGUIDES
//
//  Created by Raul Catena on 5/30/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CellTime;

@interface PSMesh : NSObject

@property (nonatomic, strong) CellTime *cellTime;
@property (nonatomic, assign) BOOL hidden;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, assign) BOOL posible;
@property (nonatomic, assign) BOOL foundInSearch;

@property (nonatomic, assign) float x;
@property (nonatomic, assign) float y;
@property (nonatomic, assign) float z;
@property (nonatomic, assign) float scaleX;
@property (nonatomic, assign) float scaleY;
@property (nonatomic, assign) float scaleZ;

@end
