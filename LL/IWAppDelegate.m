/*
 *	IWAppDelegate.m
 *	iWorm
 *	
 *	Created by Raul Catena on 2/26/13.
 *	Copyright 2013 __MyCompanyName__. All rights reserved.
 */

#import "IWAppDelegate.h"
#import <CoreData/CoreData.h>


#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation IWAppDelegate

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

@synthesize window = _window, viewController = _viewController;

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)options
{
    nglGlobalColorFormat(NGLColorFormatRGBA);
    nglGlobalFlush();
    
	// Initializes the View Controller.
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
	{
		_viewController = [[IWViewController alloc] initWithNibName:@"IWViewController_iPhone" bundle:nil];
	}
	else
	{
        IWViewController *threeD = [[IWViewController alloc]initWithNibName:@"IWViewController_iPad" bundle:nil];
        if (WORM) {
            threeD.title = @"WormGUIDES";
        }
        if (FLY) {
            threeD.title = @"FlyGUIDES";
        }
        
        UINavigationController *navCon = [[UINavigationController alloc]initWithRootViewController:threeD];
        navCon.navigationBar.barStyle = UIBarStyleBlack;
        
		_viewController = threeD;
        _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        [_window setRootViewController:navCon];
        [_window makeKeyAndVisible];
        [threeD release];
        [navCon release];
        return YES;
	}
	
	// Initializes the UIWindow.
	_window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	[_window setRootViewController:_viewController];
	[_window makeKeyAndVisible];
    
    [self persistentStoreCoordinator];
	
	return YES;
}

- (void) applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state.
}

- (void) applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, etc.
	// At this point, NinevehGL will automatically stops the renders.
}

- (void) applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state.
}

- (void) applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive.
	// At this point, NinevehGL will resume the renders.
}

- (void) applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate.
}

#pragma mark -
#pragma mark Core Data stack

/*
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
    
    if (managedObjectContext_ != nil) {
        return managedObjectContext_;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext_ = [[NSManagedObjectContext alloc] init];
        [managedObjectContext_ setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext_;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel_ != nil) {
        return managedObjectModel_;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Worm" withExtension:@"momd"];
    managedObjectModel_ = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel_;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator_ != nil) {
        return persistentStoreCoordinator_;
    }
    
    BOOL alternativeWay = YES;
    if (alternativeWay) {
        //Alternative block
        NSError *error = nil;
        persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        
        //Block to copy DataBase from MainBundle to documents directory in first load
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSLog(@"paths %@", paths);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *storePath = [documentsDirectory stringByAppendingPathComponent:@"Worm.sqlite"];
        NSLog(@"Store path is %@", storePath);
        NSFileManager *fileManager = [NSFileManager defaultManager];
        // If the expected store doesn't exist, copy the default store.
        if (![fileManager fileExistsAtPath:storePath]) {
            NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:@"Worm" ofType:@"sqlite"];
            //NSString *defaultStorePath = nil;
            if (defaultStorePath) {
                NSLog(@"Started DB copy");
                [fileManager copyItemAtPath:defaultStorePath toPath:storePath error:NULL];
                NSLog(@"Finished DB copy");
            }
        }else{
            NSLog(@"File exists");
        }
        NSLog(@"Hello");
        NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
        
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
        
        if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
            // Update to handle the error appropriately.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            exit(-1);  // Fail
        }
        //[self addSkipBackupAttributeToItemAtURL:storeUrl];
        return persistentStoreCoordinator_;
    }else{
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Worm.sqlite"];
         
         NSError *error = nil;
         persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
         
         
         
         if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
         
         NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
         abort();
         }
         
         return persistentStoreCoordinator_;
    }
    
    
    
    
    
}


#pragma mark -
#pragma mark Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void) dealloc
{
	[_viewController release];
	[_window release];
	
	[super dealloc];
}

@end