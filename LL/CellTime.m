//
//  CellTime.m
//  iWorm
//
//  Created by Raul Catena on 5/1/13.
//
//

#import "CellTime.h"
#import "Cell.h"
#import "Time.h"


@implementation CellTime

@dynamic dead;
@dynamic linage;
@dynamic size;
@dynamic x;
@dynamic y;
@dynamic z;
@dynamic cell;
@dynamic time;

@end
