//
//  CellTime.h
//  iWorm
//
//  Created by Raul Catena on 5/1/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cell, TimeWorm;

@interface CellTime : NSManagedObject

@property (nonatomic, strong) NSNumber * dead;
@property (nonatomic, strong) NSString * linage;
@property (nonatomic, strong) NSNumber * size;
@property (nonatomic, strong) NSNumber * x;
@property (nonatomic, strong) NSNumber * y;
@property (nonatomic, strong) NSNumber * z;
@property (nonatomic, strong) Cell *cell;
@property (nonatomic, strong) TimeWorm *time;

@end
