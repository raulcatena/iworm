//
//  PSDataBaseUtility.m
//  WormGUIDES
//
//  Created by Raul Catena on 5/30/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import "PSDataBaseUtility.h"
#import "PSAppDelegate.h"
#import "PSSettingsSingleton.h"
#import "TimeWorm.h"
#import "Cell.h"
#import "CellTime.h"
#import "ColorScheme.h"
#import "Search.h"
#import "HTMLParser.h"
#import "HTMLNode.h"

#define MAXTIMEPOINT 400

@interface PSDataBaseUtility()

-(NSManagedObjectContext *)managedObjectContext;

@end

@implementation PSDataBaseUtility

#pragma mark database methods

-(NSManagedObjectContext *)managedObjectContext{
    PSAppDelegate *appDelegate = (PSAppDelegate *)[[UIApplication sharedApplication]delegate];
    return appDelegate.managedObjectContext;
}

-(void)createFlyDataBase{
    int cellInt = 1;
    TimeWorm *timeObject = [NSEntityDescription insertNewObjectForEntityForName:@"TimeWorm" inManagedObjectContext:self.managedObjectContext];
    timeObject.timePoint = [NSNumber numberWithInt:1];
    timeObject.timePoint_str = [NSString stringWithFormat:@"%i", 1];
    
    NSString *fileName = [NSString stringWithFormat:@"eNeuro_cellStatistics.csv"];
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSURL *url= [NSURL fileURLWithPath:path];
    NSError *error;
    NSString *allData = [[NSString alloc]initWithContentsOfURL:url encoding:NSStringEncodingConversionAllowLossy error:&error];
    allData = [allData stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //Parse the data
    if (allData != (id)[NSNull null]) {
        NSArray *array = [allData componentsSeparatedByString:@"\r"];
        
        int x = 0;
        for (NSString *cellTime in array) {
            if (x == 0){x++;continue;}else x++;
            
            NSArray *comps = [cellTime componentsSeparatedByString:@","];
            if (comps.count < 9) {
                continue;
            }
            if ([[comps objectAtIndex:1]intValue] == 0) {
                continue;
            }
            
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
            request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"uniqueID" ascending:YES]];
            request.predicate = [NSPredicate predicateWithFormat:@"name == %@", [comps objectAtIndex:0]];
            NSArray *results = [[self managedObjectContext]executeFetchRequest:request error:nil];
            Cell *cell;
            if (results.count == 0) {
                cell = [NSEntityDescription insertNewObjectForEntityForName:@"Cell" inManagedObjectContext:self.managedObjectContext];
                
                cell.name = [comps objectAtIndex:0];
                cell.uniqueID = [NSNumber numberWithInt:cellInt];
                cellInt++;
            }else{
                cell = [results lastObject];
            }
            CellTime *cellTime = [NSEntityDescription insertNewObjectForEntityForName:@"CellTime" inManagedObjectContext:self.managedObjectContext];
            cellTime.cell = cell;
            
            cellTime.x = [NSNumber numberWithFloat:[[comps objectAtIndex:1]floatValue]];
            cellTime.y = [NSNumber numberWithFloat:[[comps objectAtIndex:2]floatValue]];
            cellTime.z = [NSNumber numberWithFloat:[[comps objectAtIndex:3]floatValue]];
            cellTime.size = [NSNumber numberWithFloat:[[comps objectAtIndex:4]floatValue]];
            cellTime.time = timeObject;
        }
        
    }
    NSError *errorB;
    [self.managedObjectContext save:&errorB];
    if (error) {
        NSLog(@"Error %@", errorB.userInfo);
    }
}

-(void)createDataBase{
    if (FLY) {
        [self createFlyDataBase];
        return;
    }
    int cellInt = 1;
    for (int x = 1; x<MAXTIMEPOINT + 1; x++) {NSLog(@"in loop %i", x);
        TimeWorm *timeObject = [NSEntityDescription insertNewObjectForEntityForName:@"TimeWorm" inManagedObjectContext:self.managedObjectContext];
        timeObject.timePoint = [NSNumber numberWithInt:x];
        timeObject.timePoint_str = [NSString stringWithFormat:@"%i", x];
        NSString *time;
        if (x<10) {
            time = [NSString stringWithFormat:@"00%i",x];
        }else if (x <100){
            time = [NSString stringWithFormat:@"0%i", x];
        }else{
            time = [NSString stringWithFormat:@"%i",x];
        }
        NSString *fileName = [NSString stringWithFormat:@"t%@-nuclei", time];
        NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
        NSURL *url= [NSURL fileURLWithPath:path];
        NSError *error;
        NSString *allData = [[NSString alloc]initWithContentsOfURL:url encoding:NSStringEncodingConversionAllowLossy error:&error];
        allData = [allData stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        //Parse the data
        if (allData != (id)[NSNull null]) {
            NSArray *array = [allData componentsSeparatedByString:@"\n"];
            
            for (NSString *cellTime in array) {
                NSArray *comps = [cellTime componentsSeparatedByString:@","];
                if (comps.count < 8) {
                    //x++;
                    continue;
                }
                if ([[comps objectAtIndex:1]intValue] == 0) {
                    //NSLog(@"Invalid");
                    continue;
                }
                
                NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
                request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"uniqueID" ascending:YES]];
                request.predicate = [NSPredicate predicateWithFormat:@"name == %@", [comps objectAtIndex:9]];
                NSArray *results = [[self managedObjectContext]executeFetchRequest:request error:nil];
                Cell *cell;
                if (results.count == 0) {
                    cell = [NSEntityDescription insertNewObjectForEntityForName:@"Cell" inManagedObjectContext:self.managedObjectContext];
                    
                    cell.name = [comps objectAtIndex:9];
                    cell.uniqueID = [NSNumber numberWithInt:cellInt];
                    cellInt++;
                    cell.predecedor = [NSNumber numberWithInt:[[comps objectAtIndex:2]intValue]];
                    cell.sucessorOne = [NSNumber numberWithInt:[[comps objectAtIndex:3]intValue]];
                    cell.sucessorTwo = [NSNumber numberWithInt:[[comps objectAtIndex:4]intValue]];
                    cell.idTag = [NSNumber numberWithInt:[[comps objectAtIndex:0]intValue]];//In file only
                }else{
                    cell = [results lastObject];
                }
                CellTime *cellTime = [NSEntityDescription insertNewObjectForEntityForName:@"CellTime" inManagedObjectContext:self.managedObjectContext];
                cellTime.cell = cell;
                
                cellTime.x = [NSNumber numberWithFloat:[[comps objectAtIndex:5]floatValue]];
                cellTime.y = [NSNumber numberWithFloat:[[comps objectAtIndex:6]floatValue]];
                cellTime.z = [NSNumber numberWithFloat:[[comps objectAtIndex:7]floatValue]];
                cellTime.size = [NSNumber numberWithFloat:[[comps objectAtIndex:8]floatValue]];
                cellTime.time = timeObject;
            }

        }
        [self.managedObjectContext save:nil];
    }
}

-(void)emparentar{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"uniqueID" ascending:YES]];
    NSArray *results = [[self managedObjectContext]executeFetchRequest:request error:nil];
    NSLog(@"%lu cells", (unsigned long)results.count);
    
    for (Cell *cell in results) {
        NSMutableString *string = [cell.name mutableCopy];
        if (string.length > 0) {
            //////WARNING/[string deleteCharactersInRange:NSMakeRange(0, 1)];//Carefull with this method. Used in the past when white space before name. Can ruin search.
            cell.name = [string copy];
            [self.managedObjectContext save:nil];
        }
    }
    
    
    for (Cell *cell in results) {
        
        for (Cell *other in results) {
            if (other == cell) {
                continue;
            }
            if (other.successors.count == 2) {
                //NSLog(@"Pillao");
                continue;
            }
            if ([cell.name isEqualToString:@"MS"] || [cell.name isEqualToString:@"E"] ) {
                if ([other.name isEqualToString:@"EMS"]) {
                    cell.predecesor = other;[self.managedObjectContext save:nil];
                }
            }else if ([cell.name isEqualToString:@"Z2"] || [cell.name isEqualToString:@"Z3"] ) {
                if ([other.name isEqualToString:@"P4"]) {
                    cell.predecesor = other;[self.managedObjectContext save:nil];
                }
            }else if ([cell.name isEqualToString:@"D"] || [cell.name isEqualToString:@"P4"]) {
                if ([other.name isEqualToString:@"P3"]) {
                    cell.predecesor = other;[self.managedObjectContext save:nil];
                }
            }else if ([cell.name isEqualToString:@"C"] || [cell.name isEqualToString:@"P3"]) {
                if ([other.name isEqualToString:@"P2"]) {
                    cell.predecesor = other;[self.managedObjectContext save:nil];
                }
            }else{
                if (cell.name.length == other.name.length + 1) {
                    if ([cell.name hasPrefix:other.name]) {
                        cell.predecesor = other;
                        [self.managedObjectContext save:nil];
                        break;
                    }
                }
            }
        }
    }
}

-(void)addPartsListToDB{
    if (FLY) {
        return;
    }
    NSString *fileName = @"PartsList";
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSURL *url= [NSURL fileURLWithPath:path];
    NSError *error;
    NSString *allData = [[NSString alloc]initWithContentsOfURL:url encoding:NSStringEncodingConversionAllowLossy error:&error];
    NSArray *registries = [allData componentsSeparatedByString:@"+"];
    NSMutableArray *processedRegistries = [NSMutableArray arrayWithCapacity:registries.count];
    for (NSString *string in registries) {
        NSArray *compsTC = [string componentsSeparatedByString:@"\t"];
        NSMutableArray *mut = [NSMutableArray arrayWithCapacity:3];
        for (NSString *sub in compsTC) {
            [mut addObject:sub];
        }
        if (mut.count == 3) {
            [processedRegistries addObject:mut];
        }
    }
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"uniqueID" ascending:YES]];
    NSArray *results = [[self managedObjectContext]executeFetchRequest:request error:nil];
    NSLog(@"There are %lu cells", (unsigned long)results.count);
    
    int unfound = 0;
    int total = 0;
    int addedUnborn = 0;
    
    for (NSArray *array in processedRegistries) {
        NSString *systName = [array objectAtIndex:1];
        NSString *noDot = [systName stringByReplacingOccurrencesOfString:@"." withString:@""];
        
        BOOL found = NO;
        ;
        for (Cell *cell in results) {
            if([cell.name rangeOfString:noDot].length && cell.name.length == noDot.length){
                cell.descriptionOfCell = [array objectAtIndex:2];
                cell.nameProper = [array objectAtIndex:0];
                [self.managedObjectContext save:nil];
                NSLog(@"Cell found %@", cell.nameProper);
                found = YES;
                total++;
            }
        }
        
        //Unborn part in Partslist that need to add and hook to ancestor
        if(!found){
            Cell *cell = [NSEntityDescription insertNewObjectForEntityForName:@"Cell" inManagedObjectContext:self.managedObjectContext];
            cell.name = noDot;
            cell.nameProper = [array objectAtIndex:0];
            NSFetchRequest *requestOrphan = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
            request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"uniqueID" ascending:YES]];
            noDot = [noDot substringToIndex:noDot.length-1];
            requestOrphan.predicate = [NSPredicate predicateWithFormat:@"name == %@", noDot];
            NSArray *resultsOrphan = [[self managedObjectContext]executeFetchRequest:requestOrphan error:nil];
            if(resultsOrphan.count == 1){
                cell.predecesor = (Cell *)resultsOrphan.lastObject;
                cell.nameProper = [array objectAtIndex:0];
                cell.descriptionOfCell = [NSString stringWithFormat:@"%@ | NOT in this dataset",[array objectAtIndex:2]];
                addedUnborn++;
                for(Cell *cell in resultsOrphan){
                    NSLog(@"Ancest %@ (%@)for %@ frm %@ results %lu", cell.name, [array objectAtIndex:0], systName, noDot, (unsigned long)resultsOrphan.count);
                }
            }
            unfound++;
        }
    }
    [self.managedObjectContext save:nil];
}


-(void)conditionalDatabaseForV3{
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"conditionalDatabaseForV3"]boolValue]==YES) {
        return;
    }else{
        NSLog(@"___________REMOVING__________");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *storePath;
        if (WORM) {
            storePath = [documentsDirectory stringByAppendingPathComponent:@"Worm.sqlite"];
        }
        if (FLY) {
            storePath = [documentsDirectory stringByAppendingPathComponent:@"fly.sqlite"];
        }
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:storePath]) {
            NSError *error;
            [fileManager removeItemAtPath:storePath error:&error];
            if(error)NSLog(@"Error %@", error.userInfo);
            [fileManager copyItemAtPath:[[NSBundle mainBundle] pathForResource:WORM?@"Worm":@"fly" ofType:@"sqlite"] toPath:storePath error:&error];
            if(error)NSLog(@"Error B %@", error.userInfo);
        }
        [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithBool:YES] forKey:@"conditionalDatabaseForV2"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

-(void)formatCell:(UITableViewCell *)cell withSearch:(Search *)search{
    cell.textLabel.text = [search searchString];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    NSArray *colorComps = [[search color]componentsSeparatedByString:@","];
    cell.textLabel.textColor = [UIColor colorWithRed:[[colorComps objectAtIndex:0]floatValue] green:[[colorComps objectAtIndex:1]floatValue]  blue:[[colorComps objectAtIndex:2]floatValue] alpha:0.7];
    NSLog(@"type %@ searchIn %@", search.type, search.searchIn);
    NSString *type = nil;
    if (search.searchIn.intValue > 99) {
        type = @"Cell";
    }
    if ([search.searchIn isEqualToString:@"Gene"]) {
        type = @"Gene search";
    }
    if(search.searchIn.intValue == 10 || search.searchIn.intValue == 11 || search.searchIn.intValue == 110 || search.searchIn.intValue == 111){
        if (type.length > 0) {
            type = [NSString stringWithFormat:@"%@ + Successors", type];
        }else{
            type = @"Successors";
        }
    }
    if (search.searchIn.intValue == 1 || search.searchIn.intValue == 11 || search.searchIn.intValue == 111 || search.searchIn.intValue == 101){
        if (type.length > 0) {
            type = [NSString stringWithFormat:@"%@ + Ancestors", type];
        }else{
            type = @"Ancestors";
        }
        
    }
    
    NSString *where = nil;
    switch (search.type.intValue) {
        case 1:
            where = @"in Proper name";
            break;
        case 10:
            where = @"in Systematic";
            break;
        case 100:
            where = @"in Description";
            break;
        case 101:
            where = @"in Proper name and description";
            break;
        case 110:
            where = @"in Description and systematic";
            break;
        case 111:
            where = @"in All";
            break;
        case 11:
            where = @"in Proper and systematic names";
            break;
        default:
            where = @"";
            break;
    }
    if ([search.type hasPrefix:@"Gene"]) {
        where = @"Gene search";
    }
    if ([search.type isEqualToString:@"GeneFailed"]) {
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@", type, where];
    if ([where isEqualToString:@"Gene search"]) {
        cell.detailTextLabel.text = where;
    }
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1];
}


-(NSArray *)allCells{
    if(!_allCells){
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
        NSArray *allCells = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        _allCells = allCells;
    }
    return _allCells;
}

-(NSArray *)getAncestorsForCell:(Cell *)cell{
    NSMutableArray *array = [NSMutableArray array];
    Cell *cursor = cell.predecesor;
    while(cursor){
        if(![array containsObject:cursor])[array addObject:cursor];
        cursor = cursor.predecesor;
    }
    return [NSArray arrayWithArray:array];
}

-(NSArray *)getDescendantsForCell:(Cell *)cell{
    NSMutableArray *array = [NSMutableArray array];
    for (Cell *acell in [self allCells]) {
        BOOL found = NO;
        Cell* cursor = acell;
        while (found == NO) {
            if (cell == cursor.predecesor || [array containsObject:cursor.predecesor]) {
                found = YES;
                if (![array containsObject:acell]) {
                    [array addObject:acell];
                }
                break;
            }
            if (!cursor) {
                break;
            }
            cursor = cursor.predecesor;
        }
    }
    return [NSArray arrayWithArray:array];
}

-(NSArray *)executeSearchWithString:(NSString *)searchString{
    
    NSMutableArray *results = [NSMutableArray arrayWithCapacity:self.allCells.count];
    
    int propagationType = [[PSSettingsSingleton sharedInstance]propagationType];
    
    for (Cell *cell in self.allCells) {
        
        
        BOOL cellAdded = NO;
        
        if ([PSSettingsSingleton sharedInstance].searchProper == YES) {
            if (cell.nameProper.length > 1) {
                NSString *propName = cell.nameProper;
                
                NSComparisonResult result = [propName compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                        range:[propName rangeOfString:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)]];
                if (result == NSOrderedSame && searchString.length > 1) {
                    cellAdded = YES;
                }
            }
        }
        
        if ([PSSettingsSingleton sharedInstance].searchSystematic == YES  && !cellAdded) {
            
            if (cell.name.length > 0) {
                
                NSString *stringForRange;//Important. This trick makes the difference between the worm and fly search
                if (WORM) {
                    stringForRange = cell.name;
                }
                if (FLY) {
                    stringForRange = searchString;
                }
                
                NSComparisonResult resultExact = [cell.name compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                              range:[cell.name rangeOfString:stringForRange options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)]];
                if (resultExact == NSOrderedSame) {
                    cellAdded = YES;
                }
            }
        }
        
        if ([PSSettingsSingleton sharedInstance].searchDescription == YES  && !cellAdded) {
            if (cell.descriptionOfCell.length > 1) {
                NSString *descript = cell.descriptionOfCell;
                
                NSComparisonResult result = [descript compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                        range:[descript rangeOfString:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)]];
                if (result == NSOrderedSame && searchString.length > 1) {
                    cellAdded = YES;
                }
            }
        }
        
        if (cellAdded == YES){
            if (propagationType >99) {
                [results addObject:cell];
            }
        }
    }
    
    NSMutableArray *finalResults = [NSMutableArray arrayWithCapacity:self.allCells.count];
    [finalResults addObjectsFromArray:results];
    
    if(propagationType == 1 || propagationType == 11 || propagationType == 111 || propagationType == 101){
        for(Cell *cell in results){
            [finalResults addObjectsFromArray:[self getAncestorsForCell:cell]];
        }
    }
    if (propagationType == 110 || propagationType == 10 || propagationType == 111 || propagationType == 11) {
        
        for (Cell *cell in results) {
            [finalResults addObjectsFromArray:[self getDescendantsForCell:cell]];
        }
    }
    return finalResults;
}

-(void)errorConecting{
    
    [self.managedObjectContext save:nil];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error connecting" message:@"The connection failed, try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [[self delegate]doneSearching];
}

-(void)searchWithSearch:(Search *)search andColor:(UIColor *)color andAlert:(UIAlertView *)alert{
    //Make check of gene name
    NSString *url = [NSString stringWithFormat:@"http://www.wormbase.org/db/get?name=%@;class=gene", search.searchString];
        NSLog(@"Call to url %@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    request.timeoutInterval = 10.0f;
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        if (error) {
            search.searchIn = @"GeneFailed";
            [self errorConecting];
        }else{
            NSString *result = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSArray *split = [result componentsSeparatedByString:@"wname=\"expression\""];
            if (split.count >= 2) {
                NSArray *restAddress = [[split objectAtIndex:1]componentsSeparatedByString:@"href=\""];
                if (restAddress.count >= 2) {
                    NSArray *finalAddress = [[restAddress objectAtIndex:1]componentsSeparatedByString:@"\"><span"];
                    if (finalAddress.count >=2) {
                        
                        NSString *urlFinal = [NSString stringWithFormat:@"http://www.wormbase.org%@", [finalAddress objectAtIndex:0]];
                        NSLog(@"La url final es %@", urlFinal);
                        
                        NSMutableURLRequest *requestFinal = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlFinal]];
                        
                        [NSURLConnection sendAsynchronousRequest:requestFinal queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *responseFinal, NSData *dataFinal_, NSError *errorFinal){
                            if (error) {
                                search.searchIn = @"GeneFailed";
                                [self errorConecting];
                            }
                            if (color) {
                                CGColorRef colorRef = [color CGColor];
                                
                                int numComponents = (int)CGColorGetNumberOfComponents(colorRef);
                                
                                if (numComponents == 4)
                                {
                                    const CGFloat *components = CGColorGetComponents(colorRef);
                                    CGFloat red = components[0];
                                    CGFloat green = components[1];
                                    CGFloat blue = components[2];
                                    CGFloat alpha = components[3];
                                    search.color = [NSString stringWithFormat:@"%f,%f,%f,%f", red, green, blue, alpha];
                                }
                            }
                            
                            
                            NSString *dataString = [[NSString alloc]initWithData:dataFinal_ encoding:NSUTF8StringEncoding];
                            
                            NSError *error;
                            HTMLParser *parser = [[HTMLParser alloc] initWithString:dataString error:&error];
                            HTMLNode *bodyNode = [parser body];
                            
                            NSArray *ulList = [bodyNode findChildrenWithAttribute:@"class" matchingName:@"anatomy_term-link" allowPartial:NO];
                            NSMutableSet *set = [NSMutableSet setWithCapacity:2000];
                            if (ulList.count > 0) {
                                NSArray *allCells = [self allCells];
                                for (HTMLNode *node in ulList) {
                                    for (Cell *cell in allCells) {
                                        if ([cell.name.lowercaseString isEqualToString:node.contents.lowercaseString] || [cell.nameProper.lowercaseString isEqualToString:node.contents.lowercaseString]) {
                                            [set addObject:cell];
                                            if([[PSSettingsSingleton sharedInstance]searchAncestors]){
                                                [set addObjectsFromArray:[self getAncestorsForCell:cell]];
                                            }
                                            if([[PSSettingsSingleton sharedInstance]searchDescendants]){
                                                [set addObjectsFromArray:[self getDescendantsForCell:cell]];
                                            }
                                            break;
                                        }
                                    }
                                }
                                NSLog(@"Found %lu", (unsigned long)set.count);
                                search.searchIn = @"Gene";//Restitue in case it was GeneFailed
                                search.cells = set;
                                
                                [self.delegate doneSearching];
                                
                                if (search.cells.count == 0) {
                                    [self.managedObjectContext deleteObject:search];
                                }else{
                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"GENE_SEARCH_DONE" object:search];
                                }
                                [self.managedObjectContext save:nil];
                                return;
                            }
                        }];
                    }
                }
            }
        }

    }];
}


-(void)doGeneSearchWithTermToRule:(Search *)search withColor:(UIColor *)color{
    [self searchWithSearch:search andColor:color andAlert:nil];
}

@end
