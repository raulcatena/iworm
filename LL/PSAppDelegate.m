//
//  PSAppDelegate.m
//  LL
//
//  Created by Raul Catena on 5/26/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import "PSAppDelegate.h"

#import "PSViewController.h"
#import "GAI.h"

@implementation PSAppDelegate


-(void)startGoogleAnalytics{
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 2;
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-45541818-4"];
}

-(void)testURL{//This become the unit tests for scene generation
    //This are different test cases, from 2nd on are generated in Android as of 27 of november, 2013 and should work
    //NSString *test = @"wormguides://wormguides/testurlscript?/set/ABa-d-s-n%3E+%23ffff0000/ABp%3E$+%23ff00ff00/C%3E+%23ffffffff/caapap%3C%3E+%23ff7f007f/D%3E+%23ffb2b2bf/E%3E+%23ffffff00/MS%3E+%23ff0000ff/view/time=-1/rX=0.400000/rY=0.400000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=0.800000/dim=0.300000/";
    //NSString *test = @"wormguides://wormguides/testurlscript?/set/ABp-d-s-n%3E$+%23ff00ff00/ABa-d-s-n%3E$+%23ffff0000/view/time=100/rX=0.400000/rY=0.400000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=0.800000/dim=0.300000/";
    //NSString *test = @"wormguides://wormguides/testurlscript?/set/abala&lt;&gt;/view/time=0/rX=-0.057555404/rY=-0.3003072/rZ=1.1201156/tX=0.0/tY=0.0/scale=1.0/dim=0.5/showLinCol=true/";
    //NSString *test = @"wormguides://wormguides/testurlscript?/set/rmed-n&lt;&gt;+%23ff0fc20f/rmel-n&lt;&gt;+%23ff7011e1/rmer-n&lt;&gt;+%23ff11e1e1/rmev-n&lt;&gt;+%23fff09380/ala-n&lt;&gt;+%23fff2fa0e/can-n&lt;&gt;+%23fffa0ef8/pharyn-d&lt;&gt;+%23fff40202/pharyn-d&gt;+%23fff40202/int-n&gt;+%23ff643210/view/time=0/rX=0.018687425/rY=-0.12167001/rZ=-0.12679914/tX=-19.272491/tY=-2.2816772/scale=1.0000000/dim=0.17396009/";
    NSString *test = @"wormguides://wormguides/testurlscript?/set/rmed-n&lt;$&gt;+%23ff0fc20f/rmel-n&lt;$&gt;+%23ff7011e1/rmer-n&lt;$&gt;+%23ff11e1e1/rmev-n&lt;$&gt;+%23fff09380/ala-n&lt;$&gt;+%23fff2fa0e/can-n&lt;$&gt;+%23fffa0ef8/pharyn-d&lt;$&gt;+%23fff40202/pharyn-d&gt;$+%23fff40202/int-n&gt;$+%23ff643210/view/time=0/rX=0.018687425/rY=-0.12167001/rZ=-0.12679914/tX=-19.272491/tY=-2.2816772/scale=1.0000000/dim=0.17396009/";
    //NSString *test = @"wormguides://wormguides/testurlscript?/set/abala&lt;$&gt;/view/time=0/rX=-0.057555404/rY=-0.3003072/rZ=1.1201156/tX=0.0/tY=0.0/scale=1.0/dim=0.5/showLinCol=true/iOS/";
    //NSString *test = @"wormguides://wormguides/testurlscript?/set/pha-4-g/view/time=0/rX=-0.057555404/rY=-0.3003072/rZ=1.1201156/tX=0.0/tY=0.0/scale=1.0/dim=0.5/showLinCol=true/iOS/";
    //NSString *test = @"wormguides://wormguides/testurlscript?/set/ABa-s$%3E+%23ffff0000/ABp-s$%3E+%23ff00ff00/amphid%20sheath-d$%3E+%23ffff0000/C-s%3C$%3E+%23ffffff00/D-s$%3E+%23ffb1b1bf/E-s$%3E+%23ffffff00/MS-s$%3E+%23ff0000ff/pha-4-g+%23ffffffff/view/time=269/rX=-0.017500/rY=-0.425000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=1.000000/dim=0.500000/iOS/";
    //NSString *test = @"http://scene.wormguides.org/wormguides/testurlscript?/set/canl-n%3C$%3E+%23ffffffff/muscle-d$+%23ff33cc00/intestinal-d$+%23ffffccff/marginal-d$%3E+%23ffcc33cc/vulval-d$+%23ff9999ff/germline-d$+%23ff003366/uterus-d$+%23ff003366/spermatheca-d$+%23ff003366/interneuron-d$+%23ffff3300/motor-d$+%23ff9966cc/sensory-d$+%23ffff66cc/polymodal-d$+%23ffcc0033/pore-d$+%23ffecf78f/duct-d$+%23ffcc9966/gland-d$+%23ff9999cc/excretory-d$+%23ffcccc99/hypoderm-d$+%23ffdcc3ac/seam-d$+%23ffcc6633/socket-d$+%23ffff9999/sheath-d$+%23ff339999/valve-d$+%23ff996633/arcade-d$%23ffcccc00/epithelium-d$%23ff996699/rectal-d$+%23ff996666/gland-d$%23ffccccff/pseudocoelomic-d$+%23ffffffcc/basement-d$+%23ffff9900/glr-d$+%23ffcc9900/coeloemocyte-d$+%23ffffcc00/head-d$+%23ffff6633/view/time=269/rX=0.000000/rY=170.500000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=1.000000/dim=0.500000/Internet%20browser/";
    //NSString *test = @"http://scene.wormguides.org/wormguides/testurlscript?/set/pha-4-g$+%23ffffffff/view/time=269/rX=0.000000/rY=0.000000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=1.000000/dim=0.500000/iOS/";
    //NSString *test = @"http://scene.wormguides.org/wormguides/testurlscript?/set/canl-n%3C$%3E+%23ffffffff/muscle-d$+%23ff33cc00/intestinal-d$+%23ffffccff/marginal-d$%3E+%23ffcc33cc/vulval-d$+%23ff9999ff/germline-d$+%23ff003366/uterus-d$+%23ff003366/spermatheca-d$+%23ff003366/interneuron-d$+%23ffff3300/motor-d$+%23ff9966cc/sensory-d$+%23ffff66cc/polymodal-d$+%23ffcc0033/pore-d$+%23ffecf78f/duct-d$+%23ffcc9966/gland-d$+%23ff9999cc/excretory-d$+%23ffcccc99/hypoderm-d$+%23ffdcc3ac/seam-d$+%23ffcc6633/socket-d$+%23ffff9999/sheath-d$+%23ff339999/valve-d$+%23ff996633/arcade-d$%23ffcccc00/epithelium-d$%23ff996699/rectal-d$+%23ff996666/gland-d$%23ffccccff/pseudocoelomic-d$+%23ffffffcc/basement-d$+%23ffff9900/pha-4-g$+%23ffffffff/glr-d$+%23ffcc9900/coeloemocyte-d$+%23ffffcc00/head-d$+%23ffff6633/view/time=269/rX=0.000000/rY=0.000000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=1.000000/dim=0.500000/iOS/";
    //NSString *test = @"http://scene.wormguides.org/wormguides/testurlscript?/set/canl-n%3C$%3E+%23ffffffff/muscle-d$+%23ff33cc00/intestinal-d$+%23ffffccff/marginal-d$%3E+%23ffcc33cc/vulval-d$+%23ff9999ff/germline-d$+%23ff003366/uterus-d$+%23ff003366/spermatheca-d$+%23ff003366/interneuron-d$+%23ffff3300/motor-d$+%23ff9966cc/sensory-d$+%23ffff66cc/polymodal-d$+%23ffcc0033/pore-d$+%23ffecf78f/duct-d$+%23ffcc9966/gland-d$+%23ff9999cc/excretory-d$+%23ffcccc99/hypoderm-d$+%23ffdcc3ac/seam-d$+%23ffcc6633/socket-d$+%23ffff9999/sheath-d$+%23ff339999/valve-d$+%23ff996633/arcade-d$%23ffcccc00/epithelium-d$%23ff996699/rectal-d$+%23ff996666/gland-d$%23ffccccff/pseudocoelomic-d$+%23ffffffcc/basement-d$+%23ffff9900/pha-4+%23ffffffff/glr-d$+%23ffcc9900/coeloemocyte-d$+%23ffffcc00/head-d$+%23ffff6633/view/time=269/rX=0.000000/rY=0.000000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=1.000000/dim=0.500000/iOS/";
    //NSString *test = @"http://scene.wormguides.org/wormguides/testurlscript?/set/pha-4&lt;&gt;/view/time=214/rX=0.67040014/rY=-0.3298003/rZ=0.41617486/tX=-2.2816772/tY=0.0/scale=1.0/dim=0.33788902/showLinCol=true/";
    //NSString *test = @"flyguides://flyguides/testurlscript?/set/sp-d$+%23ffff7e00/view/time=1/rX=0.000000/rY=0.000000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=0.772602/dim=0.800000/iOS/";
    //NSString *test = @"wormguides://wormguides/testurlscript?/set/ABa-d-n$+%23ffff0000/ABa-d-n-s$%3E+%23ffffff00/ABa-d-n-s$%3E+%23ff0000ff/ABa-d-s$%3E+%23ff7f7f7f/ABp-d-s$%3E+%23ffffffff/ABp-d-s$%3E+%23ff00ff00/view/time=347/rX=0.000000/rY=0.000000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=1.042104/dim=0.500000/iOS/";
    //NSString *test = @"flyguides://flyguides/testurlscript?/set/aa-s-n$+%23ff00ff00/sp-s$+%23ffff7e00/view/time=0/rX=0.000000/rY=0.000000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=1.000000/dim=0.500000/iOS/";
    
    [_urlHandler decomposeURL:[NSURL URLWithString:test] toURLHandler:_urlHandler];
}

//Method that is called at the beggining after app launch
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    if (!_urlHandler) {
        _urlHandler = [[URLHandler alloc]init];
    }
    
    //[self testURL];//Method only for quick testing of URLs
    
    self.viewController = [[PSViewController alloc]initWithNibName:@"PSViewController" bundle:nil andURLHandler:_urlHandler];
    
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
	{
        if (WORM) {
            self.viewController.title = @"WormGUIDES";
        }
        if (FLY) {
            self.viewController.title = @"FlyGUIDES";
        }
        
    }
    self.viewController.view.frame = self.window.frame;
    
    UINavigationController *navCon = [[UINavigationController alloc]initWithRootViewController:self.viewController];
    navCon.navigationBar.barStyle = UIBarStyleBlack;
    
    self.window.rootViewController = navCon;
    [self.window makeKeyAndVisible];
    
    [self startGoogleAnalytics];
    return YES;
}

-(void)fireTimer{
    [self.viewController loadScene:_urlHandler];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    if (![_urlHandler isValidURL:url]) {
        return NO;
    }
    [_urlHandler decomposeURL:url toURLHandler:_urlHandler];
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(fireTimer) userInfo:nil repeats:NO];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark Core Data stack


- (NSManagedObjectContext *)managedObjectContext {
    
    if (managedObjectContext_ != nil) {
        return managedObjectContext_;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext_ = [[NSManagedObjectContext alloc] init];
        [managedObjectContext_ setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext_;
}


- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel_ != nil) {
        return managedObjectModel_;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Worm" withExtension:@"momd"];
    managedObjectModel_ = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel_;
}


- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator_ != nil) {
        return persistentStoreCoordinator_;
    }
    
    BOOL alternativeWay = YES;//Alternative loads preset database from mainbundle. If alternativeWay = NO, it tries to create database (see method stack in initialGUIsetup of PSViewController;//Always ship the app with alternativeWay = YES;
    if (alternativeWay) {
        //Alternative block
        NSError *error = nil;
        persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        
        //Block to copy DataBase from MainBundle to documents directory in first load
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *storePath;
        if (WORM) {
            storePath = [documentsDirectory stringByAppendingPathComponent:@"Worm.sqlite"];
        }
        if (FLY) {
            storePath = [documentsDirectory stringByAppendingPathComponent:@"fly.sqlite"];
        }
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if (![fileManager fileExistsAtPath:storePath]) {
            NSString *defaultStorePath;
            if (WORM)defaultStorePath = [[NSBundle mainBundle] pathForResource:@"Worm" ofType:@"sqlite"];
            if (FLY) defaultStorePath = [[NSBundle mainBundle] pathForResource:@"fly" ofType:@"sqlite"];
            if (defaultStorePath)[fileManager copyItemAtPath:defaultStorePath toPath:storePath error:NULL];
        }
        NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
        
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
        
        if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
            [fileManager removeItemAtPath:storePath error:&error];
            [fileManager copyItemAtPath:[[NSBundle mainBundle] pathForResource:WORM?@"Worm":@"fly" ofType:@"sqlite"] toPath:storePath error:&error];
        }
        return persistentStoreCoordinator_;
    }else{
        NSURL *storeURL;
        if (WORM) {
            storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Worm.sqlite"];
        }
        if (FLY) {
            storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"fly.sqlite"];
        }
        NSError *error = nil;
        persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        
        
        
        if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
            
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        
        return persistentStoreCoordinator_;
    }
}


#pragma mark -
#pragma mark Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
