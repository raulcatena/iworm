//
//  PSViewController.m
//  LL
//
//  Created by Raul Catena on 5/26/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import "PSViewController.h"
#import "sphere.h"
#import "AGLKVertexAttribArrayBuffer.h"
#import "AGLKContext.h"
#import "ColorScheme.h"
#import "CellTime.h"
#import "Cell.h"
#import "TimeWorm.h"
#import "Search.h"
#import "PSAppDelegate.h"
#import "InfoViewcontrollerViewController.h"
#import "LegendViewController.h"
#import "LineageTracing.h"
#import "SearchController.h"
#import "PSMesh.h"
#import "PSDataBaseUtility.h"
#import "PSSceneGenerator.h"
#import "PSVideoGenerator.h"
#import <QuartzCore/QuartzCore.h>
#import <Twitter/Twitter.h>


#define BUFFER_OFFSET(i) ((char *)NULL + (i))
//#define MAXTIMEPOINT 270
#define LABELWIDTH 180
#define LABELHEIGHT 25
#define WIDTH_LEGEND 300
#define X_PROP 10.0f;//20
#define X_CORRECT -28.0f;//-18
#define Y_PROP 10.0f;//20
#define Y_CORRECT -14.0f;//-14
#define Z_PROP 1.5.0f;//3
#define Z_CORRECT -15.0f;//-10

// Uniform index.
enum
{
    UNIFORM_MODELVIEWPROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_NORMAL,
    NUM_ATTRIBUTES
};



//http://iphonedevelopment.blogspot.com/2009/05/procedural-spheres-in-opengl-es.html
GLfloat gCubeVertexData[216] =
{
    // Data layout for each line below is:
    // positionX, positionY, positionZ,     normalX, normalY, normalZ,
    
    0.5f, -0.5f, -0.5f,        1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,          1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    
    0.5f, 0.5f, -0.5f,         0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 1.0f, 0.0f,
    
    -0.5f, 0.5f, -0.5f,        -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        -1.0f, 0.0f, 0.0f,
    
    -0.5f, -0.5f, -0.5f,       0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         0.0f, -1.0f, 0.0f,
    
    0.5f, 0.5f, 0.5f,          0.0f, 0.0f, 1.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, 0.0f, 1.0f,
    
    0.5f, -0.5f, -0.5f,        0.0f, 0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 0.0f, -1.0f
};

@interface PSViewController () {
    GLuint _program;
    
    GLKMatrix4 _modelViewProjectionMatrix;
    GLKMatrix3 _normalMatrix;
    CGPoint _rotation;
    //-//new
    GLKMatrix4 _rotMatrix;
    
    GLuint _vertexArray;
    GLuint _vertexBuffer;
    
    //Inherited from iWorm with Nineveh
    UIView *transf;
    IBOutlet UIView *transpar;
    
    float distance;
    CGPoint position;
    CGPoint translation;
    
    NSMutableDictionary *dataComponentsArray;
    NSArray *dataOfMObjectsInTime;
    NSArray *allMOObjects;
    int count;
    float dimOthers;
    
    //Adds for size
    int vertCount;
    float sphereVertsAdj [400][1944*3];//Here put the maximum of all species in all apps
    
    UILabel *labelTouchedCell;
    
    BOOL respondingToTap;
    BOOL initial;
    
    NSTimer *timer;
    NSTimer *timerLoading;
    
    BOOL userInteraction;
    
    float scaleFactorDevice;//Will have to calculate this automatically RCF TODO
    
    NSArray *allCells;
    
    int maxTimePoint;
    
    float **rawData;
}

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;


@property (strong, nonatomic) PSDataBaseUtility *database;

- (void)setupGL;
- (void)tearDownGL;

@end

@implementation PSViewController

@synthesize points = _points;
@synthesize _group;
@synthesize sliderTime;
@synthesize toolbar;
@synthesize nuclei;
@synthesize timePointLabel;
@synthesize individualSearch;
@synthesize severalSearches;

@synthesize legend;
@synthesize loaderRequest;

@synthesize urlHandler;
@synthesize selectedMesh = _selectedMesh;

@synthesize wheelerView = _wheelerView;

#pragma mark init and memory management

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andURLHandler:(URLHandler *)handler{
    self = [self initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.urlHandler = handler;
        if (WORM) {
            maxTimePoint = 400;
        }
        if (FLY) {
            maxTimePoint = 1;
        }
        self.database = [[PSDataBaseUtility alloc]init];
    }
    return self;
}

- (void)dealloc
{
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
    
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"________\n_________\n__________");
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load methods


-(void)adjustIOS7{
    if ([[[[[UIDevice currentDevice] systemVersion]
           componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

-(void)addRecognizers{
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    UILongPressGestureRecognizer *longRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLong:)];
    [self.view addGestureRecognizer:longRecognizer];
    longRecognizer.delegate = self;
    longRecognizer.minimumPressDuration = 0.5;
    
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [self.view addGestureRecognizer:pinchRecognizer];
}

-(void)setUpScaleFactorDevice{
    if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        scaleFactorDevice = 20;
    }else{
        scaleFactorDevice = 9;
    }
}

-(void)showSpinner{
    if (self.urlHandler.searchArray.count > 0) {
        _wheelerView.center = self.view.center;
        //_wheelerView.frame = CGRectOffset(_wheelerView.frame, 0, -200);
        _wheelerView.layer.cornerRadius = 10.0f;
        _wheelerView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_wheelerView];
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.frame = CGRectMake(0, 0, 90, 90);
        [spinner startAnimating];
        spinner.center = CGPointMake(_wheelerView.bounds.size.width / 2, _wheelerView.bounds.size.height /2);
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, _wheelerView.bounds.size.width - 20, 20)];
        [label setText:@"Loading scene..."];
        [_wheelerView addSubview:label];
        [_wheelerView addSubview:spinner];
    }
}

-(PSSettingsSingleton *)searchSettings{
    if (!_searchSettings) {
        _searchSettings = [PSSettingsSingleton sharedInstance];
        [_searchSettings defaults];
    }
    return _searchSettings;
}

-(void)subscribeToNotifications{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(geneSearchDone:) name:@"GENE_SEARCH_DONE" object:nil];
}

-(void)geneSearchDone:(NSNotification *)notification{
    
    _searchText = nil;
    
    Search *search = (Search *)notification.object;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Save search rule?" message:[NSString stringWithFormat:@"%lu cells have been found", (unsigned long)search.cells.count] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        [self.managedObjectContext deleteObject:search];
        [self.managedObjectContext save:nil];
    }];
    [alertController addAction:no];
    UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:yes];
    if (!alertController) {
        NSLog(@"NO controller");
        return;
        
    }
    
    //This only works in iOS8
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        [self.popover.contentViewController presentViewController:alertController animated:YES completion:nil];
    }else{
        [self.presentedViewController presentViewController:alertController animated:YES completion:nil];
    }
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self adjustIOS7];
    
    if (!self.sliderTime) {
        [self initialGUISetUp];
    }
    
    [self startOpenGLElements];
    [self setupGL];
    
    [self addRecognizers];
    
    count = maxTimePoint;
    
    [self setUpScaleFactorDevice];
    [self searchSettings];
    
    self.wheelerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 100)];
    
    [self subscribeToNotifications];
}


-(void)loadScene:(URLHandler *)aHandler{
    
    [self showSpinner];

    NSArray *sceneResults = [self.urlHandler loadScene:self.urlHandler];
    
    if (_wheelerView) {
        [_wheelerView removeFromSuperview];
    }
    
    if (!sceneResults) {
        return;
    }
    ColorScheme *colorScheme = [sceneResults lastObject];
    if ([colorScheme isMemberOfClass:[colorScheme class]]) {
        if (colorScheme.searches.count > 0) {
            self.currentColorScheme.isCurrent = [NSNumber numberWithBool:NO];
            colorScheme.isCurrent = [NSNumber numberWithBool:YES];
            self.currentColorScheme = colorScheme;
            [self.managedObjectContext save:nil];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"New Theme" message:@"Please name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            alert.tag = 10;
            [alert show];
        }else{
            [[[UIAlertView alloc]initWithTitle:@"Error" message:@"The scene couldn't be generated" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
    }
    
    if (sceneResults.count > 6) {
        CGPoint rotate = CGPointMake([[sceneResults objectAtIndex:0]floatValue], [[sceneResults objectAtIndex:1]floatValue]);
        [self applyRotationWithCGPoint:rotate];
        //z will go here
        distance = ([[sceneResults objectAtIndex:3]floatValue]-1)*60;
        translation.x = [[sceneResults objectAtIndex:6]floatValue];
        translation.y = [[sceneResults objectAtIndex:7]floatValue];
        if(WORM)self.sliderTime.value = (float)[[sceneResults objectAtIndex:4]intValue]+1;
        dimOthers = [[sceneResults objectAtIndex:5]floatValue];
        [self reloadWorm:self.sliderTime];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self loadScene:self.urlHandler];
    [self refresher];
}

-(void)refresher{
    userInteraction = YES;
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(killStarter) userInfo:nil repeats:NO];
}

-(void)killStarter{
    userInteraction = NO;
}

-(NSManagedObjectContext *)managedObjectContext{
    PSAppDelegate *appDelegate = (PSAppDelegate *)[[UIApplication sharedApplication]delegate];
    return appDelegate.managedObjectContext;
}

#pragma mark View Init

-(void)setBarItems{
    //Setting all the navigation buttons
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spacer.width = 10;
    
    UIBarButtonItem *themes = [[UIBarButtonItem alloc]initWithTitle:@"Themes" style:UIBarButtonItemStyleBordered target:self action:@selector(action:)];
    
    UIBarButtonItem *envBBI = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(sendScreen:)];
    
    UIBarButtonItem *camBBI = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(saveScreen)];
    
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:themes, spacer, envBBI, spacer, camBBI, nil];
    
    UIBarButtonItem *magGlassBBI = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(search:)];
    
    UIButton *infoBut = [UIButton buttonWithType:UIButtonTypeInfoLight];
    infoBut.tintColor = [UIColor whiteColor];
    [infoBut addTarget:self action:@selector(goToInfo:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *info = [[UIBarButtonItem alloc]initWithCustomView:infoBut];
    UIButton *paletteBut = [UIButton buttonWithType:UIButtonTypeCustom];
    
    NSString *paletteString = @"palette.png";
    
    UIImage *paletteImg = [UIImage imageNamed:paletteString];
    paletteBut.bounds = CGRectMake(0, 0, 30, 30);
    [paletteBut setImage:paletteImg forState:UIControlStateNormal];
    [paletteBut addTarget:self action:@selector(goToPalette:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *palette = [[UIBarButtonItem alloc]initWithCustomView:paletteBut];
    
    UIBarButtonItem *question = [[UIBarButtonItem alloc]initWithTitle:@"?" style:UIBarButtonItemStyleBordered target:self action:@selector(help)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:question, spacer, spacer, info, spacer, spacer, palette, spacer, magGlassBBI, nil];
    
    for (UIBarButtonItem *item in self.navigationItem.leftBarButtonItems) {
        item.tintColor = [UIColor whiteColor];
    }
    for (UIBarButtonItem *item in self.navigationItem.rightBarButtonItems) {
        item.tintColor = [UIColor whiteColor];
    }
}

-(void)initTheTimeBar{
    NSMutableArray *items = [NSMutableArray array];//[[self.toolbar.items mutableCopy]autorelease];
    UIButton *forward = [UIButton buttonWithType:UIButtonTypeCustom];
    [forward setImage:[UIImage imageNamed:@"forward.png"] forState:UIControlStateNormal];
    [forward setFrame:CGRectMake(0, 0, 30, 30)];
    [forward addTarget:self action:@selector(forward:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *fBarButton = [[UIBarButtonItem alloc]initWithCustomView:forward];
    [items insertObject:fBarButton atIndex:0];
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    [back setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [back setFrame:CGRectMake(0, 0, 30, 30)];
    [back addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *bBarButton = [[UIBarButtonItem alloc]initWithCustomView:back];
    [items insertObject:bBarButton atIndex:0];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [items insertObject:flex atIndex:1];
    UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    [bt setFrame:CGRectMake(0, 0, 30, 30)];
    [bt addTarget:self action:@selector(playMovie:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:bt];
    [items insertObject:barButton atIndex:0];
    
    self.toolbar.items = items;
    
    
    self.sliderTime = [[UISlider alloc]initWithFrame:CGRectMake(bt.bounds.size.width * 3, self.view.bounds.size.height - 30, self.view.frame.size.width - bt.bounds.size.width * 4.7f, 20)];
    self.sliderTime.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth );
    [self.view addSubview:self.sliderTime];
    sliderTime.minimumValue = 1;
    sliderTime.maximumValue = maxTimePoint;
    [sliderTime addTarget:self action:@selector(reloadWorm:) forControlEvents:(UIControlEventValueChanged | UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    [sliderTime addTarget:self action:@selector(sliderFinished) forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    [sliderTime addTarget:self action:@selector(sliderBegan) forControlEvents:UIControlEventValueChanged];
    UIImage *stetchLeftTrack = [[UIImage imageNamed:@"redslider.png"]
                                stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    [sliderTime setMinimumTrackImage:stetchLeftTrack forState: UIControlStateNormal];
    sliderTime.value = maxTimePoint;
    
}

-(void)setTheFadeSlider{
    dimOthers = 0.8;
    if (WORM) {
        timePointLabel.text = [NSString stringWithFormat:@"%i minutes", (int)(sliderTime.value)];//each time point is one minute. Add correction if necessary
    }
    if (FLY) {
        timePointLabel.hidden = YES;
    }
    
}

-(void)checkAndSetCurrentScheme{
    //Set the current color scheme
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ColorScheme"];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    request.predicate = [NSPredicate predicateWithFormat:@"isCurrent = YES"];
    request.sortDescriptors = [NSArray arrayWithObject:sort];
    NSArray *array = [self.managedObjectContext executeFetchRequest:request error:nil];
    NSLog(@"Found %lu colorSchemes", (unsigned long)array.count);
    self.currentColorScheme = (ColorScheme *)array.lastObject;
}

-(void)databaseInitializations{
    
    //------------Load presets-------------------//
    //[self createBlankTheme];
    //[self createPresetTheme];
    
    //-------Methods called to create DB---------//Use only when alternative NO in persistentStoreCoordinator getter of App delegate
    
    /*
     [[[PSDataBaseUtility alloc]init]createDataBase];
     [[[PSDataBaseUtility alloc]init]emparentar];
     [[[PSDataBaseUtility alloc]init]addPartsListToDB];
     //[[[PSDataBaseUtility alloc]init]conditionalDatabaseForV1_7];//This method is for support of Parts list search of unborn parts from v1.7 on for all users updating from earlier versions. Future version will create a new database while conserving the colorschemes.//*/
    [[[PSDataBaseUtility alloc]init]conditionalDatabaseForV3];
    
    //-------Alternative methods to init---------//
    //[self compileInitialMesh];
    //[self loadWorm:count];
    //[self loadAllCellsFromSQLite];
}

-(void)initialGUISetUp{
    self.view.multipleTouchEnabled = YES;
    //set the bar
    [self setBarItems];
    //Set the current color scheme
    [self checkAndSetCurrentScheme];
    //Init the time slider and time controllers
    if(WORM)[self initTheTimeBar];
    //Set the fade slider
    [self setTheFadeSlider];
    //Initialize the array of results
    if (!_itemsInCurrentSearch) {
        self.itemsInCurrentSearch = [NSMutableArray array];
    }
    //Database checks before painting
    [self databaseInitializations];
    
    [self loadGenericMeshes];
}

#pragma mark - GLKView and GLKViewController delegate methods

-(void)startOpenGLElements{
    self.context = [[AGLKContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat16;//It was 24
}

- (void)setupGL
{
    [AGLKContext setCurrentContext:self.context];
    
    //[self loadShaders];
    
    self.effect = [[GLKBaseEffect alloc] init];
    self.effect.light0.enabled = GL_TRUE;
    self.effect.light0.diffuseColor = GLKVector4Make(1.0f, 0.4f, 0.4f, 1.0f);
    
    glEnable(GL_DEPTH_TEST);
    
    /*glGenVertexArraysOES(1, &_vertexArray);
     glBindVertexArrayOES(_vertexArray);
     
     glGenBuffers(1, &_vertexBuffer);
     glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
     glBufferData(GL_ARRAY_BUFFER, sizeof(sphereVerts), sphereVerts, GL_STATIC_DRAW);
     
     glEnableVertexAttribArray(GLKVertexAttribPosition);
     glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 24, BUFFER_OFFSET(0));
     glEnableVertexAttribArray(GLKVertexAttribNormal);
     glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, 24, BUFFER_OFFSET(12));*/
    
    glBindVertexArrayOES(0);
    
    /////////////////Add for nuclei size
    
    [self setUPSizeArrays];
    /////////////////Add for nuclei size END
    
    // Create vertex buffers containing vertices to draw
    self.vertexPositionBuffer = [[AGLKVertexAttribArrayBuffer alloc]
                                 initWithAttribStride:(3 * sizeof(GLfloat))
                                 numberOfVertices:sizeof(sphereVerts) / (3 * sizeof(GLfloat))
                                 bytes:sphereVerts
                                 usage:GL_STATIC_DRAW];
    self.vertexNormalBuffer = [[AGLKVertexAttribArrayBuffer alloc]
                               initWithAttribStride:(3 * sizeof(GLfloat))
                               numberOfVertices:sizeof(sphereNormals) / (3 * sizeof(GLfloat))
                               bytes:sphereNormals
                               usage:GL_STATIC_DRAW];
    self.vertexTextureCoordBuffer = [[AGLKVertexAttribArrayBuffer alloc]
                                     initWithAttribStride:(2 * sizeof(GLfloat))
                                     numberOfVertices:sizeof(sphereTexCoords) / (2 * sizeof(GLfloat))
                                     bytes:sphereTexCoords
                                     usage:GL_STATIC_DRAW];
    
    //-//new
    _rotMatrix = GLKMatrix4Identity;
    
    
}

//Add for nuclei size

-(void)setUPSizeArrays{
    vertCount = (sizeof sphereVerts)/ (sizeof sphereVerts[0]);
    
    for (int inc=0; inc<maxTimePoint; inc++) {
        float scaleNucs = (2.5f-((float)inc)/400);
        for (int k=0; k< vertCount+1; k++) {
            sphereVertsAdj[inc][k] = sphereVerts[k] * scaleNucs;
        }
    }
    
    //Prepare coloring buffer
    rawData = (float **)malloc(sizeof(float *)*10);
    for(int i=0; i < 10; i++) {
        rawData[i] = (float *)malloc(sizeof(float)*4);
    }
}

- (void)fillGLBuffers
{
    float thisTimeVerts[vertCount];
    for (int k=0; k< vertCount; k++) {
        thisTimeVerts[k] = sphereVertsAdj[count-1][k];
    }
    
    // Create vertex buffers containing vertices to draw
    self.vertexPositionBuffer = [[AGLKVertexAttribArrayBuffer alloc]
                                 initWithAttribStride:(3 * sizeof(GLfloat))
                                 numberOfVertices:sphereNumVerts//sizeof(thisTimeVerts) / (3 * sizeof(GLfloat))
                                 bytes:thisTimeVerts
                                 usage:GL_STATIC_DRAW];
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    
    glDeleteBuffers(1, &_vertexBuffer);
    glDeleteVertexArraysOES(1, &_vertexArray);
    
    self.effect = nil;
    
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
}

- (void)update{
    
    if (!userInteraction) {
        return;
    }
    //The following things can be done actually in viewdidload and didrotate
    float aspect = fabs(self.view.bounds.size.width / self.view.bounds.size.height);
    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), aspect, 0.1f, 100.0f);
    
    self.effect.transform.projectionMatrix = projectionMatrix;
    
//    GLKMatrix4 baseModelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -30.0f);
//    baseModelViewMatrix = GLKMatrix4Rotate(baseModelViewMatrix, _rotation.x, 1.0f, 0.0f, 0.0f);
    
    // Compute the model view matrix for the object rendered with GLKit
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, 0.0f);
//    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation.x, 1.0f, 1.0f, 1.0f);
//    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
    self.effect.transform.modelviewMatrix = modelViewMatrix;
    
    // Compute the model view matrix for the object rendered with ES2
//    modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, 0);
//    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation.x, 1.0f, 1.0f, 1.0f);
//    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
    
//    _normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(modelViewMatrix), NULL);
//    _modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
    
    //_rotation += self.timeSinceLastUpdate * 0.5f;
}

-(void)pauseLayer:(CALayer*)layer {
    layer.speed = 0.0;
}

-(void)resumeLayer:(CALayer*)layer {
    layer.speed = 1.0;
}


- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect{
    if (!userInteraction) {
        [self pauseLayer:self.view.layer.mask];
        return;
    }
    [self resumeLayer:self.view.layer.mask];
    
    // Clear back frame buffer (erase previous drawing)
    [(AGLKContext *)view.context clear:GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT];
    
    [self.vertexPositionBuffer
     prepareToDrawWithAttrib:GLKVertexAttribPosition
     numberOfCoordinates:3
     attribOffset:0
     shouldEnable:YES];
    [self.vertexNormalBuffer
     prepareToDrawWithAttrib:GLKVertexAttribNormal
     numberOfCoordinates:3
     attribOffset:0
     shouldEnable:YES];
    
    //Not necessary if no textures 
    //[self.vertexTextureCoordBuffer prepareToDrawWithAttrib:GLKVertexAttribTexCoord0 numberOfCoordinates:2 attribOffset:0 shouldEnable:YES];
    
    NSMutableArray *paluego = [NSMutableArray arrayWithCapacity:_group.count];//Place holder for the cells to be dim. Have to be drawn at the end only
    
    int x = 0;
    for (PSMesh *mesh in _group) {
        if (mesh.hidden) {
            continue;
        }
        
        [self getEffectForMesh:mesh];
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        if (_searchText) {
            if (mesh.posible) {
                for (int y = 0; y<10; y++) {
                    rawData[y][0] = 0.7f;rawData[y][1] = 0.3f;rawData[y][2] = 0.2f;rawData[y][3] = 0.1f;
                }
                self.effect.material.diffuseColor = GLKVector4Make(1.0, 1.0, 1.0, 1.0);
            }else if (mesh.foundInSearch) {
                for (int y = 0; y<10; y++) {
                    rawData[y][0] = 0.7f;rawData[y][1] = 0.7f;rawData[y][2] = 0.0f;rawData[y][3] = 0.1f;
                }
                self.effect.material.diffuseColor = GLKVector4Make(1.0, 1.0, 1.0, 1.0);
            }else{
                [paluego addObject:mesh];
                continue;
            }
        }else{
            if (!mesh.hidden) {
                NSMutableArray *searchesInThisTheme = [NSMutableArray array];
                
                if (self.severalSearches) {
                    for (Search *search in severalSearches) {
                        if ([mesh.cellTime.cell.searches containsObject:search]) {
                            [searchesInThisTheme addObject:search];
                        }
                    }
                }else{
                    for (Search *search in mesh.cellTime.cell.searches) {
                        if (search.theme == self.currentColorScheme) {
                            [searchesInThisTheme addObject:search];
                        }
                    }
                }
                
                searchesInThisTheme = [searchesInThisTheme sortedArrayUsingComparator:^(Search * obj1, Search * obj2) {
                    return [obj1.color compare:obj2.color options:NSCaseInsensitiveSearch];
                }].mutableCopy;
                
                int colorCount = (int)searchesInThisTheme.count;
                
                if (self.individualSearch) {
                    if ([searchesInThisTheme containsObject:self.individualSearch]) {
                        NSArray *comps = [self.individualSearch.color componentsSeparatedByString:@","];
                        for (int y = 0; y<10; y++) {
                            for (int j = 0; j<4; j++) {
                                rawData[y][j] = [[comps objectAtIndex:j]floatValue];
                            }
                        }
                        self.effect.material.diffuseColor = GLKVector4Make(1.0, 1.0, 1.0, 1.0);
                    }else{
                        [paluego addObject:mesh];continue;
                    }
                    
                }else if (!self.individualSearch && colorCount > 0) {
                    
                    NSMutableArray *reverseArray = [NSMutableArray array];//This way I avoid doing 10 times components separated by string.
                    for (Search *search in searchesInThisTheme) {
                        [reverseArray addObject:[[search color]componentsSeparatedByString:@","]];
                    }

                    if (colorCount == 1) {
                        NSArray *comps = [reverseArray lastObject];
                        for (int y = 0; y<10; y++) {
                            for (int j = 0; j<4; j++) {
                                rawData[y][j] = [[comps objectAtIndex:j]floatValue];
                            }
                        }
                    }else if(colorCount == 2){
                        int pick [10] = {0,0,1,1,1,1,1,1,0,0};
                        for (int y = 0; y < 10; y++) {
                            NSArray *comps = [reverseArray objectAtIndex:pick[y]];
                            for (int j = 0; j<4; j++) {
                                rawData[y][j] = [[comps objectAtIndex:j]floatValue];
                            }
                        }
                    }else if (colorCount == 3){
                        int pick [10] = {0,0,0,1,1,1,1,2,2,2};
                        for (int y = 0; y < 10; y++) {
                            NSArray *comps = [reverseArray objectAtIndex:pick[y]];
                            for (int j = 0; j<4; j++) {
                                rawData[y][j] = [[comps objectAtIndex:j]floatValue];
                            }
                        }
                    }else if (colorCount == 4){
                        int pick [10] = {0,0,1,1,1,2,2,2,3,3};
                        for (int y = 0; y < 10; y++) {
                            NSArray *comps = [reverseArray objectAtIndex:pick[y]];
                            for (int j = 0; j<4; j++) {
                                rawData[y][j] = [[comps objectAtIndex:j]floatValue];
                            }
                        }
                    }else{
                        int pick [10] = {0,0,1,1,2,2,3,3,4,4};
                        for (int y = 0; y < 10; y++) {
                            NSArray *comps = [reverseArray objectAtIndex:pick[y]];
                            for (int j = 0; j<4; j++) {
                                rawData[y][j] = [[comps objectAtIndex:j]floatValue];
                            }
                        }
                    }
                    self.effect.material.diffuseColor = GLKVector4Make(1.0, 1.0, 1.0, 1.0);
                }else{
                    [paluego addObject:mesh];continue;
                }
            }
        }
        
        int allInts [10] = {405, 162, 162, 162, 162, 162, 162, 162, 162, 243};
        int cum = 0;
        for (int z = 0; z < 10; z++) {
            self.effect.light0.diffuseColor = GLKVector4Make(rawData[z][0], rawData[z][1], rawData[z][2], rawData[z][3]);
            [self.effect prepareToDraw];
            glDrawArrays(GL_TRIANGLES, cum, allInts[z]);//Segunda fase son 569
            cum = cum + allInts[z];
        }
        
        x++;
    }
    
    for (PSMesh *mesh in paluego) {//Important, paint the dimmed ones at the end so that they don't poke through due to GL_BLEND technique
        [self getEffectForMesh:mesh];
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
        self.effect.light0.diffuseColor = GLKVector4Make(1, 1, 1, dimOthers);
        self.effect.material.diffuseColor = GLKVector4Make(0.3, 0.3, 0.3, dimOthers);
        //self.effect.material.ambientColor =  GLKVector4Make(0.5, 0.5, 0.5, 0.2);
        [self.effect prepareToDraw];
        glDrawArrays(GL_TRIANGLES, 0, 1944);
        glDisable(GL_CULL_FACE);
    }
    // Render the object again with ES2
    glUseProgram(_program);
    
    //glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, _modelViewProjectionMatrix.m);
    //glUniformMatrix3fv(uniforms[UNIFORM_NORMAL_MATRIX], 1, 0, _normalMatrix.m);
    
}

#pragma mark Mesh Configuration

//To configure each cell's position and size
-(void)configureCellSphere:(PSMesh *)sphere withManagedObjectCellTime:(CellTime *)aCellTime andTag:(int)x render:(BOOL)renderBool{
    
    if (!aCellTime) {
        sphere.hidden = YES;
        return;
    }else{
        sphere.hidden = NO;
        sphere.active = YES;
        if (sphere.cellTime == aCellTime) {
            return;
        }
    }
    
    sphere.x = aCellTime.x.floatValue;//-0.4;
    sphere.y = aCellTime.y.floatValue;// - 0.3;
    sphere.z = aCellTime.z.floatValue;//*0.504/0.087-0.24;
    sphere.scaleX = 1/aCellTime.size.floatValue*2;
    sphere.scaleY = 1/aCellTime.size.floatValue*2;
    sphere.scaleZ = 1/aCellTime.size.floatValue*2;
    
    sphere.cellTime = aCellTime;
    
    if (!_group) {
        self._group = [NSMutableArray arrayWithCapacity:730];
    }
    
    [_group addObject:sphere];
}

-(void)loadGenericMeshes{
    
    if (!self.currentColorScheme) {
        [self createPresetTheme];
    }
    
    dataComponentsArray = [NSMutableDictionary dictionaryWithCapacity:maxTimePoint];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"TimeWorm"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"timePoint" ascending:YES]];
    NSArray *array = [self.managedObjectContext executeFetchRequest:request error:nil];
    for (TimeWorm *time in array) {
        [dataComponentsArray setValue:time.cellTimes.allObjects forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[array indexOfObject:time]]];
    }
    
    [self setArrayOfTimeCellsAtTimePoint:maxTimePoint-1];
    
    if (dataOfMObjectsInTime.count >0) {
        for (int x = 0; x<610; x++) {
            PSMesh *sphere = [[PSMesh alloc] init];
            if (x<dataOfMObjectsInTime.count) {
                CellTime *cellT = [dataOfMObjectsInTime objectAtIndex:x];
                [self configureCellSphere:sphere withManagedObjectCellTime:cellT andTag:1 render:YES];
            }else{
                [self configureCellSphere:sphere withManagedObjectCellTime:nil andTag:0 render:YES];
            }
        }
    }
}


-(void)reuseMeshesWithTimePoint:(int)timePoint{
    
    ////////////Add for nuclei size
    [self fillGLBuffers];
    ///////////Add for nuceli size END
    [self setArrayOfTimeCellsAtTimePoint:timePoint];
    int x = 0;
    for (PSMesh *mesh in _group) {
        if (x<dataOfMObjectsInTime.count) {
            mesh.hidden = NO;
            mesh.active = YES;
            CellTime *cellTime = [dataOfMObjectsInTime objectAtIndex:x];
            [self resetMeshValues:mesh withCellTime:cellTime];
        }else{
            mesh.cellTime = nil;
            mesh.hidden = YES;
            mesh.active = NO;
        }
        x++;
    }
    
    if (_searchText) {
        [self searchVC:nil didSearchForCell:_searchText withFadeOthersValue:dimOthers];// andOptionsArray:_searchArray];
    }
}

#pragma mark utilities

-(void)createPresetTheme{
    if (WORM) {
        ColorScheme *theme = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
        self.currentColorScheme.isCurrent = [NSNumber numberWithBool:NO];
        theme.isCurrent = [NSNumber numberWithBool:YES];
        theme.name = @"New Lineage-based Theme";
        self.currentColorScheme = theme;
        
        [self searchVC:nil createSearch:@"ABa" withColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
        [self searchVC:nil createSearch:@"ABp" withColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1]];
        [self searchVC:nil createSearch:@"MS" withColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:1]];
        [self searchVC:nil createSearch:@"E" withColor:[UIColor colorWithRed:1 green:1 blue:0 alpha:1]];
        [self searchVC:nil createSearch:@"C" withColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
        [self searchVC:nil createSearch:@"D" withColor:[UIColor colorWithRed:0.7 green:0.7 blue:0.75 alpha:1]];
    }
    if (FLY) {
        ColorScheme *theme = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
        self.currentColorScheme.isCurrent = [NSNumber numberWithBool:NO];
        theme.isCurrent = [NSNumber numberWithBool:YES];
        theme.name = @"New blank Theme";
        self.currentColorScheme = theme;
        [self.managedObjectContext save:nil];
    }
}

-(void)resetMeshValues:(PSMesh *)mesh withCellTime:(CellTime *)cellTime{
    
    //material.alpha =  0.5;
    mesh.x = cellTime.x.floatValue;
    mesh.y = cellTime.y.floatValue;
    mesh.z = cellTime.z.floatValue;;
    mesh.cellTime = cellTime;
}

-(void)setArrayOfTimeCellsAtTimePoint:(int)timePoint{
    NSArray *array = [dataComponentsArray valueForKey:[NSString stringWithFormat:@"%i", timePoint]];
    if (array) {
        dataOfMObjectsInTime = array;
    }
    nuclei.text = [NSString stringWithFormat:@"%lu nuclei", (unsigned long)dataOfMObjectsInTime.count];
}

-(void)playMovie:(UIButton *)sender{
    
    if (sender.tag == 0) {
        userInteraction = YES;
        sender.tag = 1;
        [sender setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    }else{
        userInteraction = NO;
        sender.tag = 0;
        [sender setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        [timer invalidate];
        return;
    }
    if (count == maxTimePoint) {
        count = 1;
    }
    if(count == maxTimePoint - 1){
        userInteraction = NO;
        if (timer) {
            [timer invalidate];
        }
    }
    timer = [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(forward:) userInfo:nil repeats:YES];
    [timer fire];
}

-(void)movedTime{
    self.sliderTime.value = (float)count;
    timePointLabel.text = [NSString stringWithFormat:@"%i minutes", (int)(sliderTime.value)];
    [self reuseMeshesWithTimePoint:count];
}

-(IBAction)forward:(id)sender{
    [self refresher];
    if(count == maxTimePoint){
        return;
    }
    count++;
    [self movedTime];
}

-(IBAction)back:(id)sender{
    [self refresher];
    if(count == 1){
        return;
    }
    count--;
    [self movedTime];
}


//Generates matrix
-(GLKMatrix4)getEffectForMesh:(PSMesh *)mesh{
    float initialRotationOfWorm = M_PI+0.2f;
    
    GLKMatrix4 baseModelViewMatrix = GLKMatrix4MakeTranslation(translation.x, translation.y, -30+distance);
    
    baseModelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, _rotMatrix);
    
    baseModelViewMatrix = GLKMatrix4Rotate(baseModelViewMatrix, initialRotationOfWorm, 0.0f, 0.0f, 1.0f);//Corrects initial rotation
    GLKMatrix4 modelViewMatrix;
    if (WORM) {
        modelViewMatrix = GLKMatrix4MakeTranslation((mesh.x/10)-20, (mesh.y/10)-28, (mesh.z/1.8f)-12);//(mesh.x/20)-18, (mesh.y/20)-14, (mesh.z/3)-10);
    }
    if (FLY) {
        modelViewMatrix = GLKMatrix4MakeTranslation((mesh.x/10)+5, (mesh.y/10), (mesh.z/1.8f)-5);//(mesh.x/20)-18, (mesh.y/20)-14, (mesh.z/3)-10);
    }
    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, M_PI/2 - initialRotationOfWorm, 0.0f, 0.0f, 1.0f);//To handle stripes in anterior/posterior axis
    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
    
    self.effect.transform.modelviewMatrix = modelViewMatrix;
    
    return modelViewMatrix;
    //glBindVertexArrayOES(_vertexArray);
    // Now render it:
}

#pragma mark screen interaction

//This method determines whether a mesh has been touched. Transforms the 2D coordinates in the screen to the 3D (Reverse model/view transformation)
-(BOOL)analyzeMesh:(PSMesh *)mesh andTapLocation:(CGPoint)tapLocation{
    
    
    GLKMatrix4 matrix = [self getEffectForMesh:mesh];
    
    float newXPos = matrix.m30 * scaleFactorDevice;
    float newYPos = -matrix.m31 * scaleFactorDevice;
    float newZPos = matrix.m32;
    
    float extrusion = newZPos/-40;
    if (self.view.bounds.size.width > self.view.bounds.size.height) {
        extrusion = extrusion * 2048/1536;
    }
    newXPos = newXPos/extrusion;
    newYPos = newYPos/extrusion;
    
    //Compare values of mesh and values of tap
    //NSLog(@"TAP X %f and mesh in %f TAP Y %f and mesh in %f", tapLocation.x- self.view.bounds.size.width/2, newXPos, tapLocation.y- self.view.bounds.size.height/2, newYPos);
    float valueX = newXPos - (tapLocation.x - self.view.bounds.size.width/2);//Funciona con z 0 y sin rotacion
    float valueY = newYPos - (tapLocation.y - self.view.bounds.size.height/2);//Funciona con z 0 y sin rotacion
    if (valueX<0)valueX = -valueX;
    if (valueY<0)valueY = -valueY;
    
    int areaToScan = 10;
    areaToScan = MAX(areaToScan, areaToScan/extrusion);
    areaToScan = MIN(areaToScan, 250);
    
    //Analize proximity of touch and mesh. If there is touch, do something (temporarily show a message, in the future, trigger window/label with info)
    if (valueX <= areaToScan) {
        if (valueY <=areaToScan) {
            return YES;
        }
    }
    return NO;
}


//Utiliy method for developer's use only
-(void)analyzeMeshMatrix:(PSMesh *)mesh{
    
    GLKMatrix4 matrix = [self getEffectForMesh:mesh];
    //GLKMatrix4 matrix = modelViewMatrix;
    //NSLog(@"Matrixxxxxxxxx\n %f, %f, %f, %f,\n %f, %f, %f, %f,\n %f, %f, %f, %f,\n %f, %f, %f, %f", matrix.m00, matrix.m01, matrix.m02, matrix.m03, matrix.m10, matrix.m11, matrix.m12, matrix.m13, matrix.m20, matrix.m21, matrix.m22, matrix.m23, matrix.m30, matrix.m31, matrix.m32, matrix.m33);
    float newXPos = matrix.m30 * 20;
    float newYPos = -matrix.m31 * 20;
    float newZPos = matrix.m32;
    float extrusion = newZPos/-40;
    if (self.view.bounds.size.width > self.view.bounds.size.height) {
        extrusion = extrusion * 2048/1536;
    }
    newXPos = newXPos/extrusion;
    newYPos = newYPos/extrusion;
    
    int areaToScan = 20;
    areaToScan = MAX(areaToScan, areaToScan/extrusion);
    areaToScan = MIN(areaToScan, 250);
    
    transf = [[UIView alloc]initWithFrame:CGRectMake((newXPos-(areaToScan/2))+(self.view.bounds.size.width/2), (newYPos-(areaToScan/2)) + (self.view.bounds.size.height/2), areaToScan, areaToScan)];
    //NSLog(@"Positions %f, %f, %f, in screen %fx%f", transf.frame.origin.x, transf.frame.origin.y, newZPos, self.view.bounds.size.width, self.view.bounds.size.height);
    transf.layer.borderColor = [UIColor whiteColor].CGColor;
    transf.layer.borderWidth = 3.0f;
    
    transf.backgroundColor = [UIColor clearColor];
    self.view.autoresizesSubviews = NO;
    transf.autoresizingMask = UIViewAutoresizingNone;
    [self.view addSubview:transf];
}

-(PSMesh *)getMeshFromTapLocation:(CGPoint)tapLocation{
    //Compare tap coordianates with all the meshes
    NSMutableArray *posibles = [NSMutableArray arrayWithCapacity:_group.count];
    
    /*
     for (int x = 0; x<1; x++) {
     PSMesh *mesh = [_group objectAtIndex:x];
     [self analyzeMeshMatrix:mesh];
     }//*/
    
    for (PSMesh *mesh in _group) {
        if ([self analyzeMesh:mesh andTapLocation:tapLocation]) {
            if (!mesh.hidden) {
                [posibles addObject:mesh];
            }
        }
    }
    
    PSMesh *selectedMesh = nil;
    float zPos = 100;
    
    for (PSMesh *mesh in posibles) {
        //[self analyzeMeshMatrix:mesh];
        GLKMatrix4 baseModelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -30.0f+distance);
        //-//baseModelViewMatrix = GLKMatrix4Rotate(baseModelViewMatrix, _rotation.x, 1.0f, 0.0f, 0.0f);
        //-//baseModelViewMatrix = GLKMatrix4Rotate(baseModelViewMatrix, _rotation.y, 0.0f, 1.0f, 0.0f);
        GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation((mesh.x/20)-18, (mesh.y/20)-14, (-1)*(mesh.z/3));
        //-//new
        modelViewMatrix = GLKMatrix4Multiply(modelViewMatrix, _rotMatrix);
        modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
        self.effect.transform.modelviewMatrix = modelViewMatrix;
        GLKMatrix4 matrix = modelViewMatrix;
        float meshZ = matrix.m32;
        
        if (meshZ<zPos) {
            NSLog(@"Matrix is %f for Z", meshZ);
            zPos = meshZ;
            selectedMesh = mesh;
        }
    }
    
    
    return selectedMesh;
}

//Implementation of the tap gesture
- (void)handleTap:(UITapGestureRecognizer*)recognizer{
    NSLog(@"View is wide %f and tall %f", self.view.frame.size.width, self.view.frame.size.height);
    
    CGPoint tapLocation = [recognizer locationInView:self.view];
    
    PSMesh *selectedMesh = [self getMeshFromTapLocation:tapLocation];
    
    if (selectedMesh) {
        self.selectedMesh = selectedMesh;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            NSMutableString *message = [NSMutableString stringWithFormat:@""];
            if (selectedMesh.cellTime.cell.nameProper) {
                [message appendString:[NSString stringWithFormat:@"%@ :", selectedMesh.cellTime.cell.nameProper]];
            }
            if (selectedMesh.cellTime.cell.descriptionOfCell) {
                [message appendString:selectedMesh.cellTime.cell.descriptionOfCell];
            }
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:selectedMesh.cellTime.cell.name message:message delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:@"Look up", @"Info", nil];
            alert.tag = 80;
            alert.delegate = self;
            [alert show];
            return;
        }
        PSOptionsCellViewController *vc= [[PSOptionsCellViewController alloc]init];
        vc.title = selectedMesh.cellTime.cell.name;
        vc.cell = selectedMesh.cellTime.cell;
        vc.delegate = self;
        vc.contentSizeForViewInPopover = CGSizeMake(200, 0);
        UINavigationController *navCon = [[UINavigationController alloc]initWithRootViewController:vc];
        vc.view.backgroundColor = [UIColor whiteColor];
        if (WORM) {
            vc.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:vc action:@selector(showMore)];
        }
        vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:vc action:@selector(searchMore)];
        self.popover = [[UIPopoverController alloc]initWithContentViewController:navCon];
        UIPopoverArrowDirection direction;
        if (tapLocation.y < self.view.bounds.size.height/2) {
            direction = UIPopoverArrowDirectionUp;
        }else{
            direction = UIPopoverArrowDirectionDown;
        }
        [self.popover presentPopoverFromRect:CGRectMake(tapLocation.x, tapLocation.y, 10, 10) inView:self.view permittedArrowDirections:direction animated:YES];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 80) {
        if (buttonIndex == 1) {
            SearchController *searchVC = [[SearchController alloc]initWithNibName:@"SearchIPhone" bundle:nil andDelegate:self];
            //Removed autorelease to avoid crash in iOS6. Will add again when only support for iOS7
            searchVC.currentColorScheme = self.currentColorScheme;
            searchVC.previousSearchString = self.selectedMesh.cellTime.cell.name;
            searchVC.contentSizeForViewInPopover = CGSizeMake(200, 0);
            searchVC.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
            UINavigationController *navcon = [[UINavigationController alloc]initWithRootViewController:searchVC];
            navcon.navigationBar.barStyle = UIBarStyleBlackOpaque;
            [self presentViewController:navcon animated:YES completion:^{}];
        }else if(buttonIndex == 2){
            PSOptionsCellViewController *vc= [[PSOptionsCellViewController alloc]init];
            vc.second = YES;
            vc.title = _selectedMesh.cellTime.cell.name;
            vc.cell = _selectedMesh.cellTime.cell;
            vc.delegate = self;
            vc.contentSizeForViewInPopover = CGSizeMake(200, 0);
            UINavigationController *navCon = [[UINavigationController alloc]initWithRootViewController:vc];
            vc.view.backgroundColor = [UIColor whiteColor];
            vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(close)];
            [self presentViewController:navCon animated:YES completion:nil];
        }
    }
    
    if (alertView.tag == 10 && buttonIndex == 1) {
        self.currentColorScheme.name = [alertView textFieldAtIndex:0].text;
        [self.managedObjectContext save:nil];
    }
}

-(void)optionsClickedSearchCell:(PSOptionsCellViewController *)optionsPanel{
    [self.popover dismissPopoverAnimated:YES];
    SearchController *searchVC = [[SearchController alloc]initWithNibName:nil bundle:nil andDelegate:self];
    searchVC.previousSearchString = optionsPanel.cell.name;
    searchVC.currentColorScheme = self.currentColorScheme;
    self.popover.delegate = self;
    self.popover = [[UIPopoverController alloc]initWithContentViewController:searchVC];
    [self.popover presentPopoverFromRect:CGRectMake(self.view.bounds.size.width, 20, 5, 5) inView:self.navigationController.navigationBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)handleLong:(UILongPressGestureRecognizer *)sender{
    CGPoint tapLocation = [sender locationInView:self.view];
    
    PSMesh *selectedMesh = [self getMeshFromTapLocation:tapLocation];
    
    if (selectedMesh) {
        self.searchText = selectedMesh.cellTime.cell.name;
        
        if (self.popover.isPopoverVisible) {
            [self.popover dismissPopoverAnimated:YES];
            return;
        }
        
        NSString *nibString = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            nibString = @"SearchIPhone";
        }
        //SearchController *searchCont = [[]
        SearchController *searchVC = [[SearchController alloc]initWithNibName:nibString bundle:nil andDelegate:self];
        searchVC.previousSearchString = self.searchText;
        searchVC.currentColorScheme = self.currentColorScheme;
        //*
        searchVC.previousSearchString = self.searchText;
        //*/
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            searchVC.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
            UINavigationController *navcon = [[UINavigationController alloc]initWithRootViewController:searchVC];
            
            navcon.navigationBar.barStyle = UIBarStyleBlackOpaque;
            [self presentViewController:navcon animated:YES completion:nil];
            
            return;
        }else{
            self.popover.delegate = self;
            self.popover = [[UIPopoverController alloc]initWithContentViewController:searchVC];
            [self.popover presentPopoverFromRect:CGRectMake(self.view.bounds.size.width, 20, 5, 5) inView:self.navigationController.navigationBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            [searchVC.delegate searchVC:searchVC didSearchForCell:selectedMesh.cellTime.cell.name withFadeOthersValue:dimOthers];// andOptionsArray:[searchVC gatherOptions]];
        }
    }
}

-(void)searchMore{
    SearchController *searchVC = [[SearchController alloc]initWithNibName:nil bundle:nil andDelegate:self];
    //searchVC.previousSearchString = self.cell.searchMore;
    searchVC.currentColorScheme = self.currentColorScheme;
    self.popover.delegate = self;
    [self.popover dismissPopoverAnimated:YES];
    self.popover = [[UIPopoverController alloc]initWithContentViewController:searchVC];
    [self.popover presentPopoverFromRect:CGRectMake(self.view.bounds.size.width, 20, 5, 5) inView:self.navigationController.navigationBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)handlePinch:(UIGestureRecognizer *)sender
{
    CGFloat factor = [(UIPinchGestureRecognizer *)sender scale];
    
    if ([(UIPinchGestureRecognizer *)sender velocity]<0) {
        distance -= factor;
        if (distance< - 60.0) {
            distance = -60.0f;
        }
        return;
    }
    if(factor<0)factor = factor*3;
    else factor = factor * 0.33f;
    distance += factor;
    if (distance>35) {
        distance = 35.0f;
    }
}

-(void)applyRotationWithCGPoint:(CGPoint)diff{
    float rotX = diff.y * 0.005;//-1 * GLKMathDegreesToRadians(diff.y / 2.0);
    float rotY = diff.x * 0.005;//-1 * GLKMathDegreesToRadians(diff.x / 2.0);
    
    bool isInvertible;
    GLKVector3 xAxis = GLKMatrix4MultiplyVector3(GLKMatrix4Invert(_rotMatrix, &isInvertible),
                                                 GLKVector3Make(1, 0, 0));
    _rotMatrix = GLKMatrix4Rotate(_rotMatrix, rotX, xAxis.x, xAxis.y, xAxis.z);
    GLKVector3 yAxis = GLKMatrix4MultiplyVector3(GLKMatrix4Invert(_rotMatrix, &isInvertible),
                                                 GLKVector3Make(0, 1, 0));
    _rotMatrix = GLKMatrix4Rotate(_rotMatrix, rotY, yAxis.x, yAxis.y, yAxis.z);
}

//Implementation of rotation and zoom gestures
- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touchA, *touchB;
	CGPoint pointA, pointB;
	
	// Pan gesture.
	if ([touches count] == 1)
	{
		/*/-//touchA = [[touches allObjects] objectAtIndex:0];
		pointA = [touchA locationInView:self.view];
		pointB = [touchA previousLocationInView:self.view];
		
		position.x = (pointA.x - pointB.x);
		position.y = (pointA.y - pointB.y);*/
        
        //-//New
        UITouch * touch = [touches anyObject];
        CGPoint location = [touch locationInView:self.view];
        CGPoint lastLoc = [touch previousLocationInView:self.view];
        CGPoint diff = CGPointMake(location.x - lastLoc.x, location.y - lastLoc.y);
        
        [self applyRotationWithCGPoint:diff];
        
	}
	// Pinch gesture.
	else if ([touches count] == 2)
	{
		/*touchA = [[touches allObjects] objectAtIndex:0];
		touchB = [[touches allObjects] objectAtIndex:1];
		
		// Current distance.
		pointA = [touchA locationInView:self.view];
		pointB = [touchB locationInView:self.view];
		float currDistance = [self distanceFromPoint:pointA toPoint:pointB];
		
		// Previous distance.
		pointA = [touchA previousLocationInView:self.view];
		pointB = [touchB previousLocationInView:self.view];
		float prevDistance = [self distanceFromPoint:pointA toPoint:pointB];
		
		distance += (currDistance - prevDistance) * 0.005;*/
        
        touchA = [[touches allObjects] objectAtIndex:0];
		touchB = [[touches allObjects] objectAtIndex:1];
		
		// Current distance.
		pointA = [touchA locationInView:self.view];
		pointB = [touchA previousLocationInView:self.view];
        if (fabs(translation.x)<=10.0f) {
            translation.x += (pointA.x - pointB.x) * 0.04f;
        }else{
            if(translation.x > 0)
                translation.x = 10.0;
            else
                translation.x = -10.0;
        }
        
        if (fabs(translation.y)<=10.0f) {
             translation.y += (pointA.y - pointB.y) * -0.04f;
        }else{
            if(translation.y > 0)
                translation.y = 10.0;
            else
                translation.y = -10.0;
        }
	}    
    _rotation.y += position.x * 0.005;
    _rotation.x += position.y * 0.005;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    userInteraction = YES;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"Just here");
    userInteraction = NO;
}

//Utility method to calculate the distance trajected by the finger during the gesture. Used below, in the implementation of the gestures
- (float) distanceFromPoint:(CGPoint)pointA toPoint:(CGPoint)pointB{
	float xD = fabs(pointA.x - pointB.x);
	float yD = fabs(pointA.y - pointB.y);
	
	return sqrt(xD*xD + yD*yD);
}

#pragma mark Navigation

-(void)goToInfo:(UIButton *)sender{
    if (self.popover.isPopoverVisible) {
        [self.popover dismissPopoverAnimated:YES];
        return;
    }
    InfoViewcontrollerViewController *infoV = [[InfoViewcontrollerViewController alloc]init];
    infoV.view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIWebView *wv = [[UIWebView alloc]initWithFrame:infoV.view.frame];
    wv.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    NSURL *URL;
    if (WORM) {
        URL = [[NSBundle mainBundle] URLForResource:@"info" withExtension:@"htm"];
    }
    if (FLY) {
        URL = [[NSBundle mainBundle] URLForResource:@"infofly" withExtension:@"htm"];
    }
	NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    [wv loadRequest:request];
    
    [infoV.view addSubview:wv];
    if (WORM) {
        infoV.title = @"WormGUIDES info";
    }
    if (FLY) {
        infoV.title = @"FlyGUIDES info";
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
	{
        infoV.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
        UINavigationController *navcon = [[UINavigationController alloc]initWithRootViewController:infoV];
        navcon.navigationBar.barStyle = UIBarStyleBlackOpaque;
		[self presentViewController:navcon animated:YES completion:nil];
        return;
	}
    self.popover = [[UIPopoverController alloc]initWithContentViewController:infoV];
    
    self.popover.contentViewController = infoV;
    [self.popover presentPopoverFromBarButtonItem:[self.navigationItem.rightBarButtonItems objectAtIndex:0] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

-(void)action:(UIBarButtonItem *)sender{
    
    userInteraction = YES;
    
    if (self.popover.isPopoverVisible) {
        [self.popover dismissPopoverAnimated:YES];
        return;
    }
    LineageTracing *lineageTracing = [[LineageTracing alloc]init];
    lineageTracing.delegate = self;
    lineageTracing.currentColorScheme = self.currentColorScheme;
    UINavigationController *navCon = [[UINavigationController alloc]initWithRootViewController:lineageTracing];
    //navCon.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
	{
        lineageTracing.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
		[self presentViewController:navCon animated:YES completion:nil];
        return;
	}
    
    self.popover = [[UIPopoverController alloc]initWithContentViewController:navCon];
    self.popover.delegate = self;
    [self.popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    //[lineageTracing release];
}

-(void)goToPalette:(UIButton *)sender{
    
    userInteraction = YES;
    
    if (self.popover.isPopoverVisible) {
        [self.popover dismissPopoverAnimated:YES];
        self.legend = nil;
        return;
    }
    if (legend && ![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self.legend.view removeFromSuperview];
        self.legend = nil;
        return;
    }
    self.legend = [[LegendViewController alloc]initWithStyle:UITableViewStylePlain andCurrentColorScheme:self.currentColorScheme andDimValue:dimOthers];
    legend.delegate = self;
    if (self.severalSearches) {
        legend.severalSearches = [NSMutableArray arrayWithArray:self.severalSearches];
    }else if (self.individualSearch){
        legend.severalSearches = [NSMutableArray arrayWithObject:self.individualSearch];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
	{
        legend.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
        UINavigationController *navcon = [[UINavigationController alloc]initWithRootViewController:legend];
        navcon.navigationBar.barStyle = UIBarStyleBlackOpaque;
		[self presentViewController:navcon animated:YES completion:nil];
        return;
	}
    
    self.popover = [[UIPopoverController alloc]initWithContentViewController:legend];
    self.popover.delegate = self;
    [self.popover presentPopoverFromRect:sender.frame inView:self.navigationController.navigationBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)pin{
    if (self.popover.isPopoverVisible) {
        [self.popover dismissPopoverAnimated:YES];
    }
    self.legend = [[LegendViewController alloc]initWithStyle:UITableViewStylePlain andCurrentColorScheme:self.currentColorScheme andDimValue:dimOthers];
    self.legend.delegate = self;
    self.legend.view.frame = CGRectMake(self.view.bounds.size.width - WIDTH_LEGEND, 0, WIDTH_LEGEND, WIDTH_LEGEND+140);
    self.legend.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    [self.view addSubview:legend.view];
}

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self unpin];
    userInteraction = NO;
}

-(void)unpin{
    [self.legend.view removeFromSuperview];
}


-(void)search:(UIBarButtonItem *)sender{
    
    userInteraction = YES;
    
    if (!_searchText) {
        //self.searchText = @"";
    }
    
    if (self.popover.isPopoverVisible) {
        [self.popover dismissPopoverAnimated:YES];
        return;
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
	{
        SearchController *searchVCIPhone = [[SearchController alloc]initWithNibName:@"SearchIPhone" bundle:nil andDelegate:self];
        searchVCIPhone.previousSearchString = self.searchText;
        searchVCIPhone.currentColorScheme = self.currentColorScheme;
        searchVCIPhone.previousSearchString = self.searchText;
        searchVCIPhone.delegate = self;
        searchVCIPhone.navigationItem.leftBarButtonItems = @[[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)], [[UIBarButtonItem alloc]initWithTitle:@"?" style:UIBarButtonItemStyleBordered target:self action:@selector(help)]];
        UINavigationController *navcon = [[UINavigationController alloc]initWithRootViewController:searchVCIPhone];
        navcon.navigationBar.barStyle = UIBarStyleBlackOpaque;
		[self presentViewController:navcon animated:YES completion:nil];
        return;
	}else{
        SearchController *searchVC = [[SearchController alloc]initWithNibName:nil bundle:nil andDelegate:self];
        searchVC.previousSearchString = self.searchText;
        searchVC.currentColorScheme = self.currentColorScheme;
        searchVC.delegate = self;
        self.popover = [[UIPopoverController alloc]initWithContentViewController:searchVC];
        self.popover.delegate = self;
        [self.popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

-(void)close{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)help{
    if (self.popover.isPopoverVisible) {
        [self.popover dismissPopoverAnimated:YES];
        return;
    }
    InfoViewcontrollerViewController *infoV = [[InfoViewcontrollerViewController alloc]init];
    infoV.view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIWebView *wv = [[UIWebView alloc]initWithFrame:infoV.view.frame];
    wv.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"help" withExtension:@"htm"];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    [wv loadRequest:request];
    
    [infoV.view addSubview:wv];
    if (WORM) {
        infoV.title = @"WormGUIDES help";
    }
    if (FLY) {
        infoV.title = @"FlyGUIDES help";
    }
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        infoV.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
        UINavigationController *navcon = [[UINavigationController alloc]initWithRootViewController:infoV];
        navcon.navigationBar.barStyle = UIBarStyleBlackOpaque;
        [self presentViewController:navcon animated:YES completion:nil];
        return;
    }
    self.popover = [[UIPopoverController alloc]initWithContentViewController:infoV];
    
    self.popover.contentViewController = infoV;
    [self.popover presentPopoverFromBarButtonItem:[self.navigationItem.rightBarButtonItems objectAtIndex:0] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark SearchCell delegate



-(NSString *)stringProcessedForMultipleSearch:(NSString *)inputString{
    NSString *cleanSearchString = [inputString stringByReplacingOccurrencesOfString:@" " withString:@","];
    cleanSearchString = [cleanSearchString stringByReplacingOccurrencesOfString:@"AND," withString:@""];
    cleanSearchString = [cleanSearchString stringByReplacingOccurrencesOfString:@"And," withString:@""];
    cleanSearchString = [cleanSearchString stringByReplacingOccurrencesOfString:@"and," withString:@""];
    return cleanSearchString;
}

-(NSMutableArray *)findCellsForTerm:(NSString *)searchString{
    
    NSMutableArray *foundCells = [NSMutableArray array];
    
    if (_searchSettings.searchDescription) {//To support search like anphid sheath
        
        NSString *cleanSearchString = [self stringProcessedForMultipleSearch:searchString];
        NSArray *componentsSearch = [cleanSearchString componentsSeparatedByString:@","];
        if (componentsSearch.count > 1) {
            NSMutableArray *collections = [NSMutableArray arrayWithCapacity:componentsSearch.count];
            for (NSString *term in componentsSearch) {
                NSArray *resultOfSearchTerm = [self.database executeSearchWithString:term];
                [collections addObject:resultOfSearchTerm];
                for(id obj in resultOfSearchTerm){
                    if(![foundCells containsObject:obj])[foundCells addObject:obj];
                }
                //[foundCells addObjectsFromArray:resultOfSearchTerm];
            }
            //Now find the intersection
            NSMutableSet *set = [NSMutableSet setWithArray:foundCells];
            for (NSArray *collection in collections) {
                [set intersectSet:[NSSet setWithArray:collection]];
            }
            [foundCells removeAllObjects];
            [foundCells addObjectsFromArray:set.allObjects];
        }else{
            NSArray *resultOfSearchTerm = [self.database executeSearchWithString:searchString];// andOptionsArray:array];
            [foundCells addObjectsFromArray:resultOfSearchTerm];
        }
        
    }else{
        NSString *cleanSearchString = [self stringProcessedForMultipleSearch:searchString];
        NSArray *componentsSearch = [cleanSearchString componentsSeparatedByString:@","];
        
        for (NSString *term in componentsSearch) {
            NSArray *resultOfSearchTerm = [self.database executeSearchWithString:term];// andOptionsArray:array];
            [foundCells addObjectsFromArray:resultOfSearchTerm];
        }
    }
    return foundCells;
}

-(void)colorYellowTheFoundCellsInSearch{
    for (PSMesh *mesh in _group) {//Loop through all the meshes used
        mesh.posible = NO;
        mesh.foundInSearch = NO;
        //mesh.active = NO;
        for (Cell *cell in _itemsInCurrentSearch) {
            if ([cell.name isEqualToString:mesh.cellTime.cell.name]) {
                mesh.foundInSearch = YES;
                mesh.active = YES;
            }
        }
    }
}

-(void)setItemsInCurrentSearch:(NSMutableArray *)itemsInCurrentSearch{
    _itemsInCurrentSearch = itemsInCurrentSearch;
    [self colorYellowTheFoundCellsInSearch];
}

-(void)searchVC:(SearchController *)searchController didSearchForCell:(NSString *)searchString withFadeOthersValue:(float)newFadeValue{
    
    int typeOfSearch = [self.searchSettings propagationType];
    [self.itemsInCurrentSearch removeAllObjects];//Emptying the current search buffer
    self.searchText = searchString;//Store the current search word in the delegate (self)
    dimOthers = newFadeValue;
    
    self.itemsInCurrentSearch = [self findCellsForTerm:searchString];
    
    if (_itemsInCurrentSearch.count > 0) {
        [self colorYellowTheFoundCellsInSearch];
    
    
    
    
    
    //Maybe this is too much. See how to refactor this
        //Make methods to search in different compartment exact, partial, prefix////////////// Use everywhere actually
    }else{//Will paint in orange the partial searches
        if (FLY) {
            return;//Since the regular search already executes partial search
        }
        
        for (PSMesh *mesh in _group) {
            mesh.posible = NO;
            mesh.foundInSearch = NO;
            mesh.active = YES;//RCF
            
            if (!mesh.hidden) {//Is being used at this time point
                
                BOOL cellAdded = NO;
                
                if (self.searchSettings.searchProper == YES) {
                    if (mesh.cellTime.cell.nameProper.length > 1) {
                        NSString *propName = mesh.cellTime.cell.nameProper.lowercaseString;
                        NSComparisonResult result = [propName compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                                                     range:[propName rangeOfString:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)]];
                        if (result == NSOrderedSame) {
                            cellAdded = YES;
                        }
                    }
                }
                
                if (self.searchSettings.searchSystematic == YES && !cellAdded) {
                    
                    if (mesh.cellTime.cell.name.length > 0) {
                        
                        NSArray *array;
                        if ([searchString isEqualToString:@"P1"]) {
                            array = [NSArray arrayWithObjects:@" EMS", @" MS",@" E",@" P2",@" P3",@" P4",@" C",@" D",@" Z2",@" Z3", nil];
                        }else if ([searchString isEqualToString:@"P2"]) {
                            array = [NSArray arrayWithObjects:@" P3",@" P4",@" C",@" D",@" Z2",@" Z3", nil];
                        }else if ([searchString isEqualToString:@"EMS"]) {
                            array = [NSArray arrayWithObjects:@" MS",@" E", nil];
                        }else if ([searchString isEqualToString:@"P3"]) {
                            array = [NSArray arrayWithObjects:@" P4",@" D",@" Z2",@" Z3", nil];
                        }else if ([searchString isEqualToString:@"P4"]) {
                            array = [NSArray arrayWithObjects:@" Z2",@" Z3", nil];
                        }else{
                            array = nil;
                        }
                        if (array) {
                            for (NSString *string in array) {
                                NSComparisonResult result = [mesh.cellTime.cell.name compare:string options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                                                       range:[mesh.cellTime.cell.name rangeOfString:string options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)]];
                                
                                if (result == NSOrderedSame && ([[mesh.cellTime.cell.name lowercaseString] hasPrefix:string] || [mesh.cellTime.cell.name hasPrefix:string])) {
                                    cellAdded = YES;
                                }
                            }
                        }else{
                            //NSLog(@"Mesh '%@' and search '%@'", mesh.cellTime.cell.name, searchString);
                            NSComparisonResult result = [mesh.cellTime.cell.name compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                                 range:[mesh.cellTime.cell.name rangeOfString:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)]];
                            
                            if (result == NSOrderedSame  && ([[mesh.cellTime.cell.name lowercaseString] hasPrefix:searchString] || [mesh.cellTime.cell.name hasPrefix:searchString])) {
                                cellAdded = YES;
                            }
                            
                        }
                        if(typeOfSearch == 1 || typeOfSearch == 11 || typeOfSearch == 111 || typeOfSearch == 101){
                            //Do triming backwards here
                            NSMutableString *mutString = [NSMutableString stringWithString:searchString];
                            while (mutString.length>1) {
                                NSString *trimmedString = [mutString copy];
                                //NSLog(@"Comparing '%@' with '%@'", mesh.cellTime.cell.name, trimmedString);
                                NSComparisonResult resultB = [trimmedString compare:mesh.cellTime.cell.name options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                                              range:[mesh.cellTime.cell.name rangeOfString:mesh.cellTime.cell.name options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)]];
                                if ([mutString isEqualToString:@"MS"] || [mutString isEqualToString:@"E"] ) {
                                    if ([mesh.cellTime.cell.name isEqualToString:@"EMS"] ||[mesh.cellTime.cell.name isEqualToString:@"P1"]) {
                                        resultB = NSOrderedSame;
                                    }
                                }else if ([mutString isEqualToString:@"Z2"] || [mutString isEqualToString:@"Z3"] ) {
                                    if ([mesh.cellTime.cell.name isEqualToString:@"P1"] ||[mesh.cellTime.cell.name isEqualToString:@"P2"] ||[mesh.cellTime.cell.name isEqualToString:@"P3"] ||[mesh.cellTime.cell.name isEqualToString:@"P4"]) {
                                        resultB = NSOrderedSame;
                                    }
                                }else if ([mutString isEqualToString:@"D"]) {
                                    if ([mesh.cellTime.cell.name isEqualToString:@"P1"] ||[mesh.cellTime.cell.name isEqualToString:@"P2"] ||[mesh.cellTime.cell.name isEqualToString:@"P3"]) {
                                        resultB = NSOrderedSame;
                                    }
                                }else if ([mutString isEqualToString:@" C"]) {
                                    if ([mesh.cellTime.cell.name isEqualToString:@"P1"] ||[mesh.cellTime.cell.name isEqualToString:@"P2"]) {
                                        resultB = NSOrderedSame;
                                    }
                                }else if ([mutString isEqualToString:@" P4"]) {
                                    if ([mesh.cellTime.cell.name isEqualToString:@"P1"] ||[mesh.cellTime.cell.name isEqualToString:@"P2"] ||[mesh.cellTime.cell.name isEqualToString:@"P3"]) {
                                        resultB = NSOrderedSame;
                                    }
                                }else if ([mutString isEqualToString:@"P3"]) {
                                    if ([mesh.cellTime.cell.name isEqualToString:@"P1"] ||[mesh.cellTime.cell.name isEqualToString:@"P2"]) {
                                        resultB = NSOrderedSame;
                                    }
                                }
                                if (resultB == NSOrderedSame) {
                                    cellAdded = YES;
                                }
                                [mutString deleteCharactersInRange:NSMakeRange(mutString.length - 1, 1)];
                            }
                        }
                    }
                }
                
                if (self.searchSettings.searchDescription == YES  && !cellAdded) {
                    if (mesh.cellTime.cell.descriptionOfCell.length > 1) {
                        NSString *descript = mesh.cellTime.cell.descriptionOfCell;
                        NSComparisonResult result = [descript compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                                                            range:[descript rangeOfString:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)]];
                        if (result == NSOrderedSame && searchString.length > 1) {
                            cellAdded = YES;
                        }
                    }
                }
                
                if (cellAdded == YES){
                    mesh.active = YES;//Will make the mesh tappable
                    [self.itemsInCurrentSearch addObject:mesh.cellTime.cell];//Add to the current search buffer
                    mesh.foundInSearch = NO;
                    mesh.posible = YES;
                }else{
                    //mesh.active = NO;//Will turn of tapping
                }
            }
        }
    }
}

-(void)searchVC:(SearchController *)searchController didGeneSearch:(Search *)search{    
    self.severalSearches = [NSArray arrayWithObject:search];
    self.searchText = nil;
}

-(NSArray *)lastSearch{
    return _itemsInCurrentSearch;
}

-(UIImage *)viewBehind{
    return [(GLKView *)[self view] snapshot];
}


-(void)searchVC:(SearchController *)searchController createSearch:(NSString *)searchString withColor:(UIColor *)color{
    
    self.searchText = nil;
    NSLog(@"Search string is %@", searchString);
    
    if (!_currentColorScheme) {//In the first use of the app
        self.currentColorScheme = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
        self.currentColorScheme.isCurrent = [NSNumber numberWithBool:YES];
    }
    
    for (Search *search in self.currentColorScheme.searches) {
        if (search.type.intValue == [self.searchSettings typeOfSearch]) {
            if (search.searchIn.intValue == [self.searchSettings propagationType]) {
                if ([searchString isEqualToString:search.searchString]) {
                    [[[UIAlertView alloc]initWithTitle:@"Search already exists" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                    return;
                }
            }
        }
    }
    
    self.individualSearch = nil;
    
    NSArray *componentsSearch;
    if (_searchSettings.searchDescription) {
        componentsSearch = [NSArray arrayWithObject:searchString];
    }else{
        NSString *cleanSearchString = [self stringProcessedForMultipleSearch:searchString];
        componentsSearch = [cleanSearchString componentsSeparatedByString:@","];
    }
    
    for (NSString *term in componentsSearch) {
        Search *search = [NSEntityDescription insertNewObjectForEntityForName:@"Search" inManagedObjectContext:self.managedObjectContext];
        search.theme = self.currentColorScheme;
        search.searchString = term;
        search.type = [NSString stringWithFormat:@"%i", [self.searchSettings typeOfSearch]];
        search.searchIn = [NSString stringWithFormat:@"%i", [self.searchSettings propagationType]];
        
        CGColorRef colorRef = [color CGColor];
        
        int numComponents = (int)CGColorGetNumberOfComponents(colorRef);
        
        if (numComponents == 4)
        {
            const CGFloat *components = CGColorGetComponents(colorRef);
            CGFloat red = components[0];
            CGFloat green = components[1];
            CGFloat blue = components[2];
            CGFloat alpha = components[3];
            search.color = [NSString stringWithFormat:@"%f,%f,%f,%f", red, green, blue, alpha];
        }
        
        NSMutableArray *foundCells = [self findCellsForTerm:term];
        
        for (Cell *cell in foundCells) {
            NSMutableSet *set = [NSMutableSet setWithSet:cell.searches];
            [set addObject:search];
            cell.searches = set;
        }
        [self.managedObjectContext save:nil];
    }
}

-(void)searchVC:(SearchController *)searchController didDissapearWithSearch:(NSString *)searchString withColor:(UIColor *)color{// andOptionsArray:(NSArray *)array{
    if (searchString) {
        self.searchText = searchString;
    }else{
        self.searchText = nil;
    }
}

#pragma mark slider

-(void)interactionBySlider:(id)sender{
    userInteraction = YES;
}

-(void)finishedInteractionBySlider:(id)sender{
    userInteraction = NO;
}

-(void)sliderBegan{
    userInteraction = YES;
}

-(void)sliderFinished{
    userInteraction = NO;
}

-(void)reloadWorm:(UISlider *)sender{
    
    if (timer) {
        [timer invalidate];
        UIButton *button = (UIButton *)[[self.toolbar.items objectAtIndex:0]customView];
        [button setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    }
    if ((int)sender.value != count) {
        
        count = (int)sender.value;
        if(FLY)count = 1;
        [self reuseMeshesWithTimePoint:count];
        timePointLabel.text = [NSString stringWithFormat:@"%i minutes", (int)(sliderTime.value)];
        if (FLY) {
            timePointLabel.hidden = YES;
        }
    }
}

-(void)fade:(UISlider *)sender{
    dimOthers = sender.value;
    if (_searchText) {
        [self searchVC:nil didSearchForCell:_searchText withFadeOthersValue:dimOthers];// andOptionsArray:_searchArray];
    }
}

#pragma mark send or save screen

-(void)saveScreen{
    //UIGraphicsBeginImageContext(self.view.bounds.size);
    //[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    //UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //UIGraphicsEndImageContext();
    UIImage *image = [(GLKView *)[self view] snapshot];
    UIImageWriteToSavedPhotosAlbum(image, self,
                                   @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error != NULL){
    }else{
        [[[UIAlertView alloc]initWithTitle:@"Image Saved" message:@"Image saved to your device's camera roll" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}



-(void)sendScreen:(UIBarButtonItem *)sender{
    
    UIActionSheet *actionSheet;
    actionSheet = [[UIActionSheet alloc]initWithTitle:@"Share" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Cancel" otherButtonTitles:
                                      @"Image",
                                      @"Scene",
                                      //@"Movie",
                                      @"Twitter",
                                      @"Facebook",
                                      nil];
    

    
    SEL selector = NSSelectorFromString(@"_alertController");
    if ([actionSheet respondsToSelector:selector])
    {
        UIAlertController *alertController = [actionSheet valueForKey:@"_alertController"];
        if ([alertController isKindOfClass:[UIAlertController class]])
        {
            alertController.view.backgroundColor = [UIColor darkGrayColor];
            alertController.view.tintColor = [UIColor whiteColor];
        }
    }
    else
    {
        // use other methods for iOS 7 or older.
    }
    
    actionSheet.tintColor = [UIColor grayColor];
    
    actionSheet.tag = 1;
    [actionSheet showFromBarButtonItem:sender animated:YES];//showFromRect:sender.frame inView:self.navigationController.navigationBar animated:YES];
}

-(void)sendVideo{
    if (!videoGen) {
        videoGen = [[PSVideoGenerator alloc]init];
    }
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
    //NSString *storePath = [documentsDirectory stringByAppendingPathComponent:@"TempVideo.mpg"];
    if (!videoGen.videoWriter || !videoGen.writerInput) {
        
        //[videoGen startWriterWithPath:storePath];
    }
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:maxTimePoint];
    count = 1;
    for (int x = 1; x < 30; x++) {
        NSLog(@"At image %i", x);
        //[self reuseMeshesWithTimePoint:x];
        [self forward:nil];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        //UIImage *image = [(GLKView *)[self view] snapshot];
        [array addObject:image];
        count ++;
        //CVPixelBufferRef buffer = [PSVideoGenerator pixelBufferFromCGImage:image.CGImage];
        //[videoGen writeBuffer:buffer toInput:videoGen.writerInput];
    }
    //[videoGen writeImageAsMovie:array toPath:storePath size:self.view.bounds.size duration:MAXTIMEPOINT];
    [videoGen writeImagesAsMovie:array];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 1) {//Envelope
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
                [self sendEmail];
                break;
            case 2:
                [self sendSceneUrl];
                break;
            //case 3:
            //    [self sendVideo];
            //    break;
            case 3:
                [self sendSocialTweet0FB1:0];
                break;
            case 4:
                [self sendSocialTweet0FB1:1];
                break;
            default:
                break;
        }
    }
}

-(void)sendSocialTweet0FB1:(int)networkCode{
    if([SLComposeViewController isAvailableForServiceType:networkCode==0?SLServiceTypeTwitter:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled)
                NSLog(@"Cancelled");
            else
                NSLog(@"Done");
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        if (WORM) {
            [controller setInitialText:@"Check out this C.elegans image, generated with #WormGUIDES for iOS. Comments:"];
        }
        if (FLY) {
            [controller setInitialText:@"Check out this D.melanogaster image, generated with #FlyGUIDES for iOS. Comments:"];
        }
        
        [controller addURL:[NSURL URLWithString:[PSSceneGenerator generateSceneForView:self withCount:(int)self.sliderTime.value distance:distance rotation:_rotation translation:(CGPoint)translation andDimValue:dimOthers]]];
        UIImage *image = [(GLKView *)[self view] snapshot];
        [controller addImage:image];
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        NSString *account = @"Activate ";
        account = [account stringByAppendingString:networkCode == 0?@"Twitter":@"Facebook"];
        account = [account stringByAppendingString:@" Account"];
         [[[UIAlertView alloc]initWithTitle:account message:@"Go to the phone settings and login with a valid twitter account to activate this feature" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}


-(void)sendEmail{
    UIImage *image = [(GLKView *)[self view] snapshot];
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
    
    [picker setSubject:@"iWorm image"];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    NSString *name;
    if (self.currentColorScheme.name.length > 0) {
        name = self.currentColorScheme.name;
    }else{
        name = @"Image_From_iWorm";
    }
    
    [picker addAttachmentData:imageData mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"%@.jpg", name]];
    
    picker.navigationBar.barStyle = UIBarStyleBlack; // choose your style, unfortunately, Translucent colors behave quirky.
    
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)sendSceneUrl{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
	
	[picker setSubject:[@"Sharing a scene from " stringByAppendingString:WORM?@"WormGUIDES":@"FlyGUIDES"]];
	
	picker.navigationBar.barStyle = UIBarStyleBlack; // choose your style, unfortunately, Translucent colors behave quirky.
    
	[picker setMessageBody:[PSSceneGenerator generateSceneForView:self withCount:(int)self.sliderTime.value distance:distance rotation:_rotation  translation:(CGPoint)translation andDimValue:dimOthers] isHTML:YES];
    
	[self presentViewController:picker animated:YES completion:nil];
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
			
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
		}
			
			break;
	}
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark popover

/*-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    if ([popoverController.contentViewController isMemberOfClass:[SearchController class]]) {
        return NO;
    }else{
        return YES;
    }
}*/

-(void)popoverController:(UIPopoverController *)popoverController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing *)view{
    userInteraction = YES;
}

-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion{
    [super dismissViewControllerAnimated:flag completion:completion];
    userInteraction = NO;
}

#pragma mark Lineage Delegate

-(void)checkPreviousTheme{
    NSLog(@"Number of searches in current %lu", (unsigned long)self.currentColorScheme.searches.count);
    if (self.currentColorScheme) {
        if (self.currentColorScheme.searches.count == 0) {
            NSLog(@"Will delete name %@", self.currentColorScheme.name);
            [self.managedObjectContext deleteObject:self.currentColorScheme];
        }else if(self.currentColorScheme.name.length == 0 && self.currentColorScheme.searches.count > 0){
            [[[UIAlertView alloc]initWithTitle:@"Un-named profile" message:@"Name and save the current profile before other previous profiles" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }else{
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ColorScheme"];
            request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
            NSArray *allThemes = [self.managedObjectContext executeFetchRequest:request error:nil];
            
            for (ColorScheme *theme in allThemes) {
                theme.isCurrent = [NSNumber numberWithBool:NO];
            }
        }
    }
}

-(void)lineageVC:(LineageTracing *)lineageTVC didChooseTheme:(ColorScheme *)theme{
    
    //[self checkPreviousTheme];
    self.individualSearch = nil;
    self.currentColorScheme.isCurrent = [NSNumber numberWithBool:NO];
    self.severalSearches = nil;
    theme.isCurrent = [NSNumber numberWithBool:YES];
    self.currentColorScheme = theme;
    [self.managedObjectContext save:nil];
}

-(void)lineageVC:(LineageTracing *)lineageTVC didSaveCurrentWithName:(NSString *)name{
    self.currentColorScheme.name = name;
    [self.managedObjectContext save:nil];
}

-(void)lineageVC:(LineageTracing *)lineageTVC didSaveAsWithName:(NSString *)name{
    NSMutableSet *set = [NSMutableSet setWithCapacity:self.currentColorScheme.searches.count];
    for (Search *search in self.currentColorScheme.searches) {
        Search *newSearch = [NSEntityDescription insertNewObjectForEntityForName:@"Search" inManagedObjectContext:self.managedObjectContext];
        newSearch.searchString = search.searchString;
        newSearch.propagationType = search.propagationType;
        newSearch.color = search.color;
        newSearch.type = search.type;
        newSearch.searchIn = search.searchIn;//0 in pro
        newSearch.cells = search.cells;
        [set addObject:newSearch];
    }
    
    NSLog(@"copied searches %lu", (unsigned long)set.count);
    
    ColorScheme *newColorScheme = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
    newColorScheme.name = name;
    newColorScheme.searches = set;
    self.currentColorScheme.isCurrent = [NSNumber numberWithBool:NO];
    newColorScheme.isCurrent = [NSNumber numberWithBool:YES];
    self.currentColorScheme = newColorScheme;
    [self.managedObjectContext save:nil];
}


-(void)lineageVC:(LineageTracing *)lineageTVC create:(int)schemeType WithName:(NSString *)name{
    
    self.currentColorScheme.isCurrent = [NSNumber numberWithBool:NO];
    self.severalSearches = nil;
    if (schemeType == 0) {
        ColorScheme *new = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
        self.currentColorScheme = new;
    }else if (schemeType == 1){
        [self createPresetTheme];
    }
    self.currentColorScheme.isCurrent = [NSNumber numberWithBool:YES];
    self.currentColorScheme.name = name;
    [self.managedObjectContext save:nil];
    [self reuseMeshesWithTimePoint:maxTimePoint];
}

//Memory management methods
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

#pragma mark legend protocol

-(void)searchSelected:(Search *)search{
    
    self.severalSearches = nil;
    if (self.individualSearch == nil || self.individualSearch != search) {
        self.individualSearch = search;
    }else{
        self.individualSearch = nil;
    }
}

-(void)several:(NSArray *)searches{
    
    self.severalSearches = searches;
    self.individualSearch = nil;
}

-(void)fadeValue:(float)fadevalue{
    dimOthers = fadevalue;
}

@end
