//
//  Cell.m
//  WormGUIDES
//
//  Created by Raul Catena on 6/7/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import "Cell.h"
#import "Cell.h"
#import "CellTime.h"
#import "ColorScheme.h"
#import "Search.h"


@implementation Cell

@dynamic descriptionOfCell;
@dynamic idTag;
@dynamic name;
@dynamic nameProper;
@dynamic predecedor;
@dynamic predecesorUniqueID;
@dynamic sucessorOne;
@dynamic sucessorOneUniqueID;
@dynamic sucessorTwo;
@dynamic sucessorTwoUniqueID;
@dynamic uniqueID;
@dynamic cellTimes;
@dynamic searches;
@dynamic themes;
@dynamic predecesor;
@dynamic successors;

@synthesize mesh;

@end
