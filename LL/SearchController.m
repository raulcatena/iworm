//
//  UISearchController.m
//  iWorm
//
//  Created by Raul Catena on 4/15/13.
//
//

#import "SearchController.h"
#import "PSAppDelegate.h"
#import "InfoViewcontrollerViewController.h"
#import "ColorScheme.h"
#import "Search.h"
#import "Cell.h"
#import "CellTime.h"
#import <QuartzCore/QuartzCore.h>
#import "HTMLParser.h"

#define INITIAL_HEIGHT 255
#define COMPOUNDTABLE_WIDTH 500
#define ALERT_TO_ERASE_ALL 10

@interface SearchController ()

@end

@implementation SearchController




-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andDelegate:(id<SearchCellProtocol>)aDelegate{
    self = [self initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        self.delegate = aDelegate;
    }
    return self;
}



-(NSManagedObjectContext *)managedObjectContext{
    PSAppDelegate *appDelegate = (PSAppDelegate *)[[UIApplication sharedApplication]delegate];
    return appDelegate.managedObjectContext;
}

#pragma mark housekeeping

-(PSSettingsSingleton *)searchSettings{
    if (!_searchSettings) {
        _searchSettings = [PSSettingsSingleton sharedInstance];
    }
    return _searchSettings;
}

-(void)bugSpecificInit{
    if (FLY) {
        _searchCellSwitch.hidden = YES;
        _searchCellLabel.hidden = YES;
        _searchAncestorsSwitch.hidden = YES;
        _searchAncestorsLabel.hidden = YES;
        _searchDescendantsSwitch.hidden = YES;
        _searchDescendantsLabel.hidden = YES;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _searchField.text = self.previousSearchString;
    self.preferredContentSize = CGSizeMake(self.view.bounds.size.width, INITIAL_HEIGHT);
    
    compoundClosed = YES;
    
    [self placeColorButtonsUpPanel];
    [self bugSpecificInit];
    
    self.resultsTable.separatorColor = [UIColor clearColor];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        [self toogleIphoneTabs:self.iphoneOpts];
        [(UIScrollView *)self.view setContentSize:self.view.bounds.size];
    }
    
    [self searchSettings];
    [self createSearchOptions];
    [self refreshSearchOptions];
    
    self.screenName = @"SEARCH";
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    [self adjustIOS7];
    [self correctIOS6buttons];
    
    if(self.searchField.text.length > 0){
        [self searchedCell:self.searchField];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
	{
        return;
    }
    [self.searchField becomeFirstResponder];
}

-(void)adjustIOS7{
    if ([[[[[UIDevice currentDevice] systemVersion]
           componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

-(void)correctIOS6buttons{
    if([[[[[UIDevice currentDevice] systemVersion]
          componentsSeparatedByString:@"."] objectAtIndex:0] intValue] < 7){
        CGFloat offset = -35;
        self.searchAncestorsSwitch.frame = CGRectOffset(self.searchAncestorsSwitch.frame, offset, 0);
        self.searchCellSwitch.frame = CGRectOffset(self.searchCellSwitch.frame, offset, 0);
        self.searchDescendantsSwitch.frame = CGRectOffset(self.searchDescendantsSwitch.frame, offset, 0);
        self.searchProperSwitch.frame = CGRectOffset(self.searchProperSwitch.frame, offset, 0);
        self.searchSystSwitch.frame = CGRectOffset(self.searchSystSwitch.frame, offset, 0);
        self.searchDescriptSwitch.frame = CGRectOffset(self.searchDescriptSwitch.frame, offset, 0);
        self.searchGeneSwitch.frame = CGRectOffset(self.searchGeneSwitch.frame, offset, 0);
    }
}

-(void)refreshSearchOptions{
    self.searchCellSwitch.on = self.searchSettings.searchCell;
    self.searchAncestorsSwitch.on = self.searchSettings.searchAncestors;
    self.searchDescendantsSwitch.on = self.searchSettings.searchDescendants;
    self.searchSystSwitch.on = self.searchSettings.searchSystematic;
    self.searchProperSwitch.on = self.searchSettings.searchProper;
    self.searchDescriptSwitch.on = self.searchSettings.searchDescription;
    self.searchGeneSwitch.on = self.searchSettings.searchGene;
    
    self.plusButton.hidden = self.searchSettings.searchGene;
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    NSString *text = nil;
    if (self.searchField.text.length > 0) {
        text = self.searchField.text;
    }
    if (self.searchSettings.searchGene && !confirmGeneSearch) {
        _currentSearch.theme = nil;
        [self.managedObjectContext save:nil];
    }
    
    [self.delegate searchVC:self didDissapearWithSearch:text withColor:nil];// andOptionsArray:nil];
}

#pragma mark Panels and viewables

-(void)addColorPanel{
    NSArray *colorArray = [NSArray arrayWithObjects:[UIColor brownColor], [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1], [UIColor orangeColor], [UIColor purpleColor], [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1], [UIColor magentaColor],[UIColor cyanColor], [UIColor blueColor], [UIColor yellowColor], [UIColor greenColor], [UIColor redColor], [UIColor colorWithRed:1 green:1 blue:1 alpha:1], nil];
    int y = -1;
    for (int x = 0; x<12; x++) {
        int z = x%3;
        if (z == 0) {
            y++;
        }
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(buttonToggle:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = self.colorDownPanel.frame;
        button.layer.cornerRadius = button.bounds.size.width/2;
        button.layer.borderWidth = 1;
        button.layer.borderColor = [UIColor darkGrayColor].CGColor;
        button.backgroundColor =[colorArray objectAtIndex:x];
        button.frame = CGRectOffset(button.frame, (button.bounds.size.width + 5)*z, (button.bounds.size.height + 5)*y);
        [self.view addSubview:button];
    }
}

-(void)toggleCompoundSearch:(UIButton *)sender{
    if (sender.tag == 0) {
        self.preferredContentSize = CGSizeMake(self.view.bounds.size.width, INITIAL_HEIGHT * 3);;
        [self addColorPanel];
        [sender setTitle:[[sender.titleLabel.text substringToIndex:sender.titleLabel.text.length-1]stringByAppendingString:@"^"] forState:UIControlStateNormal];
    }else{
        self.preferredContentSize = CGSizeMake(self.view.bounds.size.width, INITIAL_HEIGHT);
        [sender setTitle:[[sender.titleLabel.text substringToIndex:sender.titleLabel.text.length-1]stringByAppendingString:@">"] forState:UIControlStateNormal];
    }
    sender.tag = (int)!(BOOL)sender.tag;
}

-(void)toggleResultsList:(UIButton *)sender{
    if (self.resultsTable.hidden) {
        self.resultsTable.hidden = NO;
        [self.toogleListOfResults setTitle:@"Hide results" forState:UIControlStateNormal];
    }else{
        self.resultsTable.hidden = YES;
        [self.toogleListOfResults setTitle:@"Show results" forState:UIControlStateNormal];
    }
}

//Refactor this
-(void)createSearchOptions{
    
    if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        _searchProperSwitch.tag = 1;
        _searchProperSwitch.on = self.searchSettings.searchProper;
        _searchSystSwitch.tag = 2;
        _searchSystSwitch.on = self.searchSettings.searchSystematic;
        _searchDescriptSwitch.tag = 3;
        _searchDescriptSwitch.on = self.searchSettings.searchDescription;
        _searchGeneSwitch.tag = 4;
        _searchGeneSwitch.on = self.searchSettings.searchDescription;
    }
    
    if (!self.searchOptions) {
        UIView *aView = [[UIView alloc]initWithFrame:self.resultsTable.frame];
        aView.backgroundColor = [UIColor clearColor];
        
        float switchpositioninx = aView.bounds.size.width - 100;
        if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            switchpositioninx = switchpositioninx + 20;
        }
        float height = 20.0f;
        UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(10, height, 205, 25)];
        if (WORM) {
            label1.text = @"Search Functional Names";
        }else if(FLY){
            label1.text = @"Search Cell Names";
        }
        
        label1.backgroundColor = [UIColor clearColor];
        label1.textColor = [UIColor whiteColor];
        [aView addSubview:label1];
        self.searchProperSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(switchpositioninx, height, 30, 25)];
        _searchProperSwitch.thumbTintColor = [UIColor blueColor];
        _searchProperSwitch.onTintColor = [UIColor whiteColor];
        _searchProperSwitch.tag = 1;
        _searchProperSwitch.userInteractionEnabled = YES;
        _searchProperSwitch.on = self.searchSettings.searchProper;
        [_searchProperSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        [aView addSubview:_searchProperSwitch];
        height = height + 40.0f;
        
        if (WORM) {
            UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(10, height, 205, 25)];
            label2.text = @"Search Lineage Names";
            label2.backgroundColor = [UIColor clearColor];
            label2.textColor = [UIColor whiteColor];
            [aView addSubview:label2];
            self.searchSystSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(switchpositioninx, height, 30, 25)];
            _searchSystSwitch.thumbTintColor = [UIColor blueColor];
            _searchSystSwitch.onTintColor = [UIColor whiteColor];
            _searchSystSwitch.tag = 2;
            _searchSystSwitch.on = self.searchSettings.searchSystematic;
            [_searchSystSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
            [aView addSubview:_searchSystSwitch];
            height = height + 40.0f;
        }
        
        
        UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(10, height, 205, 25)];
        label3.text = @"Search Description";
        label3.backgroundColor = [UIColor clearColor];
        label3.textColor = [UIColor whiteColor];
        [aView addSubview:label3];
        self.searchDescriptSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(switchpositioninx, height, 30, 25)];
        _searchDescriptSwitch.thumbTintColor = [UIColor blueColor];
        _searchDescriptSwitch.onTintColor = [UIColor whiteColor];
        _searchDescriptSwitch.tag = 3;
        _searchDescriptSwitch.on = self.searchSettings.searchDescription;
        [_searchDescriptSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        [aView addSubview:_searchDescriptSwitch];
        height = height + 40.0f;
        
        UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(10, height, 205, 25)];
        label4.text = @"Gene expression";
        label4.backgroundColor = [UIColor clearColor];
        label4.textColor = [UIColor whiteColor];
        [aView addSubview:label4];
        self.searchGeneSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(switchpositioninx, height, 30, 25)];
        _searchGeneSwitch.thumbTintColor = [UIColor blueColor];
        _searchGeneSwitch.onTintColor = [UIColor whiteColor];
        _searchGeneSwitch.tag = 4;
        _searchGeneSwitch.on = self.searchSettings.searchDescription;
        [_searchGeneSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        [aView addSubview:_searchGeneSwitch];
        if (FLY) {
            _searchGeneSwitch.userInteractionEnabled = NO;
            _searchGeneSwitch.alpha = 0.5;
        }
        
        self.searchOptions = aView;
        self.toogleListOfResults.hidden = YES;
        [self.view addSubview:_searchOptions];
        self.searchOptions.clipsToBounds = YES;
        self.searchOptions.hidden = NO;
        return;
    }
}

-(IBAction)toogleIphoneTabs:(UISegmentedControl *)sender{
    
    if (sender.selectedSegmentIndex == 0) {
        [self.compoundTable removeFromSuperview];
        [self.view addSubview:self.resultsTable];
    }else if(sender.selectedSegmentIndex == 1){
        [self.compoundTable removeFromSuperview];
        [self.resultsTable removeFromSuperview];
    }else{
        [self.view addSubview:self.compoundTable];
        [self.resultsTable removeFromSuperview];
    }
}

-(void)toggleSearchOptions:(UIButton *)sender{
    
    if (self.searchOptions.hidden) {
        if (self.resultsTable.hidden == NO) {
            self.resultsTable.hidden = YES;
        }
        self.toogleListOfResults.hidden = YES;
        self.searchOptions.hidden = NO;
    }else{
        self.resultsTable.hidden = NO;
        self.toogleListOfResults.hidden = NO;
        self.searchOptions.hidden = YES;
    }
}

-(void)updateBadgeAndThumb{
    int results = (int)[[self.delegate lastSearch]count];
    if (results > 0) {
        self.badge.text = [NSString stringWithFormat:@"%i", results];
        self.badge.backgroundColor = [UIColor redColor];
        self.badge.layer.borderColor = [UIColor whiteColor].CGColor;
        self.badge.layer.borderWidth = 2.5f;
        self.badge.layer.cornerRadius = self.badge.bounds.size.height/2;
        self.badge.textAlignment = NSTextAlignmentCenter;
        if (results > 99) {
            self.badge.bounds = CGRectMake(0, 0, 40, self.badge.bounds.size.height);
        }else if (results > 9){
            self.badge.bounds = CGRectMake(0, 0, 35, self.badge.bounds.size.height);
        }else{
            self.badge.bounds = CGRectMake(0, 0, 27, self.badge.bounds.size.height);
        }
    }else{
        self.badge.text = nil;
        self.badge.backgroundColor = [UIColor clearColor];
        self.badge.layer.borderColor = [UIColor clearColor].CGColor;
        self.badge.layer.borderWidth = 0;
    }
    UIImage *image = [self.delegate viewBehind];
    UIImageView *iv = [[UIImageView alloc]initWithImage:image highlightedImage:image];
    iv.bounds = CGRectMake(0, 0, 60, 60);
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc]initWithCustomView:iv]];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignCurrent)];
    [self.view addGestureRecognizer:tap];
}

#pragma mark search/related functions


-(BOOL)validateSearchBox{
    if (_searchField.text.length == 0) {
        return NO;
    }
    if (self.searchSettings.searchGene) {
        if ([_searchField.text componentsSeparatedByString:@" "].count > 1) {
            [[[UIAlertView alloc]initWithTitle:@"Remove space(s)" message:@"Only one search term can be added at at time" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
            return NO;
        }
    }
    if (!self.selectedButtonForColor) {
        [[[UIAlertView alloc]initWithTitle:@"No color selected" message:@"You must select one color first" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        [self.searchField becomeFirstResponder];
        return NO;
    }
    return YES;
}

-(void)searchedCell:(UITextField *)sender{
    if (sender.text.length == 0 || _searchSettings.searchGene) {
        return;
    }
    if (sender.text.length % 2 == 0 && _searchSettings.searchDescription && sender.text.length < 10) {
        return;
    }
    [self.delegate searchVC:self didSearchForCell:sender.text withFadeOthersValue:self.fadeSlider.value];
    [self updateBadgeAndThumb];
    [self.resultsTable reloadData];
}



-(void)searchGene:(UITextField *)sender{
    if (!_searchSettings.searchGene) {
        return;
    }
    if (![self validateSearchBox]) {
        return;
    }
    [self addSpinner];
    
    confirmGeneSearch = NO;
    if (!confirmGeneSearch) {
        _currentSearch.theme = nil;
    }

    if (!database) {
        database = [[PSDataBaseUtility alloc]init];
    }
    if(!database.delegate)database.delegate = self;
    
    Search *search = [NSEntityDescription insertNewObjectForEntityForName:@"Search" inManagedObjectContext:self.managedObjectContext];
    search.theme = self.currentColorScheme;
    search.searchString = sender.text;
    search.searchIn = @"Gene";
    [self.managedObjectContext save:nil];
    
    [database doGeneSearchWithTermToRule:search withColor:self.selectedButtonForColor.backgroundColor];
    [self.delegate searchVC:self didGeneSearch:search];
    
    self.searchField.text = nil;
}

-(void)addSpinner{
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    spinner.tag = 2000;
    spinner.frame = CGRectMake(self.view.bounds.size.width - 170, 205, 50, 50);
    [spinner startAnimating];
    
    [self.view setNeedsDisplay];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(self.view.bounds.size.width - 520, 205, 350, 50)];
    label.tag =  2000;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentRight;
    [label setText:[NSString stringWithFormat:@"Searching online for cells expressing %@", self.searchField.text]];
    
    if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        label.frame = CGRectMake(0, self.view.bounds.size.height - 60, self.view.bounds.size.width - 30, 60);
        label.numberOfLines = 2;
        spinner.frame = CGRectMake(self.view.bounds.size.width - 30, self.view.bounds.size.height - 30, 30, 30);
    }
    
    [self.view addSubview:spinner];
    [self.view addSubview:label];
}

-(void)doneSearching{
    for (UIView *view in self.view.subviews) {
        if (view.tag == 2000) {
            [view removeFromSuperview];
        }
    }
}


-(void)fade:(UISlider *)sender{
    if (self.searchField.text.length > 0) {
        [self.delegate searchVC:self didSearchForCell:self.searchField.text withFadeOthersValue:sender.value];
    }

}

-(void)addSearchToColorScheme:(UIButton *)sender{
    if (![self validateSearchBox]) {
        return;
    }
    if (_searchSettings.searchGene) {
        if (_currentSearch) {
            confirmGeneSearch = YES;
            _currentSearch.theme = self.currentColorScheme;
            [[self managedObjectContext]save:nil];
            //self.currentSearch = nil;
            self.searchField.text = nil;
        }
        self.searchField.text = nil;
        return;
    }
    
    if (![self.searchSettings areOptionsValid]) {
        [[[UIAlertView alloc]initWithTitle:@"Select at least one search option" message:@"Cell, Ancestors, and/or Descendants" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        return;
    }
    [self.delegate searchVC:self createSearch:self.searchField.text withColor:self.selectedButtonForColor.backgroundColor];
    self.searchField.text = nil;
    [self.resultsTable reloadData];
    [self.compoundTable reloadData];
}



-(void)resignCurrent{
    [self.searchField resignFirstResponder];
}

-(void)switchChanged:(UISwitch *)sender{
    switch (sender.tag) {
        case 1:
            self.searchSettings.searchProper = sender.isOn;
            break;
        case 2:
            self.searchSettings.searchSystematic = sender.isOn;
            break;
        case 3:
            self.searchSettings.searchDescription = sender.isOn;
            break;
        case 4:{
            self.searchSettings.searchDescription = NO;
            self.searchSettings.searchSystematic = NO;
            self.searchSettings.searchProper = NO;
            self.searchSettings.searchGene = sender.isOn;}
            break;
        case 5:
            self.searchSettings.searchCell = sender.isOn;
            break;
        case 6:
            self.searchSettings.searchDescendants = sender.isOn;
            break;
        case 7:
            self.searchSettings.searchAncestors = sender.isOn;
            break;
        default:
            break;
    }
    
    [self refreshSearchOptions];
    if([self.searchSettings areOptionsValid])
    [self searchedCell:self.searchField];
    
}

-(void)placeColorButtonsUpPanel{
    self.colorUpPanel.layer.cornerRadius = self.colorUpPanel.bounds.size.width/2;
    self.colorUpPanel.layer.borderWidth = 1;
    self.colorUpPanel.backgroundColor = [UIColor redColor];
    [self.colorUpPanel addTarget:self action:@selector(buttonToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    NSArray *colorArray = [NSArray arrayWithObjects:[UIColor blueColor], [UIColor yellowColor], [UIColor greenColor], [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1], [UIColor colorWithRed:1 green:1 blue:1 alpha:1], [UIColor orangeColor], nil];
    
    int numColor = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone?5:6;
    for (int x = 0; x<numColor; x++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(buttonToggle:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = self.colorUpPanel.frame;
        button.layer.cornerRadius = button.bounds.size.width/2;
        button.layer.borderWidth = 1;
        button.layer.borderColor = [UIColor darkGrayColor].CGColor;
        button.backgroundColor =[colorArray objectAtIndex:x];
        button.frame = CGRectOffset(button.frame, (button.bounds.size.width + 5)*(x+1), 0);
        [self.view addSubview:button];
    }
}

-(void)buttonToggle:(UIButton *)sender{
    
    if (sender.isSelected) {
        sender.selected = NO;
        sender.layer.borderColor = [UIColor darkGrayColor].CGColor;
        sender.layer.borderWidth = 1;
    }else{
        if (self.currentSearch != nil) {
            CGColorRef colorRef = [sender.backgroundColor CGColor];
            
            int numComponents = (int)CGColorGetNumberOfComponents(colorRef);
            
            if (numComponents == 4)
            {
                const CGFloat *components = CGColorGetComponents(colorRef);
                CGFloat red = components[0];
                CGFloat green = components[1];
                CGFloat blue = components[2];
                CGFloat alpha = components[3];
                self.currentSearch.color = [NSString stringWithFormat:@"%f,%f,%f,%f", red, green, blue, alpha];
            }
            [self.compoundTable reloadData];
            return;
        }
        
        if (_selectedButtonForColor) {
            _selectedButtonForColor.selected = NO;
            _selectedButtonForColor.layer.borderWidth = 1;
            _selectedButtonForColor.layer.borderColor = [UIColor grayColor].CGColor;
        }
        sender.selected = YES;
        sender.layer.borderColor = [UIColor whiteColor].CGColor;
        sender.layer.borderWidth = 4;        
        self.selectedButtonForColor = sender;
    }
}

-(void)help{NSLog(@"AAAA");
    if (self.popover.isPopoverVisible) {
        [self.popover dismissPopoverAnimated:YES];
        return;
    }
    InfoViewcontrollerViewController *infoV = [[InfoViewcontrollerViewController alloc]init];
    infoV.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
    UIWebView *wv = [[UIWebView alloc]initWithFrame:infoV.view.frame];
    wv.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"helpsearch" withExtension:@"htm"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    [wv loadRequest:request];
    
    [infoV.view addSubview:wv];
    if (WORM) {
        infoV.title = @"WormGUIDES help";
    }
    if (FLY) {
        infoV.title = @"FlyGUIDES help";
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        infoV.navigationItem.leftBarButtonItems = @[[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)]];
        UINavigationController *navcon = [[UINavigationController alloc]initWithRootViewController:infoV];
        navcon.navigationBar.barStyle = UIBarStyleBlackOpaque;
        [self.presentingViewController presentViewController:navcon animated:YES completion:nil];
        return;
    }
    self.popover = [[UIPopoverController alloc]initWithContentViewController:infoV];
    
    self.popover.contentViewController = infoV;
    [self.popover presentPopoverFromRect:_plusButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark Tableview
#pragma DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows;
    if (tableView ==  self.resultsTable) {
        rows = (int)[[self.delegate lastSearch]count];
        if (rows == 0) {
            rows = 3;
        }
    }else{
        if (self.currentColorScheme.searches.count > 0) {
            rows = (int)self.currentColorScheme.searches.count + 1;
        }else{
            rows = 1;
        }
    }
    return rows;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    if(tableView == self.resultsTable){
        if ([self.delegate lastSearch].count == 0) {
            if (indexPath.row == 2) {
                cell.textLabel.text = @"No results found";
            }else{
                cell.textLabel.text = nil;
            }
            cell.detailTextLabel.text = nil;
        }else{
            //cell.textLabel.text = [(CellTime *)[[self.delegate lastSearch]objectAtIndex:indexPath.row]cell].name;//When only results in current timepoint where shown
            Cell *bioCell = (Cell *)[[self.delegate lastSearch]objectAtIndex:indexPath.row];
            cell.textLabel.text = bioCell.name;
            NSString *details = [NSString stringWithFormat:@"%@", bioCell.nameProper];
            if (bioCell.nameProper.length  != 0) {
                cell.detailTextLabel.text = details;
            //}else if(bioCell.descriptionOfCell.length > 0){
                //cell.detailTextLabel.text = [cell.detailTextLabel.text stringByAppendingString:bioCell.descriptionOfCell];
            }else{
                cell.detailTextLabel.text = nil;
            }
        }
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.detailTextLabel.textColor = [UIColor whiteColor];
    }else{
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Others";
            cell.detailTextLabel.text = nil;
            cell.textLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1];
        }else{
            NSArray *array = self.currentColorScheme.searches.allObjects;
            Search *search = (Search *)[array objectAtIndex:indexPath.row - 1];
            [[[PSDataBaseUtility alloc]init]formatCell:cell withSearch:search];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.resultsTable) {
        if ([self.delegate lastSearch].count == 0) {
            return;
        }
        for (int x = 0; x<[self.delegate lastSearch].count; x++) {
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:x inSection:0]];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        Cell *cell = (Cell *)[[self.delegate lastSearch]objectAtIndex:indexPath.row];
        NSString *partsMessage;
        if (cell.nameProper.length > 0 || cell.descriptionOfCell.length > 0) {
            partsMessage = [NSString stringWithFormat:@"%@\r%@", cell.nameProper, cell.descriptionOfCell];
        }else{
            partsMessage = @"This is not a terminal cell, hence, no parts list data is available";
        }
        
        self.searchField.text = cell.name;
        [self.delegate searchVC:self didSearchForCell:cell.name withFadeOthersValue:self.fadeSlider.value];
        //[self.resultsTable reloadData];
        [[[UIAlertView alloc]initWithTitle:cell.name message:partsMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }else{
        if (indexPath.row == 0) {
            return;
        }
        for (int x = 0; x<self.currentColorScheme.searches.allObjects.count; x++) {
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:x inSection:0]];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            self.currentSearch = nil;
        }else{
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            NSArray *array = self.currentColorScheme.searches.allObjects;
            Search *search = (Search *)[array objectAtIndex:indexPath.row -1];
            self.currentSearch = search;
        }
    }
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.compoundTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.compoundTable;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_compoundTable endUpdates];
}


-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return @"Delete all searches";
    }else{
        return @"Delete";
    }
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete && tableView == _compoundTable) {
        
        if (indexPath.row == 0) {
            if (self.currentColorScheme.searches.count > 0) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"This action can not be undone" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Erase", nil];
                alert.tag = ALERT_TO_ERASE_ALL;
                [alert show];
            }
        }else{
            // Delete the row from the data source
            //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            NSArray *array = self.currentColorScheme.searches.allObjects;
            Search *search = (Search *)[array objectAtIndex:indexPath.row-1];
            
            [self.managedObjectContext deleteObject:search];
            [self.managedObjectContext save:nil];
            [self.compoundTable reloadData];
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == ALERT_TO_ERASE_ALL) {NSLog(@"here");
        if (buttonIndex == 1) {NSLog(@"rehere");
            for (Search *search in self.currentColorScheme.searches) {
                [self.managedObjectContext deleteObject:search];
            }
            [self.managedObjectContext save:nil];
            [self.compoundTable reloadData];
        }
    }
}

@end
