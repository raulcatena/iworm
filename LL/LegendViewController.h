//
//  LegendViewController.h
//  iWorm
//
//  Created by Raul Catena on 5/9/13.
//
//

#import <UIKit/UIKit.h>

@class ColorScheme, Search;

@protocol LegendProtocol;

@interface LegendViewController : UITableViewController <UIAlertViewDelegate>

@property (nonatomic, strong) ColorScheme *currentColorScheme;
@property (nonatomic, weak) id<LegendProtocol>delegate;
@property (nonatomic, strong) NSMutableArray *severalSearches;

@property (nonatomic, strong) Search *missingGeneSearch;
@property (nonatomic, strong) UISlider *dimOthersSlider;

-(id)initWithStyle:(UITableViewStyle)style andCurrentColorScheme:(ColorScheme *)colorScheme andDimValue:(float)dimValue;

@end

@protocol LegendProtocol <NSObject>

-(void)searchSelected:(Search *)search;
-(void)several:(NSArray *)searches;
-(void)fadeValue:(float)fadevalue;
-(void)pin;
-(void)unpin;
-(UIImage *)viewBehind;

@end
