
			"AB	P0.a	Embryonic founder cell\n"+
			"ADAL	AB.plapaaaapp	Ring interneurons\n"+
			"ADAR	AB.prapaaaapp	Ring interneurons\n"+
			"ADEL	AB.plapaaaapa	Anterior deirid, sensory neuron, dopaminergic\n"+
			"ADER	AB.prapaaaapa	Anterior deirid, sensory neuron, dopaminergic\n"+
			"ADEshL	AB.arppaaaa	Anterior deirid sheath\n"+
			"ADEshR	AB.arpppaaa	Anterior deirid sheath\n"+
			"ADFL	AB.alpppppaa	Amphid neuron, prob. chemosensory\n"+
			"ADFR	AB.praaappaa	Amphid neuron, prob. chemosensory\n"+
			"ADLL	AB.alppppaad	Amphid neuron, prob. chemosensory\n"+
			"ADLR	AB.praaapaad	Amphid neuron, prob. chemosensory\n"+
			"AFDL	AB.alpppapav	Amphid finger cell, neuron associated with amphid sheath\n"+
			"AFDR	AB.praaaapav	Amphid finger cell, neuron associated with amphid sheath\n"+
			"AIAL	AB.plppaappa	Amphid interneuron\n"+
			"AIAR	AB.prppaappa	Amphid interneuron\n"+
			"AIBL	AB.plaapappa	Amphid interneuron\n"+
			"AIBR	AB.praapappa	Amphid interneuron\n"+
			"AIML	AB.plpaapppa	Ring interneuron\n"+
			"AIMR	AB.prpaapppa	Ring interneuron\n"+
			"AINL	AB.alaaaalal	Ring interneuron\n"+
			"AINR	AB.alaapaaar	Ring interneuron\n"+
			"AIYL	AB.plpapaaap	Amphid interneuron\n"+
			"AIYR	AB.prpapaaap	Amphid interneuron\n"+
			"AIZL	AB.plapaaapav	Amphid interneuron\n"+
			"AIZR	AB.prapaaapav	Amphid interneuron\n"+
			"ALA	AB.alapppaaa	Neuron, sends processes laterally and along dorsal cord\n"+
			"ALML	AB.arppaappa	Anterior lateral microtubule cell, touch receptor\n"+
			"ALMR	AB.arpppappa	Anterior lateral microtubule cell, touch receptor\n"+
			"ALNL	AB.plapappppap	Neuron associated with ALM\n"+
			"ALNR	AB.prapappppap	Neuron associated with ALM\n"+
			"AMshL	AB.plaapaap	Amphid sheath\n"+
			"AMshR	AB.praapaap	Amphid sheath\n"+
			"AMsoL	AB.plpaapapa	Amphid socket\n"+
			"AMsoR	AB.prpaapapa	Amphid socket\n"+
			"AQR	QR.ap	AB.prapapaaaap. Neuron, basal body. not part of a sensillum, projects into ring\n"+
			"AS1	P1.apa	AB.plapaappapa or AB.prapaappapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS2	P2.apa	AB.prapaappapa or AB.plapaappapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS3	P3.apa	AB.plappaaaapa or AB.prappaaaapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS4	P4.apa	AB.prappaaaapa or AB.plappaaaapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS5	P5.apa	AB.plappaapapa or AB.prappaapapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS6	P6.apa	AB.prappaapapa or AB.plappaapapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS7	P7.apa	AB.plappappapa or AB.prappappapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS8	P8.apa	AB.prappappapa or AB.plappappapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS9	P9.apa	AB.plapapapapa or AB.prapapapapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS10	P10.apa	AB.prapapapapa or AB.plapapapapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"AS11	P11.apa	AB.plapappaapa. Ventral cord motor neuron, innervates dorsal muscles, no ventral counterpart\n"+
			"ASEL	AB.alppppppaa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASER	AB.praaapppaa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASGL	AB.plaapapap	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASGR	AB.praapapap	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASHL	AB.plpaappaa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASHR	AB.prpaappaa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASIL	AB.plaapapppa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASIR	AB.praapapppa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASJL	AB.alpppppppa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASJR	AB.praaappppa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASKL	AB.alpppapppa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"ASKR	AB.praaaapppa	Amphid neurons, single ciliated endngs, probably chemo-sensory; project into ring via commissure from ventral ganglion, make diverse synaptic connections in ring neuropil\n"+
			"AUAL	AB.alpppppppp	Neuron, process runs with amphid processes but lacks cillialted ending\n"+
			"AUAR	AB.praaappppp	Neuron, process runs with amphid processes but lacks cillialted ending\n"+
			"AVAL	AB.alppaaapa	Ventral cord interneuron, synapses onto VA, DA, and AS motor neurons; formerly called alpha\n"+
			"AVAR	AB.alaappapa	Ventral cord interneuron, synapses onto VA, DA, and AS motor neurons; formerly called alpha\n"+
			"AVBL	AB.plpaapaap	Ventral cord interneuron, synapsesonto VB and DB motor neurons; formerly called beta\n"+
			"AVBR	AB.prpaapaap	Ventral cord interneuron, synapsesonto VB and DB motor neurons; formerly called beta\n"+
			"AVDL	AB.alaaapalr	Ventral cord interneuron, synapses onto VA, DA, AS motorneurons; formerly called delta\n"+
			"AVDR	AB.alaaapprl	Ventral cord interneuron, synapses onto VA, DA, AS motorneurons; formerly called delta\n"+
			"AVEL	AB.alpppaaaa	Ventral cord interneuron, like AVD but outputs restrict-ed to anterior cord\n"+
			"AVER	AB.praaaaaaa	Ventral cord interneuron, like AVD but outputs restrict-ed to anterior cord\n"+
			"AVFL/R	P1.aaaa	AB.plapaappaaaa or AB.prapaappaaaa. Interneuron, processes in ventral cord and ring, few synapses\n"+
			"AVFL/R	W.aaa	AB.prapaapaaaa. Interneuron, processes in ventral cord and ring, few synapses\n"+
			"AVG	AB.prpapppap	Ventral cord interneuron, few synapses\n"+
			"AVHL	AB.alapaaaaa	Neuron, mainly postsynaptic in ventral cord and presynaptic in the ring\n"+
			"AVHR	AB.alappapaa	Neuron, mainly postsynaptic in ventral cord and presynaptic in the ring\n"+
			"AVJL	AB.alapapppa	Neuron, synapses like AVHL/R\n"+
			"AVJR	AB.alapppppa	Neuron, synapses like AVHL/R\n"+
			"AVKL	AB.plpapapap	Ring and ventral cord interneuron\n"+
			"AVKR	AB.prpapapap	Ring and ventral cord interneuron\n"+
			"AVL	AB.prpappaap	Ring and ventral cord interneuron, few synapses\n"+
			"AVM	QR.paa	AB.prapapaaapaa. Anterior ventral microtubule cell, touch receptor\n"+
			"AWAL	AB.plaapapaa	Amphid wing cells, neurons having ciliated sheet-like sensory endings closely associated with amphid sheath\n"+
			"AWAR	AB.praapapaa	Amphid wing cells, neurons having ciliated sheet-like sensory endings closely associated with amphid sheath\n"+
			"AWBL	AB.alpppppap	Amphid wing cells, neurons having ciliated sheet-like sensory endings closely associated with amphid sheath\n"+
			"AWBR	AB.praaappap	Amphid wing cells, neurons having ciliated sheet-like sensory endings closely associated with amphid sheath\n"+
			"AWCL	AB.plpaaaaap	Amphid wing cells, neurons having ciliated sheet-like sensory endings closely associated with amphid sheath\n"+
			"AWCR	AB.prpaaaaap	Amphid wing cells, neurons having ciliated sheet-like sensory endings closely associated with amphid sheath\n"+
			"B	AB.prppppapa	Rectal cell, postembryonic blast cell in male\n"+
			"BAGL	AB.alppappap	Neuron, ciliated ending in head, no supporting cells, associated with ILso\n"+
			"BAGR	AB.arappppap	Neuron, ciliated ending in head, no supporting cells, associated with ILso\n"+
			"BDUL	AB.arppaappp	Neuron, process runs along excretory canal and into ring, unique darkly staining synaptic vesicles\n"+
			"BDUR	AB.arpppappp	Neuron, process runs along excretory canal and into ring, unique darkly staining synaptic vesicles\n"+
			"C	P0.ppa	Embryonic founder cell\n"+
			"CA1	P3.aapa	AB.plappaaaaapa or AB.prappaaaaapa. Male specific cell, not reconstructed\n"+
			"CA2	P4.aapa	AB.prappaaaaapa or AB.plappaaaaapa. Male specific cell, not reconstructed\n"+
			"CA3	P5.aapa	AB.plappaapaapa or AB.prappaapaapa. Male specific cell, not reconstructed\n"+
			"CA4	P6.aapa	AB.prappaapaapa or AB.plappaapaapa. Male specific neuron, innervates dorsal muscles\n"+
			"CA5	P7.aapa	AB.plappappaapa or AB.prappappaapa. Male specific neuron, innervates dorsal muscles\n"+
			"CA6	P8.aapa	AB.prappappaapa or AB.plappappaapa. Male specific neuron, innervates dorsal muscles\n"+
			"CA7	P9.aapa	AB.plapapapaapa or AB.prapapapaapa. Male specific neuron, innervates dorsal muscles\n"+
			"CA8	P10.aapa	AB.prapapapaapa or AB.plapapapaapa. Male specific cell in ventral cord, neuron-like but lacks synapses\n"+
			"CA9	P11.aapa	AB.plapappaaapa. Male specific cell in ventral cord, neuron-like but lacks synapses\n"+
			"CANL	AB.alapaaapa	Process runs along excretory canal, no synapses, essential for survival\n"+
			"CANR	AB.alappappa	Process runs along excretory canal, no synapses, essential for survival\n"+
			"CEMDL	AB.plaaaaaap	Male specific cephalic neurons (programmed cell death in hermaphrodite embryo) open to outside, possible function in male chemotaxis toward hermaphrodite.\n"+
			"CEMDR	AB.arpapaaap	Male specific cephalic neurons (programmed cell death in hermaphrodite embryo) open to outside, possible function in male chemotaxis toward hermaphrodite.\n"+
			"CEMVL	AB.plpaapapp	Male specific cephalic neurons (programmed cell death in hermaphrodite embryo) open to outside, possible function in male chemotaxis toward hermaphrodite.\n"+
			"CEMVR	AB.prpaapapp	Male specific cephalic neurons (programmed cell death in hermaphrodite embryo) open to outside, possible function in male chemotaxis toward hermaphrodite.\n"+
			"CEPDL	AB.plaaaaappa	Cephalic neurons, contain dopamine\n"+
			"CEPDR	AB.arpapaappa	Cephalic neurons, contain dopamine\n"+
			"CEPVL	AB.plpaappppa	Cephalic neurons, contain dopamine\n"+
			"CEPVR	AB.prpaappppa	Cephalic neurons, contain dopamine\n"+
			"CEPshDL	AB.arpaaaapp	Cephalic sheath, sheet-like processes envelop meuropil of the ring and part of ventral ganglion\n"+
			"CEPshDR	AB.arpaaapap	Cephalic sheath, sheet-like processes envelop meuropil of the ring and part of ventral ganglion\n"+
			"CEPshVL	AB.plpaaapap	Cephalic sheath, sheet-like processes envelop meuropil of the ring and part of ventral ganglion\n"+
			"CEPshVR	AB.prpaaapap	Cephalic sheath, sheet-like processes envelop meuropil of the ring and part of ventral ganglion\n"+
			"CEPsoDL	AB.alapapppp	Cephalic socket\n"+
			"CEPsoDR	AB.alapppppp	Cephalic socket\n"+
			"CEPsoVL	AB.alppaappp	Cephalic socket\n"+
			"CEPsoVR	AB.alaapappp	Cephalic socket\n"+
			"CPO	P2.aap	AB.prapaappaap. Male specific cell in ventral cord, not reconstructed\n"+
			"CP1	P3.aapp	AB.plappaaaaapp or AB.prappaaaaapp. Male specific cell in ventral cord, not reconstructed\n"+
			"CP2	P4.aapp	AB.prappaaaaapp or AB.plappaaaaapp. Male specific cell in ventral cord, not reconstructed\n"+
			"CP3	P5.aapp	AB.plappaapaapp or AB.prappaapaapp. Male specific cell in ventral cord, not reconstructed\n"+
			"CP4	P6.aapp	AB.prappaapaapp or AB.plappaapaapp. Male specific motor neuron in ventral cord\n"+
			"CP5	P7.aapp	AB.plappappaapp or AB.prappappaapp. Male specific motor neuron in ventral cord\n"+
			"CP6	P8.aapp	AB.prappappaapp or AB.plappappaapp. Male specific motor neuron in ventral cord\n"+
			"CP7	P9.aapp	AB.plapapapaapp or AB.prapapapaapp. Male specific motor neuron in ventral cord\n"+
			"CP8	P10.aapp	AB.prapapapaapp or AB.plapapapaapp. Male specific interneuron, project into preanal ganglion\n"+
			"CP9	P11.aapp	AB.plapappaaapp. Male specific interneuron, project into preanal ganglion\n"+
			"D	P0.pppa	Embryonic founder cell\n"+
			"DA1	AB.prppapaap	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DA2	AB.plppapapa	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DA3	AB.prppapapa	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DA4	AB.plppapapp	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DA5	AB.prppapapp	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DA6	AB.plpppaaap	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DA7	AB.prpppaaap	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DA8	AB.prpapappp	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DA9	AB.plpppaaaa	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DB1/3	AB.plpaaaapp	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DB2	AB.arappappa	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DB3/1	AB.prpaaaapp	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DB4	AB.prpappapp	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DB5	AB.plpapappp	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DB6	AB.plppaappp	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DB7	AB.prppaappp	Ventral cord motor neurons, innervate dorsal muscles\n"+
			"DD1	AB.plppappap	Ventral cord motor neurons, reciprocal inhibitors, chnage synaptic pattern during postembryonic development\n"+
			"DD2	AB.prppappap	Ventral cord motor neurons, reciprocal inhibitors, chnage synaptic pattern during postembryonic development\n"+
			"DD3	AB.plppapppa	Ventral cord motor neurons, reciprocal inhibitors, chnage synaptic pattern during postembryonic development\n"+
			"DD4	AB.prppapppa	Ventral cord motor neurons, reciprocal inhibitors, chnage synaptic pattern during postembryonic development\n"+
			"DD5	AB.plppapppp	Ventral cord motor neurons, reciprocal inhibitors, chnage synaptic pattern during postembryonic development\n"+
			"DD6	AB.prppapppp	Ventral cord motor neurons, reciprocal inhibitors, chnage synaptic pattern during postembryonic development\n"+
			"DVA	AB.prppppapp	Ring interneurons, cell bodies in dorsorectal ganglion\n"+
			"DVB	K.p	AB.plpapppaap. Ring interneuron, cell body in dorsorectal ganglion, inervates rectal muscles\n"+
			"DVC	C.aapaa	Ring interneurons, cell bodies in dorsorectal ganglion\n"+
			"DVE	B.ppap	AB.prppppapappap. Male specific neuron, cell body in dorsorectal ganglion\n"+
			"DVF	B.ppppa	AB.prppppapappppa. Male specific neuron, cell body in dorsorectal ganglion\n"+
			"DX1/2	F.lvda	AB.plppppapplvda. Male specific neuron, darkly staining cell body in pre- anal ganglion, process penetrate basement membrane and contact muscles\n"+
			"DX1/2	F.rvda	AB.plppppapprvda. Male specific neuron, darkly staining cell body in pre- anal ganglion, process penetrate basement membrane and contact muscles\n"+
			"DX3/4	U.laa	AB.plppppapalaa. Male specific neuron, darkly staining cell body in pre- anal ganglion, process penetrate basement membrane and contact muscles\n"+
			"DX3/4	U.raa	AB.plppppaparaa. Male specific neuron, darkly staining cell body in pre- anal ganglion, process penetrate basement membrane and contact muscles\n"+
			"E	P0.pap	Embryonic founder cell\n"+
			"EF1/2	F.lvdp	Male specific neuron, large cell body in preanal ganglion, synaptic inputs from ray neurons.\n"+
			"EF1/2	F.rvdp	Male specific neuron, large cell body in preanal ganglion, synaptic inputs from ray neurons.\n"+
			"EF3/4	U.lap	AB.plppppapalap. Male specific neuron, large cell body in preanal ganglion, synaptic inputs from ray neurons.\n"+
			"EF3/4	U.rap	AB.plppppaparap. Male specific neuron, large cell body in preanal ganglion, synaptic inputs from ray neurons.\n"+
			"F	AB.plppppapp	Rectal cell, blast cell in male\n"+
			"FLPL	AB.plapaaapad	Neuron, ciliated ending in head, no supporting cells, associated with ILso\n"+
			"FLPR	AB.prapaaapad	Neuron, ciliated ending in head, no supporting cells, associated with ILso\n"+
			"GLRDL	MS.aaaaaal	Set of six cells that form a thin cylindrical sheet between pharynx and ring neuropile; no chemical synapses, but gap junctions with muscle arms and RME motor neurons\n"+
			"GLRDR	MS.aaaaaar	Set of six cells that form a thin cylindrical sheet between pharynx and ring neuropile; no chemical synapses, but gap junctions with muscle arms and RME motor neurons\n"+
			"GLRL	MS.apaaaad	Set of six cells that form a thin cylindrical sheet between pharynx and ring neuropile; no chemical synapses, but gap junctions with muscle arms and RME motor neurons\n"+
			"GLRR	MS.ppaaaad	Set of six cells that form a thin cylindrical sheet between pharynx and ring neuropile; no chemical synapses, but gap junctions with muscle arms and RME motor neurons\n"+
			"GLRVL	MS.apaaaav	Set of six cells that form a thin cylindrical sheet between pharynx and ring neuropile; no chemical synapses, but gap junctions with muscle arms and RME motor neurons\n"+
			"GLRVR	MS.ppaaaav	Set of six cells that form a thin cylindrical sheet between pharynx and ring neuropile; no chemical synapses, but gap junctions with muscle arms and RME motor neurons\n"+
			"G1	AB.prpaaaapa	Postembryonic blast cell, excretory socket in embryo\n"+
			"G2	AB.plapaapa	Postembryonic blast cell, excretory socket in L1, G2.p becomes socket later\n"+
			"HOL	AB.plaaappa	Seam hypodermal cell\n"+
			"HOR	AB.arpapppa	Seam hypodermal cell\n"+
			"H1L	AB.plaaappp	Seam hypodermal cell, postembryonic blast cell\n"+
			"H1R	AB.arpapppp	Seam hypodermal cell, postembryonic blast cell\n"+
			"H2L	AB.arppaaap	Seam hyopdermal cell, postemb. blast cell, L1 deirid socket\n"+
			"H2R	AB.arpppaap	Seam hyopdermal cell, postemb. blast cell, L1 deirid socket\n"+
			"HOA	P10.pppa	AB.plapapappppa. Neuron of hook sensillum, receptor anterior to cloaca in male\n"+
			"HOB	P10.ppap	AB.plapapapppap. Neuron of hook sensillum, receptor anterior to cloaca in male\n"+
			"HOsh	P10.ppppp	AB.plapapapppppp. Sheath of hook sensillum, receptor anterior to cloaca in male\n"+
			"HOso	P10.ppaa	AB.plapapapppaa. Socket of hook sensillum, receptor anterior to cloaca in male\n"+
			"HSNL	AB.plapppappa	Herm. specific motor neurons (die in male embryo), innervate vulval muscles, serotonergic\n"+
			"HSNR	AB.prapppappa	Herm. specific motor neurons (die in male embryo), innervate vulval muscles, serotonergic\n"+
			"I1L	AB.alpapppaa	Pharyngeal interneurons: ant sensory, input from RIP\n"+
			"I1R	AB.arapappaa	Pharyngeal interneurons: ant sensory, input from RIP\n"+
			"I2L	AB.alpappaapa	Pharyngeal interneurons, ant sensory.\n"+
			"I2R	AB.arapapaapa	Pharyngeal interneurons, ant sensory.\n"+
			"I3	MS.aaaaapaa	Pharyngeal interneuron, ant sensory.\n"+
			"I4	MS.aaaapaa	Pharyngeal interneuron.\n"+
			"I5	AB.arapapapp	Pharyngeal interneuron, post sensory.\n"+
			"I6	MS.paaapaa	Pharyngeal interneuron, post sensory.\n"+
			"IL1DL	AB.alapappaaa	Inner labial neuron\n"+
			"IL1DR	AB.alappppaaa	Inner labial neuron\n"+
			"IL1L	AB.alapaappaa	Inner labial neuron\n"+
			"IL1R	AB.alaappppaa	Inner labial neuron\n"+
			"IL1VL	AB.alppapppaa	Inner labial neuron\n"+
			"IL1VR	AB.arapppppaa	Inner labial neuron\n"+
			"IL2DL	AB.alapappap	Inner labial neuron\n"+
			"IL2DR	AB.alappppap	Inner labial neuron\n"+
			"IL2L	AB.alapaappp	Inner labial neuron\n"+
			"IL2R	AB.alaappppp	Inner labial neuron\n"+
			"IL2VL	AB.alppapppp	Inner labial neuron\n"+
			"IL2VR	AB.arapppppp	Inner labial neuron\n"+
			"ILshDL	AB.alaaaparr	Inner labial sheath\n"+
			"ILshDR	AB.alaaappll	Inner labial sheath\n"+
			"ILshL	AB.alaaaalpp	Inner labial sheath\n"+
			"ILshR	AB.alaapaapp	Inner labial sheath\n"+
			"ILshVL	AB.alppapaap	Inner labial sheath\n"+
			"ILshVR	AB.arapppaap	Inner labial sheath\n"+
			"ILsoDL	AB.plaapaaap	Inner labial socket\n"+
			"ILsoDR	AB.praapaaap	Inner labial socket\n"+
			"ILsoL	AB.alaaapall	Inner labial socket\n"+
			"ILsoR	AB.alaaapprr	Inner labial socket\n"+
			"ILsoVL	AB.alppapapp	Inner labial socket\n"+
			"ILsoVR	AB.arapppapp	Inner labial socket\n"+
			"K	AB.plpapppaa	Rectal cell, postembryonic blast cell\n"+
			"K\'	AB.plpapppap	Rectal cell\n"+
			"K.a	K.a	AB.plpapppaaa. Rectal cell\n"+
			"LUAL	AB.plpppaapap	Interneuron, short process in post ventral cord\n"+
			"LUAR	AB.prpppaapap	Interneuron, short process in post ventral cord\n"+
			"M	MS.apaap	Postembryonic mesoblast\n"+
			"M1	MS.paapaaa	Pharyngeal motorneurons\n"+
			"M2L	AB.araapappa	Pharyngeal motorneurons\n"+
			"M2R	AB.araappppa	Pharyngeal motorneurons\n"+
			"M3L	AB.araapappp	Pharyngeal sensory-motorneurons\n"+
			"M3R	AB.araappppp	Pharyngeal sensory-motorneurons\n"+
			"M4	MS.paaaaaa	Pharyngeal motorneurons\n"+
			"M5	MS.paaapap	Pharyngeal motorneurons\n"+
			"MCL	AB.alpaaappp	Pharyngeal neurons that synapse onto marginal cells\n"+
			"MCR	AB.arapaappp	Pharyngeal neurons that synapse onto marginal cells\n"+
			"MI	AB.araappaaa	Pharyngeal motor neuron/interneuron\n"+
			"MS	P0.paa	Embryonic founder cell\n"+
			"NSML	AB.araapapaav	Pharyngeal neurosecretory motorneuron, contain serotonin\n"+
			"NSMR	AB.araapppaav	Pharyngeal neurosecretory motorneuron, contain serotonin\n"+
			"OLLL	AB.alppppapaa	Lateral outer labial neurons\n"+
			"OLLR	AB.praaapapaa	Lateral outer labial neurons\n"+
			"OLLshL	AB.alpppaapd	Lateral outer labial sheath\n"+
			"OLLshR	AB.praaaaapd	Lateral outer labial sheath\n"+
			"OLLsoL	AB.alapaaapp	Lateral outer labial socket\n"+
			"OLLsoR	AB.alappappp	Lateral outer labial socket\n"+
			"OLQDL	AB.alapapapaa	Quadrant outer labial neuron\n"+
			"OLQDR	AB.alapppapaa	Quadrant outer labial neuron\n"+
			"OLQVL	AB.plpaaappaa	Quadrant outer labial neuron\n"+
			"OLQVR	AB.prpaaappaa	Quadrant outer labial neuron\n"+
			"OLQshDL	AB.arpaaaapa	Quadrant outer labial sheath\n"+
			"OLQshDR	AB.arpaaapaa	Quadrant outer labial sheath\n"+
			"OLQshVL	AB.alpppaaap	Quadrant outer labial sheath\n"+
			"OLQshVR	AB.praaaaaap	Quadrant outer labial sheath\n"+
			"OLQsoDL	AB.arpaaaaal	Quadrant outer labial socket\n"+
			"OLQsoDR	AB.arpaaaaar	Quadrant outer labial socket\n"+
			"OLQsoVL	AB.alppaaapp	Quadrant outer labial socket\n"+
			"OLQsoVR	AB.alaappapp	Quadrant outer labial socket\n"+
			"P0	Z	Single cell zygote\n"+
			"P4	P0.pppp	Embryonic founder cell of germ line\n"+
			"P1/2L	AB.plapaapp	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P1/2R	AB.prapaapp	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P3/4L	AB.plappaaa	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P3/4R	AB.prappaaa	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P5/6L	AB.plappaap	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P5/6R	AB.prappaap	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P7/8L	AB.plappapp	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P7/8R	AB.prappapp	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P9/10L	AB.plapapap	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P9/10R	AB.prapapap	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P11	AB.plapappa	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"P12	AB.prapappa	Postembryonic blast cells for ventral cord motorneurons, ventral hypodermis, vulva, male preanal ganglion; ventral hypodermis in L1\n"+
			"PCAL	Y.plppd	AB.prpppaaaaplppd. Sensory neuron of postcloacal sensilla in male\n"+
			"PCAR	Y.prppd	AB.prpppaaaaprppd. Sensory neuron of postcloacal sensilla in male\n"+
			"PCBL	Y.plpa	AB.prpppaaaaplpa. Neuron, ending is sheath of postcloacal sensilla in male\n"+
			"PCBR	Y.prpa	AB.prpppaaaaprpa. Neuron, ending is sheath of postcloacal sensilla in male\n"+
			"PCCL	B.arpaaa	AB.prppppapaarpaaa. Sensory neuron of postcloacal sensilla in male\n"+
			"PCCR	B.alpaaa	AB.prppppapaalpaaa. Sensory neuron of postcloacal sensilla in male\n"+
			"PChL	Y.plaa	AB.prpppaaaaplaa. Hypodermal cell of postcloacal sensilla in male\n"+
			"PChR	Y.praa	AB.prpppaaaapraa. Hypodermal cell of postcloacal sensilla in male\n"+
			"PCshL	Y.plppv	AB.prpppaaaaplppv. Sheath of postcloacal sensilla in male\n"+
			"PCshR	Y.prppv	AB.prpppaaaaprppv. Sheath of postcloacal sensilla in male\n"+
			"PCsoL	Y.plap	AB.prpppaaaaplap. Socket of postcloacal sensilla in male\n"+
			"PCsoR	Y.prap	AB.prpppaaaaprap. Socket of postcloacal sensilla in male\n"+
			"PDA	AB.prpppaaaa	Motor neuron, process in dorsal cord, same as Y cell in hermaphrodite, Y.a in male\n"+
			"PDB	P12.apa	AB.prapappaapa. Motor neuron, process in dorsal cord, cell body in pre-anal ganglion\n"+
			"PDC	P11.papa	AB.plapappapapa. Male specific interneuron, preanal ganglion\n"+
			"PDEL	V5L.paaa	AB.plapapaappaaa. Neuron, dopaminergic of postderid sensillum\n"+
			"PDER	V5R.paaa	AB.prapapaappaaa. Neuron, dopaminergic of postderid sensillum\n"+
			"PDEshL	V5L.papp	AB.plapapaappapp. Sheath of PDE\n"+
			"PDEshR	V5R.papp	AB.prapapaappapp. Sheath of PDE\n"+
			"PDEsoL	V5L.papa	AB.plapapaappapa. Socket of PDE\n"+
			"PDEsoR	V5R.papa	AB.prapapaappapa. Socket of PDE\n"+
			"PGA	P11.papp	AB.plapappapapp. Male specific interneuron, preanal ganglion\n"+
			"PHAL	AB.plpppaapp	Phasmid neurons, probably chemosensory\n"+
			"PHAR	AB.prpppaapp	Phasmid neurons, probably chemosensory\n"+
			"PHBL	AB.plapppappp	Phasmid neurons, probably chemosensory\n"+
			"PHRB	AB.prapppappp	Phasmid neurons, probably chemosensory\n"+
			"PHCL	TL.pppaa	AB.plappppppppaa. Neuron, striated rootlet in male, possibly sensory in tail spike\n"+
			"PHCR	TR.pppaa	AB.prappppppppaa. Neuron, striated rootlet in male, possibly sensory in tail spike\n"+
			"PHshL	AB.plpppapaa	Phasmid sheath\n"+
			"PHshR	AB.prpppapaa	Phasmid sheath\n"+
			"PHso1L	TL.paa	AB.plappppppaa. Phasmid sockets\n"+
			"PHso1R	TR.paa	AB.prappppppaa. Phasmid sockets\n"+
			"PHso2L	TL.pap	AB.plappppppap. Phasmid sockets\n"+
			"PHso2R	TR.pap	AB.prappppppap. Phasmid sockets\n"+
			"PLML	AB.plapappppaa	Posterior lateral microtubule cell, touch receptor\n"+
			"PLMR	AB.prapappppaa	Posterior lateral microtubule cell, touch receptor\n"+
			"PLNL	TL.pppap	AB.plappppppppap. Interneuron, associated with PLM\n"+
			"PLNR	TR.pppap	AB.prappppppppap. Interneuron, associated with PLM\n"+
			"PQR	QL.ap	AB.plapapaaaap. Neuron, basal body, not part of a sensillum, projects into preanal gangion\n"+
			"PVCL	AB.plpppaapaa	Ventral cord interneuron, cell body in lumbar ganglion, synapses onto VB andDB motor neurons, formerly called delta.\n"+
			"PVCR	AB.prpppaapaa	Ventral cord interneuron, cell body in lumbar ganglion, synapses onto VB andDB motor neurons, formerly called delta.\n"+
			"PVDL	V5L.paapa	AB.plapapaappaapa. Neuron, lateral process adjacent to excretory canal\n"+
			"PVDR	V5R.paapa	AB.prapapaappaapa. Neuron, lateral process adjacent to excretory canal\n"+
			"PVM	QL.paa	AB.plapapaaapaa. Posterior ventral microtuble cell, touch receptor\n"+
			"PVNL	TL.appp	AB.plapppppappp. Interneuron/motor neuron, post. vent. cord, few synapses\n"+
			"PVNR	TR.appp	AB.prapppppappp. Interneuron/motor neuron, post. vent. cord, few synapses\n"+
			"PVPL	AB.plppppaaa	Interneuron, cell body in lumbar ganglion, projects along ventral cord to nerve ring\n"+
			"PVPR	AB.prppppaaa	Interneuron, cell body in lumbar ganglion, projects along ventral cord to nerve ring\n"+
			"PVQL	AB.plapppaaa	Interneuron, projects along ventral cord to ring\n"+
			"PVQR	AB.prapppaaa	Interneuron, projects along ventral cord to ring\n"+
			"PVR	C.aappa	Interneuron, projects along ventral cord to ring\n"+
			"PVT	AB.plpappppa	Interneuron, projects along ventral cord to ring\n"+
			"PVV	P11.paaa	AB.plapappapaaa. Male specific motor neuron, ventral cord\n"+
			"PVWL	TL.ppa	AB.plapppppppa. Interneuron, posterior ventral cord, few synapses\n"+
			"PVWR	TR.ppa	AB.prapppppppa. Interneuron, posterior ventral cord, few synapses\n"+
			"PVX	P12.aap	AB.prapappaaap. Male specific interneuron, postsynaptic in ring and ventral cord\n"+
			"PVY	P11.paap	AB.plapappapaap.\n"+
			"PVZ	P10.ppppa	AB.plapapapppppa or AB.prapapapppppa. Male specific motor neuron, ventral cord\n"+
			"QL	AB.plapapaaa	Postembryonic neuroblast, migrates anteriorly\n"+
			"QR	AB.prapapaaa	Postembryonic neuroblast, migrates posteriorly\n"+
			"R1AL	V5L.pppppaaa, R1.aaa	AB.plapapaappppppaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R1AR	V5R.pppppaaa, R1.aaa	AB.prapapaappppppaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R1BL	V5L.pppppapa, R1.apa	AB.plapapaappppppapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R1BR	V5R.pppppapa, R1.apa	AB.prapapaappppppapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R1stL	V5L.pppppapp, R1.app	AB.plapapaappppppapp. Male sensory rays, structural cell\n"+
			"R1stR	V5R.pppppapp, R1.app	AB.prapapaappppppapp. Male sensory rays, structural cell\n"+
			"R2AL	V6L.papapaaa, R2.aaa	AB.arppappppapapaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R2AR	V6R.papapaaa, R2.aaa	AB.arpppppppapapaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R2BL	V6L.papapapa, R2.apa	AB.arppappppapapapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R2BR	V6R.papapapa, R2.apa	AB.arpppppppapapapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R2stL	V6L.papapapp, R2.app	AB.arppappppapapapp. Male sensory rays, structural cell\n"+
			"R2stR	V6R.papapapp, R2.app	AB.arpppppppapapapp. Male sensory rays, structural cell\n"+
			"R3AL	V6L.papppaaa, R3.aaa	AB.arppappppapppaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R3AR	V6R.papppaaa, R3.aaa	AB.arpppppppapppaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R3BL	V6L.papppapa, R3.apa	AB.arppappppapppapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R3BR	V6R.papppapa, R3.apa	AB.arpppppppapppapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R3stL	V6L.papppapp, R3.app	AB.arppappppapppapp. Male sensory rays, structural cell\n"+
			"R3stR	V6R.papppapp, R3.app	AB.arpppppppapppapp. Male sensory rays, structural cell\n"+
			"R4AL	V6L.pppapaaa, R4.aaa	AB.arppappppppapaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R4AR	V6R.pppapaaa, R4.aaa	AB.arpppppppppapaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R4BL	V6L.pppapapa, R4.apa	AB.arppappppppapapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R4BR	V6R.pppapapa, R4.apa	AB.arpppppppppapapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R4stL	V6L.pppapapp, R4.app	AB.arppappppppapapp. Male sensory rays, structural cell\n"+
			"R4stR	V6R.pppapapp, R4.app	AB.arpppppppppapapp. Male sensory rays, structural cell\n"+
			"R5AL	V6L.pppppaaa, R5.aaa	AB.arppappppppppaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R5AR	V6R.pppppaaa, R5.aaa	AB.arpppppppppppaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R5BL	V6L.pppppapa, R5.apa	AB.arppappppppppapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R5BR	V6R.pppppapa, R5.apa	AB.arpppppppppppapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R5stL	V6L.pppppapp, R5.app	AB.arppappppppppapp. Male sensory rays, structural cell\n"+
			"R5stR	V6R.pppppapp, R5.app	AB.arpppppppppppapp. Male sensory rays, structural cell\n"+
			"R6AL	V6L.ppppaaaa, R6.aaa	AB.arppapppppppaaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R6AR	V6R.ppppaaaa, R6.aaa	AB.arppppppppppaaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R6BL	V6L.ppppaapa, R6.apa	AB.arppapppppppaapa. Male sensory rays, neuron, not darkly staining nor open to outside\n"+
			"R6BR	V6R.ppppaapa, R6.apa	AB.arppppppppppaapa. Male sensory rays, neuron, not darkly staining nor open to outside\n"+
			"R6stL	V6L.ppppaapp, R6.app	AB.arppapppppppaapp. Male sensory rays, structural cell\n"+
			"R6stR	V6R.ppppaapp, R6.app	AB.arppppppppppaapp. Male sensory rays, structural cell\n"+
			"R7AL	TL.apappaaa, R7.aaa	AB.plapppppapappaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R7AR	TR.apappaaa, R7.aaa	AB.prapppppapappaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R7BL	TL.apappapa, R7.apa	AB.plapppppapappapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R7BR	TR.apappapa, R7.apa	AB.prapppppapappapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R7stL	TL.apappapp, R7.app	AB.plapppppapappapp. Male sensory rays, structural cell\n"+
			"R7stR	TR.apappapp, R7.app	AB.prapppppapappapp. Male sensory rays, structural cell\n"+
			"R8AL	TL.appaaaaa, R8.aaa	AB.plapppppappaaaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R8AR	TR.appaaaaa, R8.aaa	AB.prapppppappaaaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R8BL	TL.appaaapa, R8.apa	AB.plapppppappaaapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R8BR	TR.appaaapa, R8.apa	AB.prapppppappaaapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R8stL	TL.appaaapp, R8.app	AB.plapppppappaaapp. Male sensory rays, structural cell\n"+
			"R8stR	TR.appaaapp, R8.app	AB.prapppppappaaapp. Male sensory rays, structural cell\n"+
			"R9AL	TL.appapaaa, R9.aaa	AB.plapppppappapaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R9AR	TR.appapaaa, R9.aaa	AB.prapppppappapaaa. Male sensory rays, neuron, striated rootlet\n"+
			"R9BL	TL.appapapa, R9.apa	AB.plapppppappapapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R9BR	TR.appapapa, R9.apa	AB.prapppppappapapa. Male sensory rays, neuron, darkly staining tip, open to outside\n"+
			"R9stL	TL.appapapp, R9.app	AB.plapppppappapapp. Male sensory rays, structural cell\n"+
			"R9stR	TR.appapapp, R9.app	AB.prapppppappapapp. Male sensory rays, structural cell\n"+
			"RIAL	AB.alapaapaa	Ring interneuron, many synapses\n"+
			"RIAR	AB.alaapppaa	Ring interneuron, many synapses\n"+
			"RIBL	AB.plpaappap	Ring interneuron\n"+
			"RIBR	AB.prpaappap	Ring interneuron\n"+
			"RICL	AB.plppaaaapp	Ring interneuron\n"+
			"RICR	AB.prppaaaapp	Ring interneuron\n"+
			"RID	AB.alappaapa	Ring interneuron, projects along dorsal cord\n"+
			"RIFL	AB.plppapaaap	Ring interneuron\n"+
			"RIFR	AB.prppapaaap	Ring interneuron\n"+
			"RIGL	AB.plppappaa	Ring interneuron\n"+
			"RIGR	AB.prppappaa	Ring interneuron\n"+
			"RIH	AB.prpappaaa	Ring interneuron\n"+
			"RIML	AB.plppaapap	Ring motor neuron\n"+
			"RIMR	AB.prppaapap	Ring motor neuron\n"+
			"RIPL	AB.alpapaaaa	Ring/pharynx interneuron, only direct connection between pharynx and ring\n"+
			"RIPR	AB.prappaaaa	Ring/pharynx interneuron, only direct connection between pharynx and ring\n"+
			"RIR	AB.prpapppaa	Ring interneuron\n"+
			"RIS	AB.prpappapa	Ring interneuron\n"+
			"RIVL	AB.plpaapaaa	Ring interneuron\n"+
			"RIVR	AB.prpaapaaa	Ring interneuron\n"+
			"RMDDL	AB.alpapapaa	Ring motor neuron/interneuron, many synapses\n"+
			"RMDDR	AB.arappapaa	Ring motor neuron/interneuron, many synapses\n"+
			"RMDL	AB.alpppapad	Ring motor neuron/interneuron, many synapses\n"+
			"RMDR	AB.praaaapad	Ring motor neuron/interneuron, many synapses\n"+
			"RMDVL	AB.alppapaaa	Ring motor neuron/interneuron, many synapses\n"+
			"RMDVR	AB.arapppaaa	Ring motor neuron/interneuron, many synapses\n"+
			"RMED	AB.alapppaap	Ring motor neuron\n"+
			"RMEL	AB.alaaaarlp	Ring motor neuron\n"+
			"RMER	AB.alaaaarrp	Ring motor neuron\n"+
			"RMEV	AB.plpappaaa	Ring motor neuron\n"+
			"RMFL	G2.al	AB.plapaapaal. Ring motor neuron/interneuron\n"+
			"RMFR	G2.ar	AB.plapaapaar. Ring motor neuron/interneuron\n"+
			"RMGL	AB.plapaaapp	Ring interneuron\n"+
			"RMGR	AB.prapaaapp	Ring interneuron\n"+
			"RMHL	G1.l	AB.prpaaaapal. Ring motor neuron/interneuron\n"+
			"RMHR	G1.r	AB.prpaaaapar. Ring motor neuron/interneuron\n"+
			"SAADL	AB.alppapapa	Ring interneuron, anteriorly projecting process that runs sublaterally\n"+
			"SAADR	AB.arapppapa	Ring interneuron, anteriorly projecting process that runs sublaterally\n"+
			"SAAVL	AB.plpaaaaaa	Ring interneuron, anteriorly projecting process that runs sublaterally\n"+
			"SAAVR	AB.prpaaaaaa	Ring interneuron, anteriorly projecting process that runs sublaterally\n"+
			"SABD	AB.plppapaap	Ring interneuron, anteriorly projecting process that runs sublaterally, synapses to anterior body muscles in L1\n"+
			"SABVL	AB.plppapaaaa	Ring interneuron, anteriorly projecting process that runs sublaterally, synapses to anterior body muscles in L1\n"+
			"SABVR	AB.prppapaaaa	Ring interneuron, anteriorly projecting process that runs sublaterally, synapses to anterior body muscles in L1\n"+
			"SDQL	QL.pap	AB.plapapaaapap. Post. lateral interneuron, process projects into ring\n"+
			"SDQR	QR.pap	AB.prapapaaaoap. Ant. lateral interneuron, process projects into ring\n"+
			"SIADL	AB.plpapaapa	Receives a few synapses in the ring, has a posteriorly directed process that runs sublaterally\n"+
			"SIADR	AB.prpapaapa	Receives a few synapses in the ring, has a posteriorly directed process that runs sublaterally\n"+
			"SIAVL	AB.plpapappa	Receives a few synapses in the ring, has a posteriorly directed process that runs sublaterally\n"+
			"SIAVR	AB.prpapappa	Receives a few synapses in the ring, has a posteriorly directed process that runs sublaterally\n"+
			"SIBDL	AB.plppaaaaa	Receives a few synapses in the ring, has a posteriorly directed process that runs sublaterally\n"+
			"SIBDR	AB.prppaaaaa	Receives a few synapses in the ring, has a posteriorly directed process that runs sublaterally\n"+
			"SIBVL	AB.plpapaapp	Receives a few synapses in the ring, has a posteriorly directed process that runs sublaterally\n"+
			"SIBVR	AB.prpapaapp	Receives a few synapses in the ring, has a posteriorly directed process that runs sublaterally\n"+
			"SMBDL	AB.alpapapapp	Ring motor neuron/interneuron, has a posteriorly direct ed process that runs sublaterally\n"+
			"SMBDR	AB.arappapapp	Ring motor neuron/interneuron, has a posteriorly direct ed process that runs sublaterally\n"+
			"SMBVL	AB.alpapappp	Ring motor neuron/interneuron, has a posteriorly direct ed process that runs sublaterally\n"+
			"SMBVR	AB.arappappp	Ring motor neuron/interneuron, has a posteriorly direct ed process that runs sublaterally\n"+
			"SMDDL	AB.plpapaaaa	Ring motor neuron/interneuron, has a posteriorly direct ed process that runs sublaterally\n"+
			"SMDDR	AB.prpapaaaa	Ring motor neuron/interneuron, has a posteriorly direct ed process that runs sublaterally\n"+
			"SMDVL	AB.alppappaa	Ring motor neuron/interneuron, has a posteriorly direct ed process that runs sublaterally\n"+
			"SMDVR	AB.arappppaa	Ring motor neuron/interneuron, has a posteriorly direct ed process that runs sublaterally\n"+
			"SPCL	B.alpaap	AB.prppppapaalpaap. Male specific sensory /motor neuron, innervates spicule protractor muscle\n"+
			"SPCR	B.arpaap	AB.prppppapaarpaap. Male specific sensory /motor neuron, innervates spicule protractor muscle\n"+
			"SPDL	B.alpapaa	AB.prppppapaalpapaa. Sensory neuron of copulatory spicle in male, cilliated, open to outside at tip of spicule\n"+
			"SPDR	B.arpapaa	AB.prppppapaarpapaa. Sensory neuron of copulatory spicle in male, cilliated, open to outside at tip of spicule\n"+
			"SPVL	B.al/raalda	AB.prppppapaalaalda or AB.prppppapaaraalda. Sensory neuron of copulatory spicle in male, cilliated, open to outside at tip of spicule\n"+
			"SPVR	B.al/raarda	AB.prppppapaaraarda or AB.prppppapaalaarda. Sensory neuron of copulatory spicle in male, cilliated, open to outside at tip of spicule\n"+
			"SPshDL	B.alpapap	AB.prppppapaalpapap. Sheath of male copulatory spicule\n"+
			"SPshDR	B.arpapap	AB.prppppapaarpapap. Sheath of male copulatory spicule\n"+
			"SPshVL	B.al/raaldp	AB.prppppapaalaaldp or AB.prppppapaaraaldp. Sheath of male copulatory spicule\n"+
			"SpshVR	B.al/raarda	AB.prppppapaaraalda or AB.prppppapaalaalda. Sheath of male copulatory spicule\n"+
			"SPso1L	B.al/rpppl	AB.prppppapaalpppl or AB.prppppapaarpppl. Socket of male copulatory spicule\n"+
			"SPso1R	B.al/rpppr	AB.prppppapaalpppr or AB.prppppapaarpppr. Socket of male copulatory spicule\n"+
			"SPso2L	B.al/raald	AB.prppppapaalaald or AB.prppppapaaraald. Socket of male copulatory spicule\n"+
			"SPso2R	B.al/raard	AB.prppppapaalaard or AB.prppppapaaraard. Socket of male copulatory spicule\n"+
			"SPso3L	B.al/raalv	AB.prppppapaalaalv or AB.prppppapaaraalv. Socket of male copulatory spicule\n"+
			"SPso3R	B.al/raarv	AB.prppppapaalaarv or AB.prppppapaaraarv. Socket of male copulatory spicule\n"+
			"SPso4L	B.alpapp	AB.prppppapaalpapp. Socket of male copulatory spicule\n"+
			"SPso4r	B.arpapp	AB.prppppapaarpapp. Socket of male copulatory spicule\n"+
			"TL	AB.plappppp	Tail seam hypodermal cell, postembryonic blast cell, functions as phasmid socket in L1\n"+
			"TR	AB.prappppp	Tail seam hypodermal cell, postembryonic blast cell, functions as phasmid socket in L1\n"+
			"U	AB.plppppapa	Rectal cell, postembryonic blast cell in male\n"+
			"URADL	AB.plaaaaaaa	Ring motor neuron\n"+
			"URADR	AB.arpapaaaa	Ring motor neuron\n"+
			"URAVL	AB.plpaaapaa	Ring motor neuron\n"+
			"URAVR	AB.prpaaapaa	Ring motor neuron\n"+
			"URBL	AB.plaapaapa	Neuron, presynaptic in ring, ending in head\n"+
			"URBR	AB.praapaapa	Neuron, presynaptic in ring, ending in head\n"+
			"URXL	AB.plaaaaappp	Ring interneuron\n"+
			"URXR	AB.arpapaappp	Ring interneuron\n"+
			"URYDL	AB.alapapapp	Neuron, presynaptic in ring, ending in head\n"+
			"URYDR	AB.alapppapp	Neuron, presynaptic in ring, ending in head\n"+
			"URYVL	AB.plpaaappp	Neuron, presynaptic in ring, ending in head\n"+
			"URYVR	AB.prpaaappp	Neuron, presynaptic in ring, ending in head\n"+
			"V1L	AB.arppapaa	Seam hypodermal cell, postembryonic blast cell\n"+
			"V1R	AB.arppppaa	Seam hypodermal cell, postembryonic blast cell\n"+
			"V2L	AB.arppapap	Seam hypodermal cell, postembryonic blast cell\n"+
			"V2R	AB.arppppap	Seam hypodermal cell, postembryonic blast cell\n"+
			"V3L	AB.plappapa	Seam hypodermal cell, postembryonic blast cell\n"+
			"V3R	AB.prappapa	Seam hypodermal cell, postembryonic blast cell\n"+
			"V4L	AB.arppappa	Seam hypodermal cell, postembryonic blast cell\n"+
			"V4R	AB.arpppppa	Seam hypodermal cell, postembryonic blast cell\n"+
			"V5L	AB.plapapaap	Seam hypodermal cell, postembryonic blast cell\n"+
			"V5R	AB.prapapaap	Seam hypodermal cell, postembryonic blast cell\n"+
			"V6L	AB.arppappp	Seam hypodermal cell, postembryonic blast cell\n"+
			"V6R	AB.arpppppp	Seam hypodermal cell, postembryonic blast cell\n"+
			"VA1	W.pa	AB.prapaapapa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA2	P2.aaaa	AB.prapaappaaaa or AB.plapaappaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA3	P3.aaaa	AB.plappaaaaaaa or AB.prappaaaaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA4	P4.aaaa	AB.prappaaaaaaa or AB.plappaaaaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA5	P5.aaaa	AB.plappaapaaaa or AB.prappaapaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA6	P6.aaaa	AB.prappaapaaaa or AB.plappaapaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA7	P7.aaaa	AB.plappappaaaa or AB.prappaapaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA8	P8.aaaa	AB.prappaapaaaa or AB.plappappaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA9	P9.aaaa	AB.plapapapaaaa or AB.prapapapaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA10	P10.aaaa	AB.prapapapaaaa or AB.plapapapaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA11	P11.aaaa	AB.plapappaaaaa. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VA12	P12.aaaa	AB.prapappaaaaa. Ventral cord motor neuron, innervates vent. body muscles, but also interneuron in preanal ganglion\n"+
			"VB1	P1.aaap	AB.plapaappaaap or AB.prapaappaaap. Ventral cord motor neuron, innervates vent. body muscles, also interneuron in ring\n"+
			"VB2	W.aap	AB.prapaapaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VB3	P2.aaap	AB.prapaappaaap or AB.plapaappaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VB4	P3.aaap	AB.plappaaaaaap or AB.prappaaaaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VB5	P4.aaap	AB.prappaaaaaap or AB.plappaaaaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VB6	P5.aaap	AB.plappaapaaap or AB.prappaapaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VB7	P6.aaap	AB.prappaapaaap or AB.plappaapaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VB8	P7.aaap	AB.plappappaaap or AB.prappaapaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VB9	P8.aaap	AB.prappaapaaap or AB.plappappaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VB10	P9.aaap	AB.plapapapaaap or AB.prapapapaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VB11	P10.aaap	AB.prapapapaaap or AB.plapapapaaap. Ventral cord motor neuron, innervates vent. body muscles\n"+
			"VC1	P3.aap	AB.plappaaaaap or AB.prappaaaaap. Hermaphrodite specific ventral cord motor neuron innervates vulval muscles and ventral body muscles\n"+
			"VC2	P4.aap	AB.prappaaaaap or AB.plappaaaaap. Hermaphrodite specific ventral cord motor neuron innervates vulval muscles and ventral body muscles\n"+
			"VC3	P5.aap	AB.plappaapaap or AB.prappaapaap. Hermaphrodite specific ventral cord motor neuron innervates vulval muscles and ventral body muscles\n"+
			"VC4	P6.aap	AB.prappaapaap or AB.plappaapaap. Hermaphrodite specific ventral cord motor neuron innervates vulval muscles and ventral body muscles\n"+
			"VC5	P7.aap	AB.plappappaap or AB.prappaapaap. Hermaphrodite specific ventral cord motor neuron innervates vulval muscles and ventral body muscles\n"+
			"VC6	P8.aap	AB.prappaapaap or AB.plappappaap. Hermaphrodite specific ventral cord motor neuron innervates vulval muscles and ventral body muscles\n"+
			"VD1	W.pp	AB.prapaapapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD2	P1.app	AB.plapaappapp or AB.prapaappapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD3	P2.app	AB.prapaappapp or AB.plapaappapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD4	P3.app	AB.plappaaaapp or AB.prappaaaapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD5	P4.app	AB.prappaaaapp or AB.plappaaaapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD6	P5.app	AB.plappaapapp or AB.prappaapapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD7	P6.app	AB.prappaapapp or AB.plappaapapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD8	P7.app	AB.plappappapp or AB.prappaapapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD9	P8.app	AB.prappaapapp or AB.plappappapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD10	P9.app	AB.plapapapapp or AB.prapapapapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD11	P10.app	AB.prapapapapp or AB.plapapapapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD12	P11.app	AB.plapappaapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"VD13	P12.app	AB.prapappaapp. Ventral cord motor neuron, innervates ventral body muscles, probably reciprocal inhibitor\n"+
			"W	AB.prapaapa	Postembryonic neuroblast, analogous to Pn.a cells\n"+
			"XXXL	AB.plaaapaa	Embryonic head hypodermal cell\n"+
			"XXXR	AB.arpappaa	Embryonic head hypodermal cell\n"+
			"Y	AB.prpppaaaa	Rectal cell at hatching, becomes PDA in hermaphrodite, postembryonic blast cell in male\n"+
			"Z1	MS.pppaap	Somatic gonad precursor cell\n"+
			"Z2	P0.ppppp	Germ line precursor cell\n"+
			"Z3	P0.ppppa	Germ line precursor cell\n"+
			"Z4	MS.appaap	Somatic gonad precursor cell\n"+
			"Anterior_arcade_DR	AB.araaapppa	Interface between pharynx and hypodermis, form anterior part of the buccal cavity\n"+
			"Anterior_arcade_DL	AB.alpaappaa	Interface between pharynx and hypodermis, form anterior part of the buccal cavity\n"+
			"Anterior_arcade_V	AB.alpapaapa	Interface between pharynx and hypodermis, form anterior part of the buccal cavity\n"+
			"Posterior_arcade_DL	AB.alpaapaaa	Interface between pharynx and hypodermis, form anterior part of the buccal cavity\n"+
			"Posterior_arcade_VL	AB.alpaappap	Interface between pharynx and hypodermis, form anterior part of the buccal cavity\n"+
			"Posterior_arcade_DR	AB.araaapaaa	Interface between pharynx and hypodermis, form anterior part of the buccal cavity\n"+
			"Posterior_arcade_D	AB.araaappaa	Interface between pharynx and hypodermis, form anterior part of the buccal cavity\n"+
			"Posterior_arcade_VR	AB.araaappap	Interface between pharynx and hypodermis, form anterior part of the buccal cavity\n"+
			"Posterior_arcade_V	AB.arapapapa	Interface between pharynx and hypodermis, form anterior part of the buccal cavity\n"+
			"cc_DL	M.dlpa	Postembryonic coelomocyte in hermaphrodite\n"+
			"cc_DR	M.drpa	Postembryonic coelomocyte in hermaphrodite\n"+
			"cc_male_D	M.dlpappp	Single male postembryonic coelomocyte\n"+
			"ccAL	MS.apapaaa	Embryonic coelomocytes\n"+
			"ccAR	MS.ppapaaa	Embryonic coelomocytes\n"+
			"ccPL	MS.apapaap	Embryonic coelomocytes\n"+
			"ccPR	MS.ppapaap	Embryonic coelomocytes\n"+
			"e1D	AB.araaaapap	Pharyngeal epithelial cells\n"+
			"e1VL	AB.araaaaaaa	Pharyngeal epithelial cells\n"+
			"e1VR	AB.araaaaapa	Pharyngeal epithelial cells\n"+
			"e2DL	AB.alpaapaap	Pharyngeal epithelial cells\n"+
			"e2DR	AB.araaapaap	Pharyngeal epithelial cells\n"+
			"e2V	AB.alpappapa	Pharyngeal epithelial cells\n"+
			"e3D	AB.araapaaaa	Pharyngeal epithelial cells\n"+
			"e3VL	AB.alpaaaaaa	Pharyngeal epithelial cells\n"+
			"e3VR	AB.arapaaaaa	Pharyngeal epithelial cells\n"+
			"exc_cell	AB.plpappaap	Large H-shaped excretory cell\n"+
			"Excretory canal R	AB.plpappaap	Large H-shaped excretory cell\n"+
			"Excretory canal L	AB.plpappaap	Large H-shaped excretory cell\n"+
			"exc_duct	AB.plpaaaapa	Excretory duct\n"+
			"exc_gl_L	AB.plpapapaa	Excretory glands, fused, send processes to ring,open into excretory duct\n"+
			"exc_gl_R	AB.prpapapaa	Excretory glands, fused, send processes to ring,open into excretory duct\n"+
			"socket exc_socket	G2.p	Excretory socket, links duct to hypodermis\n"+
			"g1AL	MS.aapaapaa	Pharyngeal gland cells\n"+
			"g1AR	MS.papaapaa	Pharyngeal gland cells\n"+
			"g1P	MS.aaaaapap	Pharyngeal gland cells\n"+
			"g2L	MS.aapapaa	Pharyngeal gland cells\n"+
			"g2R	MS.papapaa	Pharyngeal gland cells\n"+
			"gon_herm_anch	Z1.ppp/Z4.aaa	Anchor cell, induces vulva, herm gonad\n"+
			"gon_herm_dtc_A	Z1.aa	Anterior distal tip cell, inhibit meiosis in neighbour- ing germ cells, lead gonad during morphogenesis, herm gonad\n"+
			"gon_herm_dtc_P	Z4.pp	Posterior distal tip cell, inhibit meiosis in neighbour- ing germ cells, lead gonad during morphogenesis, herm gonad\n"+
			"gon_herm_dish_A	Z1.apa	Anterior epithelial sheaths of distal arms, no muscle fibers, herm gonad\n"+
			"gon_herm_dish_A	Z1.paaa	Anterior epithelial sheaths of distal arms, no muscle fibers, herm gonad\n"+
			"gon_herm_dish_P	Z4.pap	Posterior epithelial sheaths of distal arms, no muscle fibers, herm gonad\n"+
			"gon_herm_dish_P	Z4.appp	Posterior epithelial sheaths of distal arms, no muscle fibers, herm gonad\n"+
			"gon_herm_prsh_A	Z1.appaaa	Anterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_A	Z1.appaap	Anterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_A	Z1.appapa	Anterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_A	Z1.appapp	Anterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_A	Z1.paapaaa	Anterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_A	Z1.paapaap	Anterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_A	Z1.paapapa	Anterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_A	Z1.paapapp	Anterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_P	Z4.paapaa	Posterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_P	Z4.paapap	Posterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_P	Z4.paappa	Posterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_P	Z4.paappp	Posterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_P	Z4.appapaa	Posterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_P	Z4.appapap	Posterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_P	Z4.appappa	Posterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_prsh_P	Z4.appappp	Posterior epithelial sheaths of proximal arms, have muscle fibers\n"+
			"gon_herm_spth_A	Z1.apppaaaa	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.apppaaap	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.apppaapa	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.apppaapp	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.apppapaa	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.apppapap	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.apppapp	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.appppa	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.appppp	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.paappaaaa	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.paappaaap	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.paappaapa	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.paappaapp	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.paappapaa	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.paappapap	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.paappapp	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.paapppa	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.paapppp	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.papaaad	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.papaaav	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z1.papaapv	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z4.apaaaad	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z4.apaaaav	Anterior spermathecae\n"+
			"gon_herm_spth_A	Z4.apaaapv	Anterior spermathecae\n"+
			"gon_herm_spth_P	Z4.paaaaa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.paaaap	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.paaapaa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.paaapapa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.paaapapp	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.paaappaa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.paaappap	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.paaapppa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.paaapppp	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.appaaaa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.appaaap	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.appaapaa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.appaapapa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.appaapapp	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.appaappaa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.appaappap	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.appaapppa	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.appaapppp	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.apappav	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.apapppd	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z4.apapppv	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z1.papppav	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z1.pappppd	Posterior spermathecae\n"+
			"gon_herm_spth_P	Z1.pappppv	Posterior spermathecae\n"+
			"gon_herm_sujn_A	Z1.papaapd	Anterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_A	Z1.papapaaa	Anterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_A	Z1.ppaaaaa	Anterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_A	Z1.ppaaapa	Anterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_A	Z4.apaaapd	Anterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_A	Z4.apaapaaa	Anterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_P	Z1.pappappp	Posterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_P	Z1.papppad	Posterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_P	Z4.apapappp	Posterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_P	Z4.apappad	Posterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_P	Z4.aapppap	Posterior spermathecal-uterine junction\n"+
			"gon_herm_sujn_P	Z4.aappppp	Posterior spermathecal-uterine junction\n"+
			"gon_herm_dut	Z1.papapaap	Dorsal uterus\n"+
			"gon_herm_dut	Z1.papapapa	Dorsal uterus\n"+
			"gon_herm_dut	Z1.papapapp	Dorsal uterus\n"+
			"gon_herm_dut	Z1.papappaa	Dorsal uterus\n"+
			"gon_herm_dut	Z1.papappap	Dorsal uterus\n"+
			"gon_herm_dut	Z1.papapppa	Dorsal uterus\n"+
			"gon_herm_dut	Z1.papapppp	Dorsal uterus\n"+
			"gon_herm_dut	Z1.pappaaaa	Dorsal uterus\n"+
			"gon_herm_dut	Z1.pappaaap	Dorsal uterus\n"+
			"gon_herm_dut	Z1.pappaapa	Dorsal uterus\n"+
			"gon_herm_dut	Z1.pappaapp	Dorsal uterus\n"+
			"gon_herm_dut	Z1.pappapaa	Dorsal uterus\n"+
			"gon_herm_dut	Z1.pappapap	Dorsal uterus\n"+
			"gon_herm_dut	Z1.pappappa	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apaapaap	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apaapapa	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apaapapp	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apaappaa	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apaappap	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apaapppa	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apaapppp	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apapaaaa	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apapaaap	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apapaapa	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apapaapp	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apapapaa	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apapapap	Dorsal uterus\n"+
			"gon_herm_dut	Z4.apapappa	Dorsal uterus\n"+
			"gon_herm_vut	Z1.ppaaaap	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaaapp	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaapaa	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaapap	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaappa	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaappp	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppapaaa	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppapaap	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppapapa	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppapapp	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppappd	Ventral uterus\n"+
			"gon_herm_vut	Z1.pppaad	Ventral uterus\n"+
			"gon_herm_vut	Z1.pppaav	Ventral uterus\n"+
			"gon_herm_vut	Z1.pppapd	Ventral uterus\n"+
			"gon_herm_vut	Z1.pppapv	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppppad	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppppav	Ventral uterus\n"+
			"gon_herm_vut	Z1.pppppaa	Ventral uterus\n"+
			"gon_herm_vut	Z1.pppppap	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppppppa	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppppppp	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapaad	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapaav	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapapd	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapapv	Ventral uterus\n"+
			"gon_herm_vut	Z4.aappaaa	Ventral uterus\n"+
			"gon_herm_vut	Z4.aappaap	Ventral uterus\n"+
			"gon_herm_vut	Z4.aappapa	Ventral uterus\n"+
			"gon_herm_vut	Z4.aappapp	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapppaa	Ventral uterus\n"+
			"gon_herm_vut	Z4.aappppa	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaaaap	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaaapp	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaapaa	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaapap	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaappa	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppaappp	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppapad	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppapav	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppappd	Ventral uterus\n"+
			"gon_herm_vut	Z1.ppappv	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaaaaaa	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaaaaap	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaaaapa	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaaaapp	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaaapd	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaaapv	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaapad	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaapav	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaappv	Ventral uterus\n"+
			"gon_herm_vut	Z4.aaappd	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapaad	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapaav	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapapaa	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapapap	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapappa	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapappp	Ventral uterus\n"+
			"gon_herm_vut	Z4.aappaaa	Ventral uterus\n"+
			"gon_herm_vut	Z4.apppaap	Ventral uterus\n"+
			"gon_herm_vut	Z4.aappppa	Ventral uterus\n"+
			"gon_herm_vut	Z4.aappppp	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapppaa	Ventral uterus\n"+
			"gon_herm_vut	Z4.aapppap	Ventral uterus\n"+
			"gon_male_dtc	Z1.a	Distal tip cell, inhibits meiosis in nearby germ cells\n"+
			"gon_male_dtc	Z1.p	Distal tip cell, inhibits meiosis in nearby grem cells\n"+
			"gon_male_link	Z1.paa/Z4.aaa	Linker cell, leads gonad during morphogenesis and initiates union with cloaca\n"+
			"gon_male_sves	Z1.ppaaaaa	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.ppaaaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.ppaaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.ppaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.ppap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.pppaaaa	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.pppaaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.pppaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.pppap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.pppp	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.pappp	Seminal vesicle, outer\n"+
			"gon_male_sves	Z4.aaapp	Seminal vesicle, outer\n"+
			"gon_male_sves	Z4.aappp	Seminal vesicle, outer\n"+
			"gon_male_sves	Z4.apaaaaa	Seminal vesicle, inner\n"+
			"gon_male_sves	Z4.apaaaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z4.apaaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z4.apaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z4.apap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z4.appaaaa	Seminal vesicle, inner\n"+
			"gon_male_sves	Z4.appaaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z4.appaap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z4.appap	Seminal vesicle, inner\n"+
			"gon_male_sves	Z4.appp	Seminal vesicle, inner\n"+
			"gon_male_sves	Z1.paapp	Seminal vesicle, outer\n"+
			"gon_male_vdef	Z1.papaaaa	Vas deferens\n"+
			"gon_male_vdef	Z1.papaaap	Vas deferens\n"+
			"gon_male_vdef	Z1.papaapaa	Vas deferens\n"+
			"gon_male_vdef	Z1.papaapap	Vas deferens\n"+
			"gon_male_vdef	Z1.papaappaa	Vas deferens\n"+
			"gon_male_vdef	Z1.papaappap	Vas deferens\n"+
			"gon_male_vdef	Z1.papaappp	Vas deferens\n"+
			"gon_male_vdef	Z1.papapa	Vas deferens\n"+
			"gon_male_vdef	Z1.papapp	Vas deferens\n"+
			"gon_male_vdef	Z1.pappa	Vas deferens\n"+
			"gon_male_vdef	Z4.aaaaaaa	Vas deferens\n"+
			"gon_male_vdef	Z4.aaaaaap	Vas deferens\n"+
			"gon_male_vdef	Z4.aaaaapaa	Vas deferens\n"+
			"gon_male_vdef	Z4.aaaaapap	Vas deferens\n"+
			"gon_male_vdef	Z4.aaaaappaa	Vas deferens\n"+
			"gon_male_vdef	Z4.aaaaappap	Vas deferens\n"+
			"gon_male_vdef	Z4.aaaaappp	Vas deferens\n"+
			"gon_male_vdef	Z4.aaaapa	Vas deferens\n"+
			"gon_male_vdef	Z4.aaaapp	Vas deferens\n"+
			"gon_male_vdef	Z4.aaapa	Vas deferens\n"+
			"gon_male_vdef	Z4.aapaaaa	Vas deferens\n"+
			"gon_male_vdef	Z4.aapaaap	Vas deferens\n"+
			"gon_male_vdef	Z4.aapaapaa	Vas deferens\n"+
			"gon_male_vdef	Z4.aapaapap	Vas deferens\n"+
			"gon_male_vdef	Z4.aapaappaa	Vas deferens\n"+
			"gon_male_vdef	Z4.aapaappap	Vas deferens\n"+
			"gon_male_vdef	Z4.aapaappp	Vas deferens\n"+
			"gon_male_vdef	Z4.aapapa	Vas deferens\n"+
			"gon_male_vdef	Z4.aapapp	Vas deferens\n"+
			"gon_male_vdef	Z4.aappa	Vas deferens\n"+
			"gon_male_vdef	Z1.paaaaaa	Vas deferens\n"+
			"gon_male_vdef	Z1.paaaaap	Vas deferens\n"+
			"gon_male_vdef	Z1.paaaapaa	Vas deferens\n"+
			"gon_male_vdef	Z1.paaaapap	Vas deferens\n"+
			"gon_male_vdef	Z1.paaaappaa	Vas deferens\n"+
			"gon_male_vdef	Z1.paaaappap	Vas deferens\n"+
			"gon_male_vdef	Z1.paaaappp	Vas deferens\n"+
			"gon_male_vdef	Z1.paaapa	Vas deferens\n"+
			"gon_male_vdef	Z1.paaapp	Vas deferens\n"+
			"gon_male_vdef	Z1.paapa	Vas deferens\n"+
			"hmc	MS.appaaa	Head mesodermal cell, function unknown\n"+
			"hyp1	AB.alpaapppa	Cylindrical hypodermal syncytium in head\n"+
			"hyp1	AB.araaapppp	Cylindrical hypodermal syncytium in head\n"+
			"hyp1	AB.arappaapa	Cylindrical hypodermal syncytium in head\n"+
			"hyp2	AB.alpaapppp	Cylindrical hypodermal syncytium in head\n"+
			"hyp2	AB.alpapaaap	Cylindrical hypodermal syncytium in head\n"+
			"hyp3	AB.plaapaaaa	Cylindrical hypodermal syncytium in head\n"+
			"hyp3	AB.praapaaaa	Cylindrical hypodermal syncytium in head\n"+
			"hyp4	AB.arpapapa	Cylindrical hypodermal syncytium in head\n"+
			"hyp4	AB.plaappaa	Cylindrical hypodermal syncytium in head\n"+
			"hyp4	AB.praappaa	Cylindrical hypodermal syncytium in head\n"+
			"hyp5	AB.arpappap	Cylindrical hypodermal syncytium in head\n"+
			"hyp5	AB.plaaapap	Cylindrical hypodermal syncytium in head\n"+
			"hyp6	AB.arpaapaa	Cylindrical hypodermal syncytium in head\n"+
			"hyp6	AB.arpapapp	Cylindrical hypodermal syncytium in head\n"+
			"hyp6	AB.plaaaapa	Cylindrical hypodermal syncytium in head\n"+
			"hyp6	AB.plaaaapp	Cylindrical hypodermal syncytium in head\n"+
			"hyp6	AB.plaappap	Cylindrical hypodermal syncytium in head\n"+
			"hyp6	AB.praappap	Cylindrical hypodermal syncytium in head\n"+
			"hyp7	AB.arpaapap	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.arpaappa	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.arpaappp	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.arppaapa	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.arpppapa	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.plaapppa	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.plaapppp	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.plappppa	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.praapppa	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.praapppp	Embryonic large hypodermal syncytium\n"+
			"hyp7	AB.prappppa	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.aaaaa	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.aaaap	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.aaapa	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.aaapp	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.aappd	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.paaaa	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.paaap	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.paapa	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.paapp	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.papaa	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.papap	Embryonic large hypodermal syncytium\n"+
			"hyp7	C.pappd	Embryonic large hypodermal syncytium\n"+
			"hyp7	H1L.apa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H1L.appa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H1L.p	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H1R.apa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H1R.appa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H1R.p	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H2L.ap	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H2L.pa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H2L.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H2L.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H2R.ap	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H2R.pa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H2R.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	H2R.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1L.a	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1L.paa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1L.papa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1L.pappa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1L.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1L.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1L.ppppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1R.a	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1R.paa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1R.papa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1R.pappa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1R.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1R.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V1R.ppppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2L.a	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2L.paa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2L.papa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2L.pappa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2L.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2L.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2L.ppppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2R.a	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2R.paa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2R.papa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2R.pappa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2R.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2R.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V2R.ppppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3L.a	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3L.paa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3L.papa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3L.pappa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3L.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3L.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3L.ppppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3R.a	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3R.paa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3R.papa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3R.pappa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3R.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3R.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V3R.ppppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4L.a	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4L.paa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4L.papa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4L.pappa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4L.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4L.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4L.ppppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4R.a	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4R.paa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4R.papa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4R.pappa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4R.ppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4R.pppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V4R.ppppa	Postembryonic large hypodermal syncytium\n"+
			"hyp7	V5L.a	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V5L.ppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V5L.pppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V5L.ppppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V5R.a	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V5R.ppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V5R.pppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V5R.ppppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6L.a	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6L.paa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6L.papa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6L.pappa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6L.ppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6L.pppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6L.ppppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6R.a	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6R.paa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6R.papa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6R.pappa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6R.ppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6R.pppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V6R.ppppa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	TL.aa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	TL.apaa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	TL.apap	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	TR.aa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	TR.apaa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	TR.apap	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	V5L.a	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V5L.ppa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V5L.pppaa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V5L.pppapa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V5L.ppppa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V5R.a	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V5R.ppa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V5R.pppaa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V5R.pppapa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V5R.ppppa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6L.a	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6L.paa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6L.papaa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6L.pappa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6L.ppa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6L.pppaa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6L.ppppap	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6R.a	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6R.paa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6R.papaa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6R.pappa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6R.ppa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6R.pppaa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	V6R.ppppap	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TL.aa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TL.apaa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TL.apapa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TL.apappp	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TL.appaap	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TL.appapp	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TR.aa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TR.apaa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TR.apapa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TR.apappp	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TR.appaap	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	TR.appapp	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	P1.p	Postembryonic large hypodermal syncytium\n"+
			"hyp7	P2.p	Postembryonic large hypodermal syncytium\n"+
			"hyp7	P3.pa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	P3.pp	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	P4.pa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	P4.pp	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	P8.pa	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	P8.pp	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	P9.p	Postembryonic large hypodermal syncytium\n"+
			"hyp7	P10.p	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	P11.p	Postembryonic large hypodermal syncytium, hermaphrodite\n"+
			"hyp7	P3.p	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	P4.p	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	P5.p	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	P6.p	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	P7.p	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	P8.p	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	P10.paaa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	P10.paap	Postembryonic large hypodermal syncytium, male\n"+
			"hyp7	P10.papa	Postembryonic large hypodermal syncytium, male\n"+
			"hyp8/9	AB.plpppapap	Tail ventral hypodermis\n"+
			"hyp8/9	AB.prpppapap	Tail ventral hypodermis\n"+
			"hyp10	AB.plppppppp	Tail ventral hypodermis\n"+
			"hyp10	AB.prppppppp	Tail ventral hypodermis\n"+
			"hyp11	C.pappa	Tail dorsal hypodermis\n"+
			"hyp12	P12.pa	Preanal hypodermis\n"+
			"hyp_hook	P10.papp	Hook\n"+
			"hyp_hook	P11.ppaa	Hypodermis associated with hook sensillum of male\n"+
			"hyp_hook	P11.ppap	Hypodermis associated with hook sensillum of male\n"+
			"hyp_hook	P11.ppp	Hypodermis associated with hook sensillum of male\n"+
			"int	E.alaad	Intestinal cell\n"+
			"int	E.alaav	Intestinal cell\n"+
			"int	E.alap	Intestinal cell\n"+
			"int	E.alpa	Intestinal cell\n"+
			"int	E.alpp	Intestinal cell\n"+
			"int	E.araad	Intestinal cell\n"+
			"int	E.araav	Intestinal cell\n"+
			"int	E.arap	Intestinal cell\n"+
			"int	E.arpa	Intestinal cell\n"+
			"int	E.arpp	Intestinal cell\n"+
			"int	E.plaa	Intestinal cell\n"+
			"int	E.plap	Intestinal cell\n"+
			"int	E.plpa	Intestinal cell\n"+
			"int	E.plppa	Intestinal cell\n"+
			"int	E.plppp	Intestinal cell\n"+
			"int	E.praa	Intestinal cell\n"+
			"int	E.prap	Intestinal cell\n"+
			"int	E.prpa	Intestinal cell\n"+
			"int	E.prppa	Intestinal cell\n"+
			"int	E.prppp	Intestinal cell\n"+
			"int	In.a	Postembryonic intestinal cell\n"+
			"int	In.p	Postembryonic intestinal cell\n"+
			"linker_killer	U.lp or U.rp	One of these cells, sometimes fused with U.l/ra, phagocytoses the male linker cell\n"+
			"m1DL_of_pm1	AB.araapaaap	Pharyngeal muscle cell\n"+
			"m1DR_of_pm1	AB.araappaap	Pharyngeal muscle cell\n"+
			"m1L_of_pm1	AB.araaaaaap	Pharyngeal muscle cell\n"+
			"m1R_of_pm1	AB.araaaaapp	Pharyngeal muscle cell\n"+
			"m1VL_of_pm1	AB.alpaaaapa	Pharyngeal muscle cell\n"+
			"m1VR_of_pm1	AB.araaaaapa	Pharyngeal muscle cell\n"+
			"m2DL_of_pm2	AB.araapaapa	Pharyngeal muscle cell\n"+
			"m2DR_of_pm2	AB.araappapa	Pharyngeal muscle cell\n"+
			"m2L_of_pm2	AB.alpaaapaa	Pharyngeal muscle cell\n"+
			"m2R_of_pm2	AB.arapaapaa	Pharyngeal muscle cell\n"+
			"m2VL_of_pm2	AB.alpaaaaap	Pharyngeal muscle cell\n"+
			"m2VR_of_pm2	AB.arapaaaap	Pharyngeal muscle cell\n"+
			"m3DL	MS.aaapaaa	Pharyngeal muscle cell\n"+
			"m3DR	MS.paaaapa	Pharyngeal muscle cell\n"+
			"m3L	AB.alpaapapp	Pharyngeal muscle cell\n"+
			"m3R	AB.arapaappa	Pharyngeal muscle cell\n"+
			"m3VL	AB.alpappppp	Pharyngeal muscle cell\n"+
			"m3VR	AB.arapapppp	Pharyngeal muscle cell\n"+
			"m4DL	MS.aaaaapp	Pharyngeal muscle cell\n"+
			"m4DR	MS.paaaapp	Pharyngeal muscle cell\n"+
			"m4L	MS.aaapaap	Pharyngeal muscle cell\n"+
			"m4R	AB.araaapapp	Pharyngeal muscle cell\n"+
			"m4VL	MS.aapaaaa	Pharyngeal muscle cell\n"+
			"m4VR	MS.papappp	Pharyngeal muscle cell\n"+
			"m5DL	MS.aaaapap	Pharyngeal muscle cell\n"+
			"m5DR	MS.paaappa	Pharyngeal muscle cell\n"+
			"m5L	AB.araapapap	Pharyngeal muscle cell\n"+
			"m5R	AB.araapppap	Pharyngeal muscle cell\n"+
			"m5VL	MS.aapaaap	Pharyngeal muscle cell\n"+
			"m5VR	MS.papaaap	Pharyngeal muscle cell\n"+
			"m6D	MS.paaappp	Pharyngeal muscle cell\n"+
			"m6VL	MS.aapappa	Pharyngeal muscle cell\n"+
			"m6VR	MS.papappa	Pharyngeal muscle cell\n"+
			"m7D	MS.aaaappp	Pharyngeal muscle cell\n"+
			"m7VL	MS.aapaapp	Pharyngeal muscle cell\n"+
			"m7VR	MS.papaapp	Pharyngeal muscle cell\n"+
			"m8	MS.aaapapp	Pharyngeal muscle cell\n"+
			"mc1DL	AB.alpaapapa	Pharyngeal marginal cell\n"+
			"mc1DR	AB.araaapapa	Pharyngeal marginal cell\n"+
			"mc1V	AB.alpappppa	Pharyngeal marginal cell\n"+
			"mc2DL	AB.araapaapp	Pharyngeal marginal cell\n"+
			"mc2DR	AB.araappapp	Pharyngeal marginal cell\n"+
			"mc2V	AB.arapapppa	Pharyngeal marginal cell\n"+
			"mc3DL	MS.aaapapa	Pharyngeal marginal cell\n"+
			"mc3DR	MS.paapapa	Pharyngeal marginal cell\n"+
			"mc3V	AB.alpappapp	Pharyngeal marginal cell\n"+
			"mu_anal	AB.plpppppap	Anal depressor muscle\n"+
			"VR20_VBW_muscle	AB.prpppppaa	Body wall muscle\n"+
			"VL9_VBW_muscle	C.apaaaa	Body wall muscle\n"+
			"VL11_VBW_muscle	C.apaaap	Body wall muscle\n"+
			"VL13_VBW_muscle	C.apaapa	Body wall muscle\n"+
			"VL15_VBW_muscle	C.apaapp	Body wall muscle\n"+
			"DL13_DBW_muscle	C.apapaa	Body wall muscle\n"+
			"DL17_DBW_muscle	C.apapap	Body wall muscle\n"+
			"VL20_VBW_muscle	C.apappa	Body wall muscle\n"+
			"VL22_VBW_muscle	C.apappp	Body wall muscle\n"+
			"DL11_DBW_muscle	C.appaaa	Body wall muscle\n"+
			"DL18_DBW_muscle	C.appaap	Body wall muscle\n"+
			"DL19_DBW_muscle	C.appapa	Body wall muscle\n"+
			"DL23_DBW_muscle	C.appapp	Body wall muscle\n"+
			"DL20_DBW_muscle	C.apppaa	Body wall muscle\n"+
			"DL22_DBW_muscle	C.apppap	Body wall muscle\n"+
			"DL24_DBW_muscle	C.appppd	Body wall muscle\n"+
			"VL23_VBW_muscle	C.appppv	Body wall muscle\n"+
			"VR9_VBW_muscle	C.ppaaaa	Body wall muscle\n"+
			"VR11_VBW_muscle	C.ppaaap	Body wall muscle\n"+
			"VR13_VBW_muscle	C.ppaapa	Body wall muscle\n"+
			"VR15_VBW_muscle	C.ppaapp	Body wall muscle\n"+
			"DR13_DBW_muscle	C.ppapaa	Body wall muscle\n"+
			"DR17_DBW_muscle	C.ppapap	Body wall muscle\n"+
			"VR19_VBW_muscle	C.ppappa	Body wall muscle\n"+
			"VR22_VBW_muscle	C.ppappp	Body wall muscle\n"+
			"DR11_DBW_muscle	C.pppaaa	Body wall muscle\n"+
			"DR18_DBW_muscle	C.pppaap	Body wall muscle\n"+
			"DR19_DBW_muscle	C.pppapa	Body wall muscle\n"+
			"DR23_DBW_muscle	C.pppapp	Body wall muscle\n"+
			"DR20_DBW_muscle	C.ppppaa	Body wall muscle\n"+
			"DR22_DBW_muscle	C.ppppap	Body wall muscle\n"+
			"DR24_DBW_muscle	C.pppppd	Body wall muscle\n"+
			"VR24_VBW_muscle	C.pppppv	Body wall muscle\n"+
			"VL5_VBW_muscle	D.aaaa	Body wall muscle\n"+
			"DL7_DBW_muscle	D.aaap	Body wall muscle\n"+
			"VL4_VBW_muscle	D.aapa	Body wall muscle\n"+
			"VL6_VBW_muscle	D.aapp	Body wall muscle\n"+
			"DL8_DBW_muscle	D.appaa	Body wall muscle\n"+
			"DL10_DBW_muscle	D.aappap	Body wall muscle\n"+
			"DL12_DBW_muscle	D.aapppa	Body wall muscle\n"+
			"DL14_DBW_muscle	D.aapppp	Body wall muscle\n"+
			"VR5_VBW_muscle	D.paaa	Body wall muscle\n"+
			"DR7_DBW_muscle	D.paap	Body wall muscle\n"+
			"VR4_VBW_muscle	D.papa	Body wall muscle\n"+
			"VR6_VBW_muscle	D.papp	Body wall muscle\n"+
			"VR7_VBW_muscle	D.ppaa	Body wall muscle\n"+
			"DR9_DBW_muscle	D.ppap	Body wall muscle\n"+
			"DR8_DBW_muscle	D.pppaa	Body wall muscle\n"+
			"DR10_DBW_muscle	D.pppap	Body wall muscle\n"+
			"DR12_DBW_muscle	D.ppppa	Body wall muscle\n"+
			"DR14_DBW_muscle	D.ppppp	Body wall muscle\n"+
			"VL8_VBW_muscle	MS.aapppaa	Body wall muscle\n"+
			"VL10_VBW_muscle	MS.aapppap	Body wall muscle\n"+
			"VL12_VBW_muscle	MS.aappppa	Body wall muscle\n"+
			"VL14_VBW_muscle	MS.aappppp	Body wall muscle\n"+
			"DL1_DBW_muscle	MS.apaaap	Body wall muscle\n"+
			"VL1_VBW_muscle	MS.apapap	Body wall muscle\n"+
			"DL2_DBW_muscle	MS.apappa	Body wall muscle\n"+
			"DL3_DBW_muscle	MS.apappp	Body wall muscle\n"+
			"VL2_VBW_muscle	MS.appapp	Body wall muscle\n"+
			"VL3_VBW_muscle	MS.apppaa	Body wall muscle\n"+
			"DL5_DBW_muscle	MS.apppap	Body wall muscle\n"+
			"DL4_DBW_muscle	MS.appppa	Body wall muscle\n"+
			"DL6_DBW_muscle	MS.appppp	Body wall muscle\n"+
			"VL16_VBW_muscle	MS.pappaa	Body wall muscle\n"+
			"VR16_VBW_muscle	MS.pappap	Body wall muscle\n"+
			"VR8_VBW_muscle	MS.papppaa	Body wall muscle\n"+
			"VR10_VBW_muscle	MS.papppap	Body wall muscle\n"+
			"VR12_VBW_muscle	MS.pappppa	Body wall muscle\n"+
			"VR14_VBW_muscle	MS.pappppp	Body wall muscle\n"+
			"DR1_DBW_muscle	MS.ppaaap	Body wall muscle\n"+
			"VR1_VBW_muscle	MS.ppapap	Body wall muscle\n"+
			"DR2_DBW_muscle	MS.ppappa	Body wall muscle\n"+
			"DR3_DBW_muscle	MS.ppappp	Body wall muscle\n"+
			"VR2_VBW_muscle	MS.pppapp	Body wall muscle\n"+
			"VR3_VBW_muscle	MS.ppppaa	Body wall muscle\n"+
			"DR5_DBW_muscle	MS.ppppap	Body wall muscle\n"+
			"DR4_DBW_muscle	MS.pppppa	Body wall muscle\n"+
			"DR6_DBW_muscle	MS.pppppp	Body wall muscle\n"+
			"mu_int_L	AB.plpppppaa	Intestinal muscles, attach to intestine and body wall anterior to anus\n"+
			"mu_int_R	MS.ppaapp	Intestinal muscles, attach to intestine and body wall anterior to anus\n"+
			"mu_male_diag	M.dlpaaaap	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.dlpaaapa	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.dlpaaapp	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.dlpaapaa	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.dlpaapap	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.dlpapaaa	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.dlpapaap	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.drpaaaap	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.drpaaapa	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.drpaaapp	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.drpaapaa	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.drpaappa	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.drpaapap	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.drpapaaa	Diagonal tail muscles in male body wall\n"+
			"mu_male_diag	M.drpapaap	Diagonal tail muscles in male body wall\n"+
			"mu_male_gub	M.dlpappap	Gubernaculum erector\n"+
			"mu_male_gub	M.drpappap	Gubernaculum erector\n"+
			"mu_male_gub	M.dlpapapa	Gubernaculum retractor\n"+
			"mu_male_gub	M.drpapapa	Gubernaculum retractor\n"+
			"mu_male_long	M.vlpaaaaa	Anterior outer longitudinal male muscle\n"+
			"mu_male_long	M.vlpaaaap	Posterior outer longitudinal male muscle\n"+
			"mu_male_long	M.vlpaaapa	Anterior inner longitudinal male muscle\n"+
			"mu_male_long	M.vlpaaapp	Posterior inner longitudinal male muscle\n"+
			"mu_male_long	M.vlpaapp	Caudal longitudinal male muscle\n"+
			"mu_male_long	M.vrpaaaaa	Anterior outer longitudinal male muscle\n"+
			"mu_male_long	M.vrpaaaap	Posterior outer longitudinal male muscle\n"+
			"mu_male_long	M.vrpaaapa	Anterior inner longitudinal male muscle\n"+
			"mu_male_long	M.vrpaaapp	Posterior inner longitudinal male muscle\n"+
			"mu_male_long	M.vrpaapp	Caudal longitudinal male muscle\n"+
			"mu_male_obliq	M.dlpapapp	Anterior oblique male muscle\n"+
			"mu_male_obliq	M.dlpappaa	Posterior oblique male muscle\n"+
			"mu_male_obliq	M.drpapapp	Anterior oblique male muscle\n"+
			"mu_male_obliq	M.drpappap	Posterior oblique male muscle\n"+
			"mu_male_spic	M.dlpaaaaa	Ventral spicule protractor, male\n"+
			"mu_male_spic	M.dlpaappa	Ventral spicule retractor, male\n"+
			"mu_male_spic	M.dlpaappp	Dorsal spicule retractor, male\n"+
			"mu_male_spic	M.prpaaaaa	Ventral spicule protractor, male\n"+
			"mu_male_spic	M.drpaappp	Ventral spicule protractor, male\n"+
			"mu_male_spic	M.drpappaa	Dorsal spicule protractor, male\n"+
			"mu_male_spic	M.vlpaapa	Dorsal spicule protractor, male\n"+
			"mu_male_spic	M.vrpaapa	Dorsal spicule protractor, male\n"+
			"mu_sph	AB.prpppppap	Sphincter muscle of intestino-rectal valve\n"+
			"um2	M.vlpaaaaa	Uterine muscle 2\n"+
			"um1	M.vlpaaaap	Uterine muscle 1\n"+
			"um1	M.vlpaappa	Uterine muscle 1\n"+
			"um2	M.vlpaappp	Uterine muscle 2\n"+
			"um2	M.vrpaaaaa	Uterine muscle 2\n"+
			"um1	M.vrpaaaap	Uterine muscle 1\n"+
			"um1	M.vrpaappa	Uterine muscle 1\n"+
			"um2	M.vrpaappp	Uterine muscle 2\n"+
			"vm2	M.vlpaaapa	Vulval muscle 2\n"+
			"vm1	M.vlpaaapp	Vulval muscle 1\n"+
			"vm1	M.vlpaapaa	Vulval muscle 1\n"+
			"vm2	M.vlpaapap	Vulval muscle 2\n"+
			"vm2	M.vrpaaapa	Vulval muscle 2\n"+
			"vm1	M.vrpaaapp	Vulval muscle 1\n"+
			"vm1	M.vrpaapaa	Vulval muscle 1\n"+
			"vm2	M.vrpaapap	Vulval muscle 2\n"+
			"proct	B.alaalv/B.araalv	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alaarv/B.araarv	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alapaad	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alapapa	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alapapp	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alappv	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alppalv/B.arppalv	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alppald/B.arppald	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alpparv/B.arpparv	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alppl/B.arppl	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.alppr/B.arppr	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.arapaad	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.arapapa	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.arapapp	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.arappv	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.paa	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.pap	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	B.pppa	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	F.lvv	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"proct	F.rvv	Male proctodeum, union of intestine and vas deferens, contains copulatory spicules\n"+
			"R1_hyp	V5.pppppp, R1.p	Ray 1 hypodermal cell, fuses with other ray hypodermal cells to form the tail seam\n"+
			"R2_hyp	V6.papapp, R2.p	Ray 2 hypodermal cell, fuses with other ray hypodermal cells to form the tail seam\n"+
			"R3_hyp	V6.papppp, R3.p	Male sensory ray 3 hypodermal cell, fuses with other ray hypodermal cells to form the tail seam\n"+
			"R4_hyp	V6.pppapp, R4.p	Male sensory ray 4 hypodermal cell, fuses with other ray hypodermal cells to form the tail seam\n"+
			"R5_hyp	V6.ppppap, R5.p	Male sensory ray 5 hypodermal cell, fuses with other ray hypodermal cells to form the tail seam\n"+
			"R6_hyp	V6.pppppp, R6.p	Male sensory ray 6 hypodermal cell, fuses with hypodermal syncytium\n"+
			"R7_hyp	T.apappp, R7.p	Male sensory ray 7 hypodermal cell, fuses with hypodermal syncytium\n"+
			"R8_hyp	T.appaap, R8.p	Male sensory ray 8 hypodermal cell, fuses with hypodermal syncytium\n"+
			"R9_hyp	T.appapp, R9.p	Male sensory ray 9 hypodermal cell, fuses with hypodermal syncytium\n"+
			"rect_D	AB.plpappppp	Rectal epithelial cells, adjacent to intestino-rectal valve, have microvilli\n"+
			"rect_VL	AB.plppppaap	Rectal epithelial cells, adjacent to intestino-rectal valve, have microvilli\n"+
			"rect_VR	AB.prppppaap	Rectal epithelial cells, adjacent to intestino-rectal valve, have microvilli\n"+
			"se	H1L.aa	Postembryonic seam hypodermal cells, make alae\n"+
			"se	H1L.appp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	H1R.aa	Postembryonic seam hypodermal cells, make alae\n"+
			"se	H1R.appp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	H2L.pppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	H2R.pppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V1L.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V1L.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V1R.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V1R.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V2L.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V2L.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V2R.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V2R.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V3L.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V3L.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V3R.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V3R.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V4L.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V4L.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V4R.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se	V4R.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se_herm	V5L.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se_herm	V5R.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se_herm	V6L.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se_herm	V6L.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se_herm	V6R.pappp	Postembryonic seam hypodermal cells, make alae\n"+
			"se_herm	V6R.ppppp	Postembryonic seam hypodermal cells, make alae\n"+
			"se_herm	TL.appa	Postembryonic seam hypodermal cells, make alae\n"+
			"se_herm	TR.appa	Postembryonic seam hypodermal cells, make alae\n"+
			"se_male	V5L.pppapp	Postembryonic seam hypodermal cells, make alae\n"+
			"se_male	V5R.pppapp	Postembryonic seam hypodermal cells, make alae\n"+
			"set	V5L.pppppp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"set	V5R.pppppp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"set	V6L.papapp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"set	V6L.papppp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"set	V6L.pppapp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"set	V6L.pppppp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"set	V6R.papapp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"set	V6R.papppp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"set	V6R.pppapp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"set	V6R.pppppp	Mail tail seam, does not fuse with main seam, does not make alae\n"+
			"spike	AB.plppppppa	Used during embryogenesis to make tail spike, then die\n"+
			"spike	AB.prppppppa	Used during embryogenesis to make tail spike, then die\n"+
			"virL	AB.prpappppp	Intestino-rectal valve\n"+
			"virR	AB.prpappppa	Intestino-rectal valve\n"+
			"vpi1	MS.paapapp	Pharyngo-intestinal valve\n"+
			"vpi2DL	MS.aapappp	Pharyngo-intestinal valve\n"+
			"vpi2DR	MS.papappp	Pharyngo-intestinal valve\n"+
			"vpi2V	MS.aappaa	Pharyngo-intestinal valve\n"+
			"vpi3D	MS.aaappp	Pharyngo-intestinal valve\n"+
			"vpi3V	MS.aappap	Pharyngo-intestinal valve\n"+
			"vulva	P5.paaa	Hermaphrodite vulva\n"+
			"vulva	P5.paap	Hermaphrodite vulva\n"+
			"vulva	P5.papa	Hermaphrodite vulva\n"+
			"vulva	P5.papp	Hermaphrodite vulva\n"+
			"vulva	P5.ppal	Hermaphrodite vulva\n"+
			"vulva	P5.ppar	Hermaphrodite vulva\n"+
			"vulva	P5.ppp	Hermaphrodite vulva\n"+
			"vulva	P6.paal	Hermaphrodite vulva\n"+
			"vulva	P6.paar	Hermaphrodite vulva\n"+
			"vulva	P6.papl	Hermaphrodite vulva\n"+
			"vulva	P6.papr	Hermaphrodite vulva\n"+
			"vulva	P6.ppal	Hermaphrodite vulva\n"+
			"vulva	P6.ppar	Hermaphrodite vulva\n"+
			"vulva	P6.pppl	Hermaphrodite vulva\n"+
			"vulva	P6.pppr	Hermaphrodite vulva\n"+
			"vulva	P7.paa	Hermaphrodite vulva\n"+
			"vulva	P7.papl	Hermaphrodite vulva\n"+
			"vulva	P7.papr	Hermaphrodite vulva\n"+
			"vulva	P7.ppaa	Hermaphrodite vulva\n"+
			"vulva	P7.ppap	Hermaphrodite vulva\n"+
			"vulva	P7.pppa	Hermaphrodite vulva\n"+
			"vulva	P7.pppp	Hermaphrodite vulva" +
			"";
