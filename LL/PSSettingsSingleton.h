//
//  PSSettingsSingleton.h
//  WormGUIDES
//
//  Created by Raul Catena on 9/5/14.
//  Copyright (c) 2014 Raul Catena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSSettingsSingleton : NSObject

+(PSSettingsSingleton *)sharedInstance;

@property (nonatomic, assign)  BOOL searchSystematic;
@property (nonatomic, assign) BOOL searchProper;
@property (nonatomic, assign) BOOL searchDescription;
@property (nonatomic, assign) BOOL searchGene;
@property (nonatomic, assign)  BOOL searchCell;
@property (nonatomic, assign) BOOL searchAncestors;
@property (nonatomic, assign) BOOL searchDescendants;

-(BOOL)areOptionsValid;
-(int)propagationType;
-(int)typeOfSearch;
-(void)defaults;

@end
