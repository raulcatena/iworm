/*
 *	IWViewController.m
 *	iWorm
 *	
 *	Created by Raul Catena on 2/26/13.
 *	Copyright 2013 __MyCompanyName__. All rights reserved.
 */

#import "IWViewController.h"
#import "IWAppDelegate.h"
#import "CellTime.h"
#import "Cell.h"
#import "TimeWorm.h"
#import "ColorScheme.h"
#import "Search.h"
#import "LineageTracing.h"
#import "LegendViewController.h"
#import "SearchController.h"
#import "InfoViewcontrollerViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "RCFMesh.h"

#define LABELWIDTH 180
#define LABELHEIGHT 25
#define MAXTIMEPOINT 171

@implementation IWViewController

@synthesize popover = _popover;
@synthesize searchText = _searchText;
@synthesize searchArray = _searchArray;
@synthesize currentColorScheme = _currentColorScheme;
@synthesize nextColorScheme = _nextColorScheme;
@synthesize itemsInCurrentSearch = _itemsInCurrentSearch;
@synthesize ngView = _ngView;


#pragma mark memory management
- (void) dealloc
{
	[_mesh release];
	[_camera release];
    [_group release];
    [_popover release];
    [_searchText release];
    [_searchArray release];
    [_currentColorScheme release];
    [_nextColorScheme release];
    [_itemsInCurrentSearch release];
    [_ngView release];
    
    [transf release];//For development only
    [transpar release];//For development only
	
	[super dealloc];
}

-(void)viewDidUnload{
    [super viewDidUnload];
    self.ngView = nil;
}

#pragma mark viewcontroller life cycle
//iOS must implement method for View Controllers
- (void) viewDidLoad
{
	// Must call super to agree with the UIKit rules.
	[super viewDidLoad];
    self.view.multipleTouchEnabled = YES;
    
    //Setting all the navigation buttons
    UIBarButtonItem *spacer = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil]autorelease];
    spacer.width = 20;
    
    UIBarButtonItem *themes = [[[UIBarButtonItem alloc]initWithTitle:@"Themes" style:UIBarButtonItemStyleBordered target:self action:@selector(action:)]autorelease];
    
    UIButton *envelopeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *envelope = [UIImage imageNamed:@"Envelope.png"];
    envelopeButton.frame = CGRectMake(0, 0, 30, 22);
    [envelopeButton setImage:envelope forState:UIControlStateNormal];
    [envelopeButton addTarget:self action:@selector(sendScreen) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *envBBI = [[[UIBarButtonItem alloc]initWithCustomView:envelopeButton]autorelease];
    
    UIButton *cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *camera = [UIImage imageNamed:@"Camera.png"];
    cameraButton.frame = CGRectMake(0, 0, 30, 22);
    [cameraButton setImage:camera forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(saveScreen) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *camBBI = [[[UIBarButtonItem alloc]initWithCustomView:cameraButton]autorelease];
    
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:spacer, themes, spacer, envBBI, spacer, camBBI, nil];
    
    UIButton *magGlassButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *magGlass = [UIImage imageNamed:@"magGlass.png"];
    magGlassButton.frame = CGRectMake(0, 0, 22, 22);
    [magGlassButton setImage:magGlass forState:UIControlStateNormal];
    [magGlassButton addTarget:self action:@selector(search:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *magGlassBBI = [[[UIBarButtonItem alloc]initWithCustomView:magGlassButton]autorelease];
    
    UIButton *infoBut = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [infoBut addTarget:self action:@selector(goToInfo:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *info = [[[UIBarButtonItem alloc]initWithCustomView:infoBut]autorelease];
    UIButton *paletteBut = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *paletteImg = [UIImage imageNamed:@"palette.png"];
    paletteBut.bounds = CGRectMake(0, 0, 30, 30);
    [paletteBut setImage:paletteImg forState:UIControlStateNormal];
    [paletteBut addTarget:self action:@selector(goToPalette:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *palette = [[[UIBarButtonItem alloc]initWithCustomView:paletteBut]autorelease];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:info, spacer, palette, spacer, magGlassBBI, nil];
    
    //Set the current color scheme
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ColorScheme"];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    request.predicate = [NSPredicate predicateWithFormat:@"isCurrent = YES"];
    request.sortDescriptors = [NSArray arrayWithObject:sort];
    NSArray *array = [self.managedObjectContext executeFetchRequest:request error:nil];
    NSLog(@"Found %i colorSchemes", array.count);
    self.currentColorScheme = (ColorScheme *)array.lastObject;
    //[self eraseSearchesInCurrent];
    
    //Initialize the array of results
    if (!_itemsInCurrentSearch) {
        self.itemsInCurrentSearch = [NSMutableArray array];
    }
    
    //Perform initial 3D actions
    [self initialLoad];
}

//Utility method for developing only
-(void)eraseSearchesInCurrent{
    NSArray *searches = [_currentColorScheme.searches allObjects];
     for (Search *search in searches) {
     [self.managedObjectContext deleteObject:search];
     }
     [self.managedObjectContext save:nil];
     return;
}


//iOS method
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    //When the view has appear I load the worm
}




#pragma mark database methods

-(NSManagedObjectContext *)managedObjectContext{
    IWAppDelegate *appDelegate = (IWAppDelegate *)[[UIApplication sharedApplication]delegate];
    return appDelegate.managedObjectContext;
}

-(void)createDataBase{
    int cellInt = 1;
    for (int x = 1; x<MAXTIMEPOINT + 1; x++) {NSLog(@"in loop %i", x);//Up to 241, there are 240 time points
        TimeWorm *timeObject = [NSEntityDescription insertNewObjectForEntityForName:@"TimeWorm" inManagedObjectContext:self.managedObjectContext];
        timeObject.timePoint = [NSNumber numberWithInt:x];
        timeObject.timePoint_str = [NSString stringWithFormat:@"%i", x];
        NSString *time;
        if (x<10) {
            time = [NSString stringWithFormat:@"00%i",x];
        }else if (x <100){
            time = [NSString stringWithFormat:@"0%i", x];
        }else{
            time = [NSString stringWithFormat:@"%i",x];
        }
        NSString *fileName = [NSString stringWithFormat:@"t%@-nuclei", time];
        NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
        NSURL *url= [NSURL fileURLWithPath:path];
        NSError *error;
        NSString *allData = [[NSString alloc]initWithContentsOfURL:url encoding:NSStringEncodingConversionAllowLossy error:&error];
        
        //Parse the data
        if (allData != (id)[NSNull null]) {
            NSArray *array = [allData componentsSeparatedByString:@"\n"];
            for (NSString *cellTime in array) {
                NSArray *comps = [cellTime componentsSeparatedByString:@","];
                if (comps.count < 8) {
                    //x++;
                    continue;
                }
                NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
                request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"uniqueID" ascending:YES]];
                request.predicate = [NSPredicate predicateWithFormat:@"name == %@", [comps objectAtIndex:9]];
                NSArray *results = [[self managedObjectContext]executeFetchRequest:request error:nil];
                Cell *cell;
                if (results.count == 0) {
                    cell = [NSEntityDescription insertNewObjectForEntityForName:@"Cell" inManagedObjectContext:self.managedObjectContext];
                    cell.name = [comps objectAtIndex:9];
                    cell.uniqueID = [NSNumber numberWithInt:cellInt];
                    cellInt++;
                    cell.predecedor = [NSNumber numberWithInt:[[comps objectAtIndex:2]intValue]];
                    cell.sucessorOne = [NSNumber numberWithInt:[[comps objectAtIndex:3]intValue]];
                    cell.sucessorTwo = [NSNumber numberWithInt:[[comps objectAtIndex:4]intValue]];
                    cell.idTag = [NSNumber numberWithInt:[[comps objectAtIndex:0]intValue]];//In file only
                }else{
                    cell = [results lastObject];
                }
                CellTime *cellTime = [NSEntityDescription insertNewObjectForEntityForName:@"CellTime" inManagedObjectContext:self.managedObjectContext];
                cellTime.cell = cell;
                cellTime.x = [NSNumber numberWithFloat:[[comps objectAtIndex:5]floatValue]];
                cellTime.y = [NSNumber numberWithFloat:[[comps objectAtIndex:6]floatValue]];
                cellTime.z = [NSNumber numberWithFloat:[[comps objectAtIndex:7]floatValue]];
                cellTime.size = [NSNumber numberWithFloat:[[comps objectAtIndex:8]floatValue]];
                cellTime.time = timeObject;
            }
        }
        [allData release];
        [self.managedObjectContext save:nil];
    }
    //[self.managedObjectContext save:nil];
}

-(void)addPartsListToDB{
    NSString *fileName = @"PartsList";
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSURL *url= [NSURL fileURLWithPath:path];
    NSError *error;
    NSString *allData = [[NSString alloc]initWithContentsOfURL:url encoding:NSStringEncodingConversionAllowLossy error:&error];
    NSArray *registries = [allData componentsSeparatedByString:@"+"];
    NSMutableArray *processedRegistries = [NSMutableArray arrayWithCapacity:registries.count];
    for (NSString *string in registries) {
        NSArray *compsTC = [string componentsSeparatedByString:@"\t"];
        NSMutableArray *mut = [NSMutableArray arrayWithCapacity:3];
        for (NSString *sub in compsTC) {
            [mut addObject:sub];
        }
        //NSLog(@"Comps extracted %i", mut.count);
        if (mut.count == 3) {
            //NSLog(@"%@\n%@\n%@", [mut objectAtIndex:0], [mut objectAtIndex:1], [mut objectAtIndex:2]);
            [processedRegistries addObject:mut];
        }//else{
            //NSLog(@"%@", [mut objectAtIndex:0]);
            //NSLog(@"Did not retrieve 3");
        //}
    }
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"uniqueID" ascending:YES]];
    //request.predicate = [NSPredicate predicateWithFormat:@"name == %@", noSpace];
    NSArray *results = [[self managedObjectContext]executeFetchRequest:request error:nil];
    NSLog(@"There are %i cells", results.count);
    
    for (NSArray *array in processedRegistries) {
        NSString *systName = [array objectAtIndex:1];
        NSString *noDot = [systName stringByReplacingOccurrencesOfString:@"." withString:@""];
        NSString *andSpace = [@" " stringByAppendingFormat:@"%@", noDot];
        
        for (Cell *cell in results) {
            if ([andSpace isEqualToString:cell.name]) {
                cell.descriptionOfCell = [array objectAtIndex:2];
                cell.nameProper = [array objectAtIndex:0];
                [self.managedObjectContext save:nil];
                NSLog(@"Cell found");
            }
        }
    }
    //NSLog(@"%@", registries);
}

#pragma mark rotation and zoom
//This method is called continuosly, and updates de 3dview
- (void) drawView
{
    //Applies any rotation if there has been tap gesture translation, which modifies de iVar position. See more later
    //_group.rotateX += position.y;
    //_group.rotateY += position.x;
    [_group rotateRelativeToX:position.y toY:position.x toZ:0.0];

    //Applies zoom. Distance is modified by pinch gesture. See more later
    if (_group.z >= -1 && _group.z < 0.801) {
        _group.z += distance * 2;
    }
    if (_group.z < -1) {
        _group.z = -1;
    }else if(_group.z > 0.8){
        _group.z = 0.8;
    }else{
    
    }
    
    //Variables reset to 0. Since the changes in rotation and Z axis (zoom) are cumulative/incremental
	position.x = 0.0;
	position.y = 0.0;
	distance = 0.0;
    
	[_camera drawCamera];
}

//Utility method to calculate the distance trajected by the finger during the gesture. Used below, in the implementation of the gestures
- (float) distanceFromPoint:(CGPoint)pointA toPoint:(CGPoint)pointB{
	float xD = fabs(pointA.x - pointB.x);
	float yD = fabs(pointA.y - pointB.y);
	
	return sqrt(xD*xD + yD*yD);
}

#pragma mark themes


-(void)createPresetTheme{
    ColorScheme *theme = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
    self.currentColorScheme.isCurrent = [NSNumber numberWithBool:NO];
    theme.isCurrent = [NSNumber numberWithBool:YES];
    theme.name = @"New with default theme";
    self.currentColorScheme = theme;
    
    [self searchVC:nil createSearch:@"ABa" withColor:[UIColor colorWithRed:0.07 green:0.32 blue:0.14 alpha:1] andOptionsArray:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithBool:NO], [NSNumber numberWithBool:YES], [NSNumber numberWithBool:NO], nil]];
    [self searchVC:nil createSearch:@"ABp" withColor:[UIColor colorWithRed:0.2 green:0.21 blue:0.17 alpha:1] andOptionsArray:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithBool:NO], [NSNumber numberWithBool:YES], [NSNumber numberWithBool:NO], nil]];
    [self searchVC:nil createSearch:@"MS" withColor:[UIColor colorWithRed:0.27 green:0.55 blue:0.22 alpha:1] andOptionsArray:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithBool:NO], [NSNumber numberWithBool:YES], [NSNumber numberWithBool:NO], nil]];
    [self searchVC:nil createSearch:@"E" withColor:[UIColor colorWithRed:0.12 green:0.21 blue:0.1 alpha:1] andOptionsArray:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithBool:NO], [NSNumber numberWithBool:YES], [NSNumber numberWithBool:NO], nil]];
    [self searchVC:nil createSearch:@"C" withColor:[UIColor colorWithRed:0.12 green:0.59 blue:0.07 alpha:1] andOptionsArray:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithBool:NO], [NSNumber numberWithBool:YES], [NSNumber numberWithBool:NO], nil]];
    [self searchVC:nil createSearch:@"D" withColor:[UIColor colorWithRed:0.2 green:0.57 blue:0.15 alpha:1] andOptionsArray:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithBool:NO], [NSNumber numberWithBool:YES], [NSNumber numberWithBool:NO], nil]];
    
}

#pragma mark loading methods

-(void)initialLoad{
    UISlider *slider = [[UISlider alloc]initWithFrame:CGRectMake(30, self.view.frame.size.height - 30, self.view.frame.size.width - 60, 20)];
    slider.minimumValue = 1;
    slider.maximumValue = MAXTIMEPOINT;
    for (id view in slider.subviews) {
        NSLog(@"Class %@", NSStringFromClass([view class]));
        if ([view isMemberOfClass:[UIImageView class]]) {
            UIImageView *imageV = (UIImageView *)view;
            imageV.image = [UIImage imageNamed:@"iWorm19.png"];
            for (id other in imageV.subviews) {
                NSLog(@"B Class %@", NSStringFromClass([other class]));
                [other setBackgroundColor:[UIColor redColor]];
            }
        }else{
            UIView *aView = (UIView *)view;
            for (id other in aView.subviews) {
                NSLog(@"C Class %@", NSStringFromClass([other class]));
                [other setBackgroundColor:[UIColor redColor]];
            }
        }
        
    }
    [slider addTarget:self action:@selector(reloadWorm:) forControlEvents:(UIControlEventValueChanged | UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    UIImage *stetchLeftTrack = [[UIImage imageNamed:@"redslider.png"]
                   stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    [slider setMinimumTrackImage:stetchLeftTrack forState: UIControlStateNormal];
    slider.value = MAXTIMEPOINT;
    [self.view addSubview:slider];
    count = (int)slider.value;
    
    UILabel *fadeLabel = [[UILabel alloc]initWithFrame:CGRectMake(-90, 120, 200, 15)];
    fadeLabel.text = @"Fade";
    fadeLabel.backgroundColor = [UIColor clearColor];
    fadeLabel.textColor = [UIColor whiteColor];
    UISlider *fadeSlider = [[UISlider alloc]initWithFrame:CGRectMake(-70, 120, 200, 10)];
    [fadeSlider addTarget:self action:@selector(fade:) forControlEvents:(UIControlEventValueChanged | UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    [fadeSlider setMinimumTrackImage:stetchLeftTrack forState: UIControlStateNormal];
    fadeSlider.value = 0.8;
    dimOthers = fadeSlider.value;
    CGAffineTransform trans = CGAffineTransformMakeRotation(-M_PI * 0.5);
    fadeSlider.transform = trans;
    fadeLabel.transform = trans;
    [self.view addSubview:fadeSlider];
    [self.view addSubview:fadeLabel];
    
    labelDay = [[UILabel alloc]initWithFrame:CGRectMake(3.0, self.view.bounds.size.height - 10, LABELWIDTH, LABELHEIGHT)];
    labelDay.text = [NSString stringWithFormat:@"timepoint %i", (int)slider.value];
    labelDay.backgroundColor = [UIColor clearColor];
    labelDay.textColor = [UIColor whiteColor];
    
    labelCell = [[UILabel alloc]initWithFrame:CGRectMake(self.view.bounds.size.width - LABELWIDTH, self.view.bounds.size.height - 10, LABELWIDTH, LABELHEIGHT)];
    labelCell.backgroundColor = [UIColor clearColor];
    labelCell.textColor = [UIColor whiteColor];
    [self.view addSubview:labelDay];
    [self.view addSubview:labelCell];
    
    //------------Load presets-------------------//
    //[self createBlankTheme];
    //[self createPresetTheme];
    
    //-------Methods called to create DB---------//
    //[self createDataBase];
    //[self addPartsListToDB];
    
    //-------Alternative methods to init---------//
    [self compileInitialMesh];
    //[self loadWorm:count];
    //[self loadAllCellsFromSQLite];
    //[self loadGenericMeshes];
}

-(void)compileInitialMesh{
    NSDictionary *settings = [self grabTheSettings];
    
    //Initialize group. It is an special array that holds all the 3D objects
    if (_group) {
        [_group removeAll];
    }
    if (!_group) {
        _group = [[NGLGroup3D alloc]init];
    }
    
	_mesh = [[RCFMesh alloc] initWithFile:@"sphere.obj" settings:settings delegate:self];
    _mesh.tag = 1000;
    [_group addObject:_mesh];
    //Set material and basic properties for the mesh
    NGLMaterial *material = [[[NGLMaterial alloc]init]autorelease];
    material.diffuseColor = nglColorMake(0.4, 0.8, 0.2, 0);//alpha 0 to make invisible. Then copy instance
    _mesh.material = material;
    _mesh.scaleX = 0.2;
    _mesh.scaleY = 0.2;
    _mesh.scaleZ = 0.2;
    _mesh.x = 0;
    _mesh.y = 0;
    _mesh.z = 0;
    //Add mesh to the group
    
    //Init the camera
    _camera = [[NGLCamera alloc] initWithMeshes:_mesh, nil];
}

//To configure each cell's position and size
-(void)configureCellSphere:(NGLMesh *)sphere withComponentsFromDict:(NSArray *)components andTag:(int)x render:(BOOL)renderBool{
    NGLMaterial *materialB = [NGLMaterial material];
    //materialB.ambientColor = nglColorMake(0.9-(x*0.001), 0.1+(x*0.001), 0.5, 1);
    materialB.diffuseColor= nglColorMake(0.9-(x*0.01), 0.1+(x*0.01), 0.9, 0.5);
    materialB.alpha = 1;
    materialB.shininess = 500;
    materialB.name = [components objectAtIndex:9];
    
    sphere.material = materialB;
    sphere.touchable = YES;
    if (renderBool) {
        sphere.tag = x;
    }else{
        sphere.tag = 0;
    }
    
    sphere.x = [[components objectAtIndex:5]floatValue]/1000-0.4;
    sphere.y = [[components objectAtIndex:6]floatValue]/1000 - 0.3;
    sphere.z = [[components objectAtIndex:7]floatValue]/1000*0.504/0.087-0.24;
    sphere.scaleX = 1/[[components objectAtIndex:8]floatValue]*2;
    sphere.scaleY = 1/[[components objectAtIndex:8]floatValue]*2;
    sphere.scaleZ = 1/[[components objectAtIndex:8]floatValue]*2;
    
    [_camera addMesh:sphere];
    [_group addObject:sphere];
}



-(NSDictionary *)grabTheSettings{
    NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
							  //kNGLMeshOriginalYes, kNGLMeshKeyOriginal,
							  kNGLMeshCentralizeYes, kNGLMeshKeyCentralize,
							  @"0.3", kNGLMeshKeyNormalize,
							  nil];
    return settings;
}

//This method reads the csv files containing coordinates and size of nuclei, and parses the string to vault values to 3D objects created in a for loop.
-(void)loadWorm:(int)timePoint{
    NSString *time;
    if (count<10) {
        time = [NSString stringWithFormat:@"00%i",timePoint];
    }else if (count <100){
        time = [NSString stringWithFormat:@"0%i", timePoint];
    }else{
        time = [NSString stringWithFormat:@"%i",timePoint];
    }
    
    // Setting the loading process parameters. To take advantage of the NGL Binary feature,
	// remove the line "kNGLMeshOriginalYes, kNGLMeshKeyOriginal,". Your mesh will be loaded 950% faster.
	NSDictionary *settings = [self grabTheSettings];
    
    //Initialize group. It is an special array that holds all the 3D objects
    if (_group) {
        [_group removeAll];
    }
    if (!_group) {
        _group = [[NGLGroup3D alloc]init];
    }
    
	
    //This is a dumb mesh that servers only to keep coordinates to 0
    //untitled.obj is a mesh file generated elsewhere, with the 3D data for the object
    if(!_mesh){
        _mesh = [[[NGLMesh alloc] initWithFile:@"untitled.obj" settings:settings delegate:nil]autorelease];
    }
    //Set material and basic properties for the mesh
    NGLMaterial *material = [[[NGLMaterial alloc]init]autorelease];
    //material.diffuseColor = nglColorMake(0.4, 0.8, 0.2, 1);
    _mesh.material = material;
    _mesh.scaleX = 0.2;
    _mesh.scaleY = 0.2;
    _mesh.scaleZ = 0.2;
    _mesh.x = 0;
    _mesh.y = 0;
    _mesh.z = 0;
    //Add mesh to the group
    [_group addObject:_mesh];
    
    
    //Init the camera
    _camera = [[NGLCamera alloc] initWithMeshes:nil];
    NSLog(@"Begin parsing file %@", [[NSDate date]description]);
    //Load the file. Here I will implement reading from different time points
    NSString *fileName = [NSString stringWithFormat:@"t%@-nuclei", time];
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
	NSURL *url= [NSURL fileURLWithPath:path];
    NSError *error;
	NSString *allData = [[NSString alloc]initWithContentsOfURL:url encoding:NSStringEncodingConversionAllowLossy error:&error];
    
    nglGlobalMultithreading(NGLMultithreadingParser);
    //Parse the data
    if (allData != (id)[NSNull null]) {
        if (dataComponentsArray) {
            [dataComponentsArray release];
        }
        dataComponentsArray = [[allData componentsSeparatedByString:@"\n"] mutableCopy];// CSV ends with ACSI 13 CR (if stored on a Mac Excel 2008) is \n in others
        //NSLog(@"all data %i", array.count);
        //Declare a mesh pointer that I will reuse
        NGLMesh *sphere;
        float x = 0;
        //Generate a mesh per cell
        
        NSString *firstCell = [dataComponentsArray objectAtIndex:x];
        NSArray *components = [firstCell componentsSeparatedByString:@","];
        if (components.count > 8) {
            sphere = [[NGLMesh alloc] initWithFile:@"untitled.obj" settings:settings delegate:self];
            [dataComponentsArray removeObjectAtIndex:x];
            x++;
        }else{
            return;
        }
        [sphere compileCoreMesh];
        [self configureCellSphere:sphere withComponentsFromDict:components andTag:x render:NO];
        [sphere release];
    }
    [self setUpGestureAndCamera];
}

-(void)loadAllCellsFromSQLite{
    
	NSDictionary *settings = [self grabTheSettings];
    
    //Initialize group. It is an special array that holds all the 3D objects
    if (_group) {
        [_group removeAll];
    }
    if (!_group) {
        _group = [[NGLGroup3D alloc]init];
    }
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"uniqueID" ascending:YES]];
    if (allMOObjects) {
        [allMOObjects release];
    }
    allMOObjects = [[[self managedObjectContext]executeFetchRequest:request error:nil]retain];
    NSLog(@"Found %i cell objects", allMOObjects.count);
    
	
    //This is a dumb mesh that servers only to keep coordinates to 0
    //untitled.obj is a mesh file generated elsewhere, with the 3D data for the object
	_mesh = [[NGLMesh alloc] initWithFile:@"untitled.obj" settings:settings delegate:self];
    //[_group addObject:_mesh];
    //Set material and basic properties for the mesh
    NGLMaterial *material = [[[NGLMaterial alloc]init]autorelease];
    material.diffuseColor = nglColorMake(0.4, 0.8, 0.2, 0);//alpha 0 to make invisible. Then copy instance
    _mesh.material = material;
    _mesh.scaleX = 0.2;
    _mesh.scaleY = 0.2;
    _mesh.scaleZ = 0.2;
    _mesh.x = 0;
    _mesh.y = 0;
    _mesh.z = 0;
    //Add mesh to the group
    
    //Init the camera
    _camera = [[NGLCamera alloc] initWithMeshes:_mesh, nil];
    
    nglGlobalMultithreading(NGLMultithreadingParser);
    int x = 0;
    for (Cell *cell in allMOObjects) {
        //Important to send autorrelease. Otherwise it crashes
        RCFMesh *sphere = [[_mesh copyInstance]autorelease];//[[NGLMesh alloc] initWithFile:@"untitled.obj" settings:settings delegate:nil];
        cell.mesh = sphere;
        //[allMOObjects addObject:sphere];
        //[sphere compileCoreMesh];
        [self configureCellSphere:sphere withManagedObjectCellTime:[cell.cellTimes anyObject] andTag:x render:YES andMaterial:nil];
        x++;
    }
    NSLog(@"here");
    
    [self setUpGestureAndCamera];
    
}

-(void)loadWormFromSQLite:(int)timePoint{
    [self setArrayOfTimeCellsAtTimePoint:timePoint];
    
    for (NGLMesh *mesh in _group) {
        NGLMaterial *mat = mesh.material;
        mat.alpha = timePoint/239;
    }
    
    for (CellTime *cellTime in dataOfMObjectsInTime) {
        NGLMaterial *material = cellTime.cell.mesh.material;
        material.alpha =  1;
        cellTime.cell.mesh.x = cellTime.x.floatValue /1000-0.4;
        cellTime.cell.mesh.y = cellTime.y.floatValue /1000-0.3;
        cellTime.cell.mesh.z = cellTime.z.floatValue /1000*0.504/0.087-0.24;
    }
    nglGlobalFlush();
}

-(NGLMaterialMulti *)createMaterialMultiWithColors:(NSArray *)arrayOfColors{
    NGLSurface *surfaceA = [[[NGLSurface alloc]initWithStart:0 length:108 identifier:0]autorelease];
    NGLMaterial *matA = [NGLMaterial material];
    matA.diffuseColor= nglColorMake(0.4, 0.8, 0.4, 1);
    NGLSurface *surfaceB = [[[NGLSurface alloc]initWithStart:108 length:72 identifier:0]autorelease];
    NGLMaterial *matB = [NGLMaterial material];
    matB.diffuseColor= nglColorMake(0.8, 0.2, 0.2, 1);
    NGLSurface *surfaceC = [[[NGLSurface alloc]initWithStart:180 length:72 identifier:0]autorelease];
    NGLMaterial *matC = [NGLMaterial material];
    matC.diffuseColor= nglColorMake(0.1, 0.8, 0.8, 1);
    NGLSurface *surfaceD = [[[NGLSurface alloc]initWithStart:252 length:108 identifier:0]autorelease];
    NGLMaterial *matD = [NGLMaterial material];
    matD.diffuseColor= nglColorMake(0.1, 0.1, 0.8, 1);
    NGLSurfaceMulti *multiSurface = [[NGLSurfaceMulti alloc] initWithSurfaces:surfaceA, surfaceB, surfaceC, surfaceD, nil];
    NGLMaterialMulti *multiMaterial = [[NGLMaterialMulti alloc] initWithMaterials:matA, matB, matC, matD, nil];
    
    return multiMaterial;
}

//To configure each cell's position and size
-(void)configureCellSphere:(RCFMesh *)sphere withManagedObjectCellTime:(CellTime *)aCellTime andTag:(int)x render:(BOOL)renderBool andMaterial:(NGLMaterial *)material{
    
    NGLMaterial *materialB = [sphere material];//[NGLMaterial material];
    //materialB.ambientColor = nglColorMake(0.9-(x*0.001), 0.1+(x*0.001), 0.5, 1);
    
    materialB.shininess = 500;
    materialB.name = aCellTime.cell.name;
    if (!aCellTime) {
        materialB.diffuseColor= nglColorMake(0.9, 0, 0, 1);
        //materialB.ambientColor= nglColorMake(0.9, 0, 0, 0.5);
        materialB.alpha = 0;
        sphere.touchable = NO;
    }else{
        materialB.diffuseColor= nglColorMake(0.4, 0.8, 0.4, 1);
        //materialB.ambientColor= nglColorMake(0.4, 0.8, 0.4, 0.5);
        materialB.alpha = 1;
        sphere.touchable = YES;
    }
    //sphere.material = materialB;
    
    /*NGLSurface *surfaceA = [[[NGLSurface alloc]initWithStart:0 length:108 identifier:0]autorelease];
    NGLMaterial *matA = [NGLMaterial material];
    matA.diffuseColor= nglColorMake(0.4, 0.8, 0.4, 1);
    NGLSurface *surfaceB = [[[NGLSurface alloc]initWithStart:108 length:72 identifier:0]autorelease];
    NGLMaterial *matB = [NGLMaterial material];
    matB.diffuseColor= nglColorMake(0.8, 0.2, 0.2, 1);
    NGLSurface *surfaceC = [[[NGLSurface alloc]initWithStart:180 length:72 identifier:0]autorelease];
    NGLMaterial *matC = [NGLMaterial material];
    matC.diffuseColor= nglColorMake(0.1, 0.8, 0.8, 1);
    NGLSurface *surfaceD = [[[NGLSurface alloc]initWithStart:252 length:108 identifier:0]autorelease];
    NGLMaterial *matD = [NGLMaterial material];
    matD.diffuseColor= nglColorMake(0.1, 0.1, 0.8, 1);
    NGLSurfaceMulti *multiSurface = [[NGLSurfaceMulti alloc] initWithSurfaces:surfaceA, surfaceB, surfaceC, surfaceD, nil];
    NGLMaterialMulti *multiMaterial = [[NGLMaterialMulti alloc] initWithMaterials:matA, matB, matC, matD, nil];
    sphere.material = multiMaterial;
    sphere.surface = multiSurface;*/

    
    sphere.x = aCellTime.x.floatValue/1000-0.4;
    sphere.y = aCellTime.y.floatValue/1000 - 0.3;
    sphere.z = aCellTime.z.floatValue/1000*0.504/0.087-0.24;
    sphere.scaleX = 1/aCellTime.size.floatValue*2;
    sphere.scaleY = 1/aCellTime.size.floatValue*2;
    sphere.scaleZ = 1/aCellTime.size.floatValue*2;
    
    
    if (renderBool) {
        sphere.tag = x;
    }else{
        sphere.tag = 0;
    }
    
    sphere.cellTime = aCellTime;
    //[sphere compileCoreMesh];
    [_camera addMesh:sphere];
    [_group addObject:sphere];
}

-(void)loadGenericMeshes{
    
    [self setArrayOfTimeCellsWithMax];
    
    //NSDictionary *settings = [self grabTheSettings];
        
    nglGlobalMultithreading(NGLMultithreadingParser);
    
    if (dataOfMObjectsInTime.count >0) {
        for (int x = 0; x<581; x++) {
            RCFMesh *sphere = [[_mesh copy]autorelease];//[[[RCFMesh alloc] initWithFile:@"sphere.obj" settings:settings delegate:nil]autorelease];
            if (x<dataOfMObjectsInTime.count) {
                CellTime *cellT = [dataOfMObjectsInTime objectAtIndex:x];
                [self configureCellSphere:sphere withManagedObjectCellTime:cellT andTag:1 render:YES andMaterial:nil];
            }else{
                [self configureCellSphere:sphere withManagedObjectCellTime:nil andTag:0 render:YES andMaterial:nil];
                //break;
            }
        }
    }
    
    if (!self.currentColorScheme) {
        [self createPresetTheme];
    }
    
    [self setUpGestureAndCamera];
}

-(void)reuseMeshesWithTimePoint:(int)timePoint{
    [self setArrayOfTimeCellsAtTimePoint:timePoint];
    int x = 0;
    for (RCFMesh *mesh in _group) {
        if (mesh == _mesh) {
            continue;
        }
        if (x<dataOfMObjectsInTime.count) {
            mesh.touchable = YES;
            CellTime *cellTime = [dataOfMObjectsInTime objectAtIndex:x];
            [self resetMeshValues:mesh withCellTime:cellTime];
        }else{
            NGLMaterial *mat = (NGLMaterial *)mesh.material;
            mat.alpha = 0;
            mesh.material = mat;;
            mesh.cellTime = nil;
            mesh.touchable = NO;
        }
        x++;
    }

    if (_searchText) {
        [self searchVC:nil didSearchForCell:_searchText withFadeOthersValue:dimOthers andOptionsArray:_searchArray];
    }else{
        [self executePainting];
    }
}


#pragma mark slider

-(void)reloadWorm:(UISlider *)sender{
    if ((int)sender.value != count) {
        //[_group removeAll];
        //sender.value = (int)sender.value;
        count = (int)sender.value;
        //[self loadWorm:count];//Using files
        //[self loadWormFromSQLite:count];//Using SQLite database
        [self reuseMeshesWithTimePoint:count];//With reuse method
        labelDay.text = [NSString stringWithFormat:@"timepoint %i", count];
    }
}

-(void)fade:(UISlider *)sender{
    dimOthers = sender.value;
    if (_searchText) {
        [self searchVC:nil didSearchForCell:_searchText withFadeOthersValue:dimOthers andOptionsArray:_searchArray];
    }else{
        [self executePainting];
    }
}

#pragma mark utilities

-(void)resetMeshValues:(RCFMesh *)mesh withCellTime:(CellTime *)cellTime{
    NGLMaterial *material = mesh.material;
    material.name = cellTime.cell.name;
    //material.alpha =  0.5;
    mesh.x = cellTime.x.floatValue /1000-0.4;
    mesh.y = cellTime.y.floatValue /1000-0.3;
    mesh.z = cellTime.z.floatValue /1000*0.504/0.087-0.24;
    mesh.material = material;
    mesh.cellTime = cellTime;
}

-(void)setArrayOfTimeCellsWithMax{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CellTime"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"time.timePoint == %i", MAXTIMEPOINT];
    if(dataOfMObjectsInTime){
        [dataOfMObjectsInTime release];
    }
    dataOfMObjectsInTime = [[[self managedObjectContext]executeFetchRequest:request error:nil]retain];
    NSLog(@"Found %i cellTime objects", dataOfMObjectsInTime.count);
    labelCell.text = [NSString stringWithFormat:@"%i nuclei", dataOfMObjectsInTime.count];
}

-(void)setArrayOfTimeCellsAtTimePoint:(int)timePoint{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CellTime"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"time.timePoint == %i", timePoint];
    if(dataOfMObjectsInTime){
        [dataOfMObjectsInTime release];
    }
    dataOfMObjectsInTime = [[[self managedObjectContext]executeFetchRequest:request error:nil]retain];
    ////NSLog(@"Found %i cellTime objects", dataOfMObjectsInTime.count);
    labelCell.text = [NSString stringWithFormat:@"%i nuclei", dataOfMObjectsInTime.count];
}

-(void)setUpGestureAndCamera{
    //Add a gesture recognizer that will be used for recognizing cells when tapping. This is different to the gestures for rotating and zooming. Those come in built with the UIView, all of them are implemented later in this class
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapRecognizer];
    [tapRecognizer release];
    //[_group removeObject:_mesh];
    _group.rotationSpace = NGLRotationSpaceWorld;
	//[_camera autoAdjustAspectRatio:YES animated:YES];
    [_camera translateToX:0 toY:0 toZ:1];
	_camera.lookAtTarget = _group;
	// Starts the debug monitor.
	//[[NGLDebug debugMonitor] startWithView:(NGLView *)self.view];
}

-(NSString *)getCell:(int)inTag{
    NSString *name;
    NSString *time;
    if (count<10) {
        time = [NSString stringWithFormat:@"00%i",count];
    }else if (count <100){
        time = [NSString stringWithFormat:@"0%i", count];
    }else{
        time = [NSString stringWithFormat:@"%i",count];
    }
    
    NSString *fileName = [NSString stringWithFormat:@"t%@-nuclei", time];
    NSLog(@"filename %@", fileName);
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
	NSURL *url= [NSURL fileURLWithPath:path];
    NSError *error;
	NSString *allData = [[NSString alloc]initWithContentsOfURL:url encoding:NSStringEncodingConversionAllowLossy error:&error];
    
    //Parse the data
    if (allData != (id)[NSNull null]) {
        NSArray *array = [allData componentsSeparatedByString:@"\n"];// CSV ends with ACSI 13 CR (if stored on a Mac Excel 2008) is \n in others
        NSString *line = [array objectAtIndex:inTag];
        NSArray *components = [line componentsSeparatedByString:@","];
        name = [components objectAtIndex:9];
    }
    return name;
}

//This method determines whether a mesh has been touched. Transforms the 2D coordinates in the screen to the 3D (Reverse model/view transformation)
-(BOOL)analyzeMesh:(NGLMesh *)mesh andTapLocation:(CGPoint)tapLocation{
    
    //Find coordinates of a mesh in 3D space, using its initial values and doing the trasformations with relevant values of the model view matrix
    float newXPos = mesh.x * _group.matrix[0][0] + mesh.y * _group.matrix[0][4] + mesh.z * _group.matrix[0][8];
    float newYPos = mesh.x * _group.matrix[0][1] + mesh.y * _group.matrix[0][5] + mesh.z * _group.matrix[0][9];
    //Y is inverted since Y coordinates in iOS screen are counterintuitive
    newYPos = -newYPos;
    //Calculate the z distance (important for the camera transformation(
    float newZPos = mesh.matrixOrtho[3][0];
    //Correction due to distance to camera (things that are far look closer to the center that objects that are close to the camera, although their Y and X values are the same. 
    float extrusion = newZPos/_camera.z;
    //Calculate values in screen. All this is like a simplified eye-coordinates tranformation in OpenGL
    newXPos = newXPos/extrusion;
    newYPos = newYPos/extrusion;
    //NSLog(@"the mesh position is X %f and Y %f with taps X %f and Y %f", newXPos*1000, newYPos*1000, tapLocation.x -382, tapLocation.y - 502);
    
    //Compare values of mesh and values of tap
    float valueX = newXPos*1000 - (tapLocation.x - self.view.bounds.size.width/2);//Funciona con z 0 y sin rotacion
    float valueY = newYPos*1000 - (tapLocation.y - self.view.bounds.size.height/2);//Funciona con z 0 y sin rotacion
    if (valueX<0)valueX = -valueX;
    if (valueY<0)valueY = -valueY;
    
    //Analize proximity of touch and mesh. If there is touch, do something (temporarily show a message, in the future, trigger window/label with info)
    if (valueX <= 30) {//[[[[UIAlertView alloc]initWithTitle:@"Touched" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]autorelease]show];
        if (valueY <=30) {
            //NSLog(@"Hit mesh at X and Y tag %i and value X %f and value Y %f", mesh.tag, valueX, valueY);
            //NSString *cellNumber = [NSString stringWithFormat:@"touched the cell number %i, with name %@", mesh.tag, [self getCell:mesh.tag]];
            //[[[[UIAlertView alloc]initWithTitle:@"Touched" message:cellNumber delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]autorelease]show];
            return YES;
        }
    }
    return NO;
}


//Utiliy method for developer's use only
-(void)analyzeMeshMatrix:(NGLMesh *)mesh{
    
    float newXPos = mesh.x * _group.matrix[0][0] + mesh.y * _group.matrix[0][4] + mesh.z * _group.matrix[0][8];
    float newYPos = mesh.x * _group.matrix[0][1] + mesh.y * _group.matrix[0][5] + mesh.z * _group.matrix[0][9];
    newYPos = -newYPos;
    float newZPos = mesh.matrixOrtho[3][0];
    float extrusion = newZPos/_camera.z;
    newXPos = newXPos/extrusion;
    newYPos = newYPos/extrusion;
        
    if (transf) {
        [transf release];
    }
    transf = [[UIView alloc]initWithFrame:CGRectMake(newXPos*1000 + 382, newYPos*1000 + 502, 10, 10)];
    
    transf.backgroundColor = [UIColor whiteColor];
    self.view.autoresizesSubviews = NO;
    transf.autoresizingMask = UIViewAutoresizingNone;
    [transpar addSubview:transf];
    
        
    for (int x = 0 ; x<4 ; x++) {
        NSLog(@"GR [%f][%f][%f][%f]", _group.matrix[0][x], _group.matrix[0][x + 4], _group.matrix[0][x + 8], _group.matrix[0][x + 12]);
    }
    for (int x = 0 ; x<4 ; x++) {
        NSLog(@"GR [%f][%f][%f][%f]", mesh.matrixOrtho[0][x], mesh.matrixOrtho[1][x], mesh.matrixOrtho[2][x], mesh.matrixOrtho[3][x]);
    }
    
    NSLog(@"\n");
    NSLog(@"Camera near %f, far %f, angle %f aspect %f", _camera.nearPlane, _camera.farPlane, _camera.angleView, _camera.aspectRatio);
    NSLog(@"\n");
    
}

//Utiliy method for developer's use only
-(void)generateGrayMeshAtLocation:(CGPoint)location{
    NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
							  //kNGLMeshOriginalYes, kNGLMeshKeyOriginal,
							  kNGLMeshCentralizeYes, kNGLMeshKeyCentralize,
							  @"0.3", kNGLMeshKeyNormalize,
							  nil];
     NGLMesh *sphere = [[NGLMesh alloc] initWithFile:@"untitled.obj" settings:settings delegate:nil];
     NGLMaterial *materialB = [NGLMaterial material];
     materialB.diffuseColor = nglColorMake(0.9, 0.9, 0.9, 0.1);
     sphere.material = materialB;
     //[sphere compileCoreMesh];
     
     sphere.touchable = YES;
     sphere.delegate = self;
     
     sphere.x = (location.x - 382)/1000;
     sphere.y = -(location.y - 502)/1000;
     sphere.z = 0;
     sphere.scaleX = 0.1;
     sphere.scaleY = 0.1;
     sphere.scaleZ = 0.1;
     
     [_camera addMesh:sphere];
     [_group addObject:sphere];
     [sphere release];
}

#pragma mark Navigation buttons


#pragma mark popover delegate

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self executePainting];
}


#pragma mark screen interaction
//Implementation of the tap gesture
- (void)handleTap: (UITapGestureRecognizer*)recognizer{

    CGPoint tapLocation = [recognizer locationInView:self.view];
    
    //Compare tap coordianates with all the meshes
    NSMutableArray *posibles = [NSMutableArray arrayWithCapacity:_group.count];
    
    for (RCFMesh *mesh in _group) {
        
        if (mesh == _mesh) {
            continue;
        }
        if ([self analyzeMesh:mesh andTapLocation:tapLocation] && mesh.tag >0) {
            if (mesh.isTouchable) {
                [posibles addObject:mesh];
            }
        }
    }
    
    RCFMesh *selectedMesh = nil;
    float zPos = -1;
    for (RCFMesh *mesh in posibles) {
        float meshZ = mesh.x * _group.matrix[0][2] + mesh.y * _group.matrix[0][3] + mesh.z * _group.matrix[0][10];
        if (meshZ>zPos) {
            zPos = meshZ;
            selectedMesh = mesh;
        }
    }
    
    if (selectedMesh) {
        UIViewController *vc= [[UIViewController alloc]init];
        vc.title = [(NGLMaterial *)selectedMesh.material name];//[self getCell:selectedMesh.tag];
        vc.contentSizeForViewInPopover = CGSizeMake(200, 0);
        UINavigationController *navCon = [[UINavigationController alloc]initWithRootViewController:vc];
        navCon.navigationBar.tintColor = [UIColor blackColor];
        vc.view.backgroundColor = [UIColor whiteColor];
        self.popover = [[[UIPopoverController alloc]initWithContentViewController:navCon]autorelease];
        [self.popover presentPopoverFromRect:CGRectMake(tapLocation.x, tapLocation.y, 10, 10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        [vc release];
    }
    //[self generateGrayMeshAtLocation:tapLocation];
}

//Implementation of rotation and zoom gestures
- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touchA, *touchB;
	CGPoint pointA, pointB;
	
	// Pan gesture.
	if ([touches count] == 1)
	{
		touchA = [[touches allObjects] objectAtIndex:0];
		pointA = [touchA locationInView:self.view];
		pointB = [touchA previousLocationInView:self.view];
		
		position.x = (pointA.x - pointB.x);
		position.y = (pointA.y - pointB.y);
        
        
	}
	// Pinch gesture.
	else if ([touches count] == 2)
	{
		touchA = [[touches allObjects] objectAtIndex:0];
		touchB = [[touches allObjects] objectAtIndex:1];
		
		// Current distance.
		pointA = [touchA locationInView:self.view];
		pointB = [touchB locationInView:self.view];
		float currDistance = [self distanceFromPoint:pointA toPoint:pointB];
		
		// Previous distance.
		pointA = [touchA previousLocationInView:self.view];
		pointB = [touchB previousLocationInView:self.view];
		float prevDistance = [self distanceFromPoint:pointA toPoint:pointB];
		
		distance = (currDistance - prevDistance) * 0.005;
	}
    
    /*for (NGLMesh *mesh in _group) {
        NGLMaterial *materialB = mesh.material;
        if (mesh.matrixOrtho[3][0] < 0.7) {
            materialB.alpha = mesh.matrixOrtho[3][0];
        }else{
            materialB.alpha = 1;
        }
        mesh.material = materialB;
    }*/
}


//Memory management methods
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return NO;
}

#pragma mark SearchCell delegate

/*-(void)searchVC:(SearchController *)searchController didSearchForCell:(NSString *)searchString withFadeOthersValue:(float)newFadeValue andOptionsArray:(NSArray *)array{
    
    int typeOfSearch = [[array objectAtIndex:0]intValue];
    BOOL searchProper = [[array objectAtIndex:1]boolValue];
    BOOL searchSyst = [[array objectAtIndex:2]boolValue];
    BOOL searchDescription = [[array objectAtIndex:3]boolValue];
    
    NSLog(@"Search type %i\n search proper %i\n search syst %i\n search description %i\n", typeOfSearch, searchProper, searchSyst, searchDescription);
    
    
    [self.itemsInCurrentSearch removeAllObjects];//Emptying the current search buffer
    
    self.searchText = searchString;//Store the current search word in the delegate (self)
    
    dimOthers = newFadeValue;
    for (RCFMesh *mesh in _group) {//Loop through all the meshes used
        if (mesh == _mesh) {
            continue;
        }
        if (mesh.isTouchable) {//Is being used at this time point
            NGLMaterial *mat = mesh.material;//Analize the material (contains the name of the cell)
            
            /*if ([mat.name hasPrefix:searchString] && mesh.isTouchable) {
                mat.diffuseColor= nglColorMake(0.9, 0.5, 0.4, 0.8);
            }else if(mesh.isTouchable){
                mat.diffuseColor= nglColorMake(0.3, 0.3, 0.3, 0.5);
                mat.alpha = dimOthers;
            }///*
            
            NSComparisonResult result = [mat.name compare:searchString options:(NSDiacriticInsensitiveSearch)
                                                    range:[mat.name rangeOfString:searchString options:(NSDiacriticInsensitiveSearch)]];
            
            if (result == NSOrderedSame){
                mat.diffuseColor= nglColorMake(0.9, 0.5, 0.4, 0.8);
                mesh.tag = 1;//Will make the mesh tappable
                [self.itemsInCurrentSearch addObject:mesh.cellTime];//Add to the current search buffer
                
            }else{
                mat.diffuseColor= nglColorMake(0.3, 0.3, 0.3, 0.5);
                mat.alpha = dimOthers;
                mesh.tag = 0;//Will turn of tapping
            }
        }
    }
}*/

-(void)searchVC:(SearchController *)searchController didSearchForCell:(NSString *)searchString withFadeOthersValue:(float)newFadeValue andOptionsArray:(NSArray *)array{
    
    int typeOfSearch = [[array objectAtIndex:0]intValue];
    BOOL searchProper = [[array objectAtIndex:1]boolValue];
    BOOL searchSyst = [[array objectAtIndex:2]boolValue];
    BOOL searchDescription = [[array objectAtIndex:3]boolValue];
    
    //NSLog(@"Search type %i\n search proper %i\n search syst %i\n search description %i\n", typeOfSearch, searchProper, searchSyst, searchDescription);
    
    
    [self.itemsInCurrentSearch removeAllObjects];//Emptying the current search buffer
    
    self.searchText = searchString;//Store the current search word in the delegate (self)
    self.searchArray = array;
    
    dimOthers = newFadeValue;
    
    BOOL cellExact = NO;
    NSArray *results = [self executeSearchWithString:searchString andOptionsArray:array];
    if (results.count > 0) {
        cellExact = YES;
    }
    
    for (RCFMesh *mesh in _group) {//Loop through all the meshes used
        if (mesh == _mesh) {
            continue;
        }
        if (mesh.isTouchable) {//Is being used at this time point
            
            NGLMaterial *mat = mesh.material;//Analize the material (contains the name of the cell)
            BOOL cellAdded = NO;
            
            if (searchProper == YES) {
                if (mesh.cellTime.cell.nameProper.length > 0) {
                    NSComparisonResult result = [mesh.cellTime.cell.nameProper compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                                                 range:[mesh.cellTime.cell.nameProper rangeOfString:searchString options:(NSDiacriticInsensitiveSearch)]];
                    if (result == NSOrderedSame) {
                        cellAdded = YES;
                    }
                    
                    if ([mesh.cellTime.cell.nameProper isEqualToString:searchString]) {
                        cellExact = YES;
                    }
                }
            }
            
            if (searchSyst == YES) {
                if (mesh.cellTime.cell.name.length > 0) {
                    NSComparisonResult result = [mesh.cellTime.cell.name compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                                           range:[mesh.cellTime.cell.name rangeOfString:searchString options:(NSDiacriticInsensitiveSearch)]];
                    if (result == NSOrderedSame) {
                        cellAdded = YES;
                    }
                    
                    NSMutableString *trunc = [NSMutableString stringWithString:mesh.cellTime.cell.name];
                    [trunc deleteCharactersInRange:NSMakeRange(0, 1)];
                    if ([trunc isEqualToString:searchString]) {
                        cellExact = YES;
                    }
                }
                
            }
            
            if (searchDescription == YES) {
                if (mesh.cellTime.cell.descriptionOfCell.length > 0) {
                    NSComparisonResult result = [mesh.cellTime.cell.descriptionOfCell compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                                                        range:[mesh.cellTime.cell.descriptionOfCell rangeOfString:searchString options:(NSDiacriticInsensitiveSearch)]];
                    if (result == NSOrderedSame && searchString.length > 1) {
                        cellAdded = YES;
                    }
                }
            }
            if (cellExact == YES) {
                if (cellAdded == YES){
                    NGLMaterial *mat = (NGLMaterial *)mesh.material;
                    mat.diffuseColor= nglColorMake(0.9, 0.9, 0.0, 1);
                    mesh.tag = 1;//Will make the mesh tappable
                    [self.itemsInCurrentSearch addObject:mesh.cellTime];//Add to the current search buffer
                    
                }else{
                    NGLMaterial *mat = (NGLMaterial *)mesh.material;
                    mat.diffuseColor= nglColorMake(0.3, 0.3, 0.3, 0.5);
                    mat.alpha = dimOthers;
                    mesh.tag = 0;//Will turn of tapping
                }
            }else{
                if (cellAdded == YES){
                    NGLMaterial *mat = (NGLMaterial *)mesh.material;
                    mat.diffuseColor= nglColorMake(0.9, 0.5, 0.4, 1);
                    mesh.tag = 1;//Will make the mesh tappable
                    [self.itemsInCurrentSearch addObject:mesh.cellTime];//Add to the current search buffer
                    
                }else{
                     NGLMaterial *mat = (NGLMaterial *)mesh.material;
                    mat.diffuseColor= nglColorMake(0.3, 0.3, 0.3, 0.5);
                    mat.alpha = dimOthers;
                    mesh.tag = 0;//Will turn of tapping
                }
            } 
        }
    }
}

-(NSArray *)lastSearch{
    return _itemsInCurrentSearch;
}

-(NSArray *)executeSearchWithString:(NSString *)searchString andOptionsArray:(NSArray *)array{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cell"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    NSArray *allCells = [self.managedObjectContext executeFetchRequest:request error:nil];
    
    NSMutableArray *results = [NSMutableArray arrayWithCapacity:allCells.count];
    
    int typeOfSearch = [[array objectAtIndex:0]intValue];
    BOOL searchProper = [[array objectAtIndex:1]boolValue];
    BOOL searchSyst = [[array objectAtIndex:2]boolValue];
    BOOL searchDescription = [[array objectAtIndex:3]boolValue];
        
    for (Cell *cell in allCells) {
        BOOL cellAdded = NO;
        
        if (searchProper == YES) {
            if (cell.nameProper.length > 0) {
                NSComparisonResult result = [cell.nameProper compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                               range:[cell.nameProper rangeOfString:searchString options:(NSDiacriticInsensitiveSearch)]];
                if (result == NSOrderedSame) {
                    cellAdded = YES;
                }
            }
        }
        
        if (searchSyst == YES) {
            if (cell.name.length > 0) {
                NSMutableString *mutString = [NSMutableString stringWithString:cell.name];
                [mutString deleteCharactersInRange:NSMakeRange(0, 1)];
                if ([mutString isEqualToString:searchString]) {
                    cellAdded = YES;
                }
            }
        }
        
        if (searchDescription == YES) {
            if (cell.descriptionOfCell.length > 0) {
                NSComparisonResult result = [cell.descriptionOfCell compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                                      range:[cell.descriptionOfCell rangeOfString:searchString options:(NSDiacriticInsensitiveSearch)]];
                if (result == NSOrderedSame && searchString.length > 1) {
                    cellAdded = YES;
                }
            }
        }
        if (cellAdded == YES){
            [results addObject:cell];
            
            if (typeOfSearch == 0) {
                
            }else{
                for (Cell * others in allCells) {
                    if (others == cell) {
                        continue;//This one is already added
                    }
                    
                    NSComparisonResult result = [others.name compare:searchString options:(NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch)
                                                               range:[cell.name rangeOfString:searchString options:(NSDiacriticInsensitiveSearch)]];
                    if (result == NSOrderedSame) {
                        [results addObject:others];
                    }
                }
                if(typeOfSearch == 2){
                    //Do triming backwards here
                    NSMutableArray *array = [NSMutableArray arrayWithObject:searchString];
                    NSMutableString *mutString = [NSMutableString stringWithString:searchString];
                    while (mutString.length>1) {
                        [mutString deleteCharactersInRange:NSMakeRange(mutString.length - 1, 1)];
                        NSString *trimmedString = [[mutString copy]autorelease];
                        NSLog(@"Trimmed %@", trimmedString);
                        [array addObject:trimmedString];
                    }
                }
            }
        }
    }
    
    NSLog(@"Returns %i", results.count);
    return results;
}

-(void)searchVC:(SearchController *)searchController createSearch:(NSString *)searchString withColor:(UIColor *)color andOptionsArray:(NSArray *)array{
    
    self.searchText = nil;
    
    if (!_currentColorScheme) {//In the first use of the app
        self.currentColorScheme = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
        self.currentColorScheme.isCurrent = [NSNumber numberWithBool:YES];
    }
    
    int typeOfSearch = [[array objectAtIndex:0]intValue];
    BOOL searchProper = [[array objectAtIndex:1]boolValue];
    BOOL searchSyst = [[array objectAtIndex:2]boolValue];
    BOOL searchDescription = [[array objectAtIndex:3]boolValue];
    
    Search *search = [NSEntityDescription insertNewObjectForEntityForName:@"Search" inManagedObjectContext:self.managedObjectContext];
    search.theme = self.currentColorScheme;
    search.searchString = searchString;
    search.type = [NSString stringWithFormat:@"%i", typeOfSearch];
    search.searchIn = [NSString stringWithFormat:@"%i", ((int)searchProper + (int)searchSyst * 10 + (int)searchDescription * 100)];
    //NSLog(@"Search in is %i", search.searchIn.intValue);
    
    CGColorRef colorRef = [color CGColor];
    
    int numComponents = CGColorGetNumberOfComponents(colorRef);
    
    if (numComponents == 4)
    {
        const CGFloat *components = CGColorGetComponents(colorRef);
        CGFloat red = components[0];
        CGFloat green = components[1];
        CGFloat blue = components[2];
        //CGFloat alpha = components[3];
        search.color = [NSString stringWithFormat:@"%f,%f,%f", red, green, blue];
    }

    NSArray *foundCells = [self executeSearchWithString:searchString andOptionsArray:array];
    for (Cell *cell in foundCells) {
        NSMutableSet *set = [NSMutableSet setWithSet:cell.searches];
        [set addObject:search];
        cell.searches = set;
    }    
    [self.managedObjectContext save:nil];
    
    [self executePainting];
}

-(void)searchVC:(SearchController *)searchController didDissapearWithSearch:(NSString *)searchString withColor:(UIColor *)color andOptionsArray:(NSArray *)array{
    if (searchString) {
        self.searchText = searchString;
    }else{
        self.searchText = nil;
    }
    [_itemsInCurrentSearch removeAllObjects];
}

-(void)executePainting{NSLog(@"Exc");
    
    for (NGLMesh *mesh in _group) {
        if (mesh == _mesh) {
            continue;
        }
        if (mesh.isTouchable) {
            NGLMaterial *mat =(NGLMaterial *)mesh.material;
            mat.alpha = dimOthers;
            mesh.tag = 0;
            mat.diffuseColor= nglColorMake(0.3, 0.3, 0.3, 0.5);
            mesh.material = mat;
        }
    }
    
    if (_currentColorScheme.searches.count > 0) {
        
        for (Search *aSearch in self.currentColorScheme.searches) {
            ////NSLog(@"Search loop");
            for (RCFMesh *mesh in _group) {
                if (mesh == _mesh) {
                    continue;
                }
                if (mesh.isTouchable) {//Is being used at this time point
                    
                    if ([aSearch.cells containsObject:mesh.cellTime.cell]) {
                        ////NSLog(@"Found cell");
                        NGLMaterial *mat =(NGLMaterial *)mesh.material;
                        NSArray *colorComps = [aSearch.color componentsSeparatedByString:@","];
                        mat.diffuseColor= nglColorMake([[colorComps objectAtIndex:0]floatValue], [[colorComps objectAtIndex:1]floatValue], [[colorComps objectAtIndex:2]floatValue], 1);
                        mat.alpha = 1;
                        mesh.tag = 1;
                        mesh.material = mat;
                    }
                }
            }
        }
    }
}

-(void)newExcecutePainting{
    NSLog(@"new");
    for (NGLMesh *mesh in _group) {
        if (mesh == _mesh) {
            continue;
        }
        if (mesh.isTouchable) {
            NGLMaterialMulti *mult = (NGLMaterialMulti *)mesh.material;
            for (NGLMaterial *mat in mult) {
                mat.alpha = 0.5;
                mat.diffuseColor= nglColorMake(0.3, 0.3, 0.3, 0.5);
            }
            mesh.tag = 0;
            
            mesh.material = mult;
            [mesh compileCoreMesh];
        }
    }
    
    for (RCFMesh *mesh in _group) {
        if (mesh.isTouchable) {
            NSMutableArray *searchesInThisTheme = [NSMutableArray array];
            for (Search *search in mesh.cellTime.cell.searches) {
                if (search.theme == self.currentColorScheme) {
                    [searchesInThisTheme addObject:search];
                }
            }
            
            
            if (searchesInThisTheme.count > 1) {NSLog(@"here");
                if (searchesInThisTheme.count == 2) {
                    //108, 144, 108
                    NSMutableArray *colors = [NSMutableArray array];
                    for (Search *search in searchesInThisTheme) {
                        [colors addObject:[search.color componentsSeparatedByString:@","]];
                        [colors addObject:[search.color componentsSeparatedByString:@","]];
                    }
                    int x = 0;
                    NGLMaterialMulti *mult = (NGLMaterialMulti *)mesh.material;
                    for (NGLMaterial *mat in mult) {
                        NSArray *colorComps = [colors objectAtIndex:x];
                        mat.diffuseColor= nglColorMake([[colorComps objectAtIndex:0]floatValue], [[colorComps objectAtIndex:1]floatValue], [[colorComps objectAtIndex:2]floatValue], 1);
                        mat.alpha = 1;
                        x++;
                    }
                    mesh.material = mult;
                    [mesh compileCoreMesh];
                }else if(searchesInThisTheme.count == 3){
                    //108, 144, 108
                    
                }else if(searchesInThisTheme.count >= 4){
                    //108, 144, 108
                    
                }
                
            }else if(searchesInThisTheme.count == 1){
                //Regular coloring here
                Search *search = [searchesInThisTheme lastObject];
                NGLMaterialMulti *mult =(NGLMaterialMulti *)mesh.material;
                NSArray *colorComps = [search.color componentsSeparatedByString:@","];
                for (NGLMaterial *mat in mult) {
                    mat.diffuseColor= nglColorMake([[colorComps objectAtIndex:0]floatValue], [[colorComps objectAtIndex:1]floatValue], [[colorComps objectAtIndex:2]floatValue], 1);
                    mat.alpha = 1;
                }
                
                mesh.tag = 1;
                mesh.material = mult;
            }else{
                //Turn black here
                //No need to do, already done above
            }
        }
    }
}

#pragma mark mesh delegates

-(void)meshCompilingDidFinish:(NGLParsing)parsing{
    NSLog(@"Compile done %i", parsing.mesh.tag);
    if (initial == NO) {
        [self loadGenericMeshes];
        initial = YES;
    }

}
-(void)meshLoadingDidFinish:(NGLParsing)parsing{
    NSLog(@"loading A done %i", parsing.mesh.tag);
    if (initial == NO) {
        [_mesh compileCoreMesh];
    }
    //nglGlobalFlush();
}

-(void)meshLoadingProgress:(NGLParsing)parsing{
    NSLog(@"loading progress %i", parsing.mesh.tag);
}
-(void)meshLoadingWillStart:(NGLParsing)parsing{
    NSLog(@"loading will start %i", parsing.mesh.tag);
}

#pragma mark Lineage Delegate

-(void)lineageVC:(LineageTracing *)lineageTVC didChooseTheme:(ColorScheme *)theme{
    NSLog(@"Number of searches in current %i", self.currentColorScheme.searches.count);
    if (self.currentColorScheme) {
        if (self.currentColorScheme.searches.count == 0) {
            NSLog(@"Will delete name %@", self.currentColorScheme.name);
            [self.managedObjectContext deleteObject:self.currentColorScheme];
        }else if(self.currentColorScheme.name.length == 0 && self.currentColorScheme.searches.count > 0){
            [[[[UIAlertView alloc]initWithTitle:@"Un-named profile" message:@"Name and save the current profile before other previous profiles" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]autorelease]show];
        }else{
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ColorScheme"];
            request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
            NSArray *allThemes = [self.managedObjectContext executeFetchRequest:request error:nil];
            
            for (ColorScheme *theme in allThemes) {
                theme.isCurrent = [NSNumber numberWithBool:NO];
            }
        }
    }
    
    theme.isCurrent = [NSNumber numberWithBool:YES];
    self.currentColorScheme = theme;
    [self.managedObjectContext save:nil];
    [self executePainting];
}

-(void)lineageVCdidChooseBlank:(LineageTracing *)lineageTVC{
    
    self.currentColorScheme.isCurrent = [NSNumber numberWithBool:NO];
    [self.managedObjectContext save:nil];
    ColorScheme *new = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
    new.isCurrent = [NSNumber numberWithBool:YES];
    new.saved = [NSNumber numberWithBool:NO];
    new.name = @"Untitled";
    lineageTVC.nameOfCurrent.text = new.name;
    self.currentColorScheme = new;
    [self.managedObjectContext save:nil];
    
    for (RCFMesh *mesh in _group) {
        if (mesh == _mesh) {
            continue;
        }
        if(mesh.isTouchable){
            NGLMaterial *mat = (NGLMaterial *)mesh.material;
            mat.diffuseColor= nglColorMake(0.3, 0.3, 0.3, 0.5);
            //mat.alpha = 1;
            mesh.tag = 0;//Will turn of tapping
            mesh.material = mat;
        }
    }
}

-(void)lineageVCdidChoosePreset:(LineageTracing *)lineageTVC{
    NSLog(@"hol");
    [self createPresetTheme];
}

-(void)lineageVC:(LineageTracing *)lineageTVC didSaveCurrentWithName:(NSString *)name{
    self.currentColorScheme.name = name;
    //self.currentColorScheme.isCurrent = [NSNumber numberWithBool:NO];
    //ColorScheme *new = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
    //new.isCurrent = [NSNumber numberWithBool:YES];
    //self.currentColorScheme = new;
    [self.managedObjectContext save:nil];
}

#pragma mark send or save screen

-(void)saveScreen{
    UIImage *image = [_ngView drawToImage];
    UIImageWriteToSavedPhotosAlbum(image, self,
                                   @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error != NULL){        
    }else{
        [[[[UIAlertView alloc]initWithTitle:@"Image Saved" message:@"Image saved to your device's camera roll" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]autorelease]show];
    }
}



-(void)sendScreen{NSLog(@"called");
    //UIGraphicsBeginImageContext(self.view.bounds.size);
    //[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    //UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //UIGraphicsEndImageContext();
    
    //UIImage *image = [_ngView drawToImage];
    UIImage *image = [[_ngView drawToTexture]image];
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
	
	[picker setSubject:@"iWorm image"];
	
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    NSString *name;
    if (self.currentColorScheme.name.length > 0) {
        name = self.currentColorScheme.name;
    }else{
        name = @"Image_From_iWorm";
    }
    
    [picker addAttachmentData:imageData mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"%@.jpg", name]];
    
	// Fill out the email body text
	
	//[picker setMessageBody:emailBody isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
	//[picker setToRecipients:[NSArray arrayWithObject:@""]];
	
	picker.navigationBar.barStyle = UIBarStyleBlack; // choose your style, unfortunately, Translucent colors behave quirky.
	
	[self presentModalViewController:picker animated:YES];
	[picker release];
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
			
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
			[alert release];
		}
			
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

@end