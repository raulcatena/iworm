//
//  NGLMeshCell.m
//  iWorm
//
//  Created by Raul Catena on 4/27/13.
//
//

#import "NGLMeshCell.h"

@implementation NGLMeshCell

-(id)initWithFile:(NSString *)named settings:(NSDictionary *)dict delegate:(id<NGLMeshDelegate>)target andCell:(Cell *)cell{
    self = [self initWithFile:named settings:dict delegate:target];
    if (self) {
        if (cell) {
            self.cell = cell;
        }
    }
    return self;
}

@end
