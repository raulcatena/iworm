//
//  PSAppDelegate.h
//  LL
//
//  Created by Raul Catena on 5/26/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URLHandler.h"

@class PSViewController;

@interface PSAppDelegate : UIResponder <UIApplicationDelegate, UITabBarDelegate>
{
	UIWindow *_window;
	PSViewController *_viewController;
    URLHandler *_urlHandler;
    
@private
    NSManagedObjectContext *managedObjectContext_;
    NSManagedObjectModel *managedObjectModel_;
    NSPersistentStoreCoordinator *persistentStoreCoordinator_;
}

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) PSViewController *viewController;

@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
