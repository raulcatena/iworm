//
//  Search.h
//  iWorm
//
//  Created by Raul Catena on 5/18/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cell, ColorScheme;

@interface Search : NSManagedObject

@property (nonatomic, strong) NSString * color;
@property (nonatomic, strong) NSString * propagationType;
@property (nonatomic, strong) NSString * searchString;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * searchIn;//0 in proper, 1 in syst, 2 in descrip, 3 in all, 4 proper and syst, 5 proper and descr, 6 syst and descript
@property (nonatomic, strong) NSSet *cells;
@property (nonatomic, strong) ColorScheme *theme;
@end

@interface Search (CoreDataGeneratedAccessors)

- (void)addCellsObject:(Cell *)value;
- (void)removeCellsObject:(Cell *)value;
- (void)addCells:(NSSet *)values;
- (void)removeCells:(NSSet *)values;

@end
