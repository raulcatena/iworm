//
//  Cell.h
//  WormGUIDES
//
//  Created by Raul Catena on 6/7/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cell, CellTime, ColorScheme, Search, PSMesh;

@interface Cell : NSManagedObject

@property (nonatomic, strong) NSString * descriptionOfCell;
@property (nonatomic, strong) NSNumber * idTag;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * nameProper;
@property (nonatomic, strong) NSNumber * predecedor;
@property (nonatomic, strong) NSNumber * predecesorUniqueID;
@property (nonatomic, strong) NSNumber * sucessorOne;
@property (nonatomic, strong) NSNumber * sucessorOneUniqueID;
@property (nonatomic, strong) NSNumber * sucessorTwo;
@property (nonatomic, strong) NSNumber * sucessorTwoUniqueID;
@property (nonatomic, strong) NSNumber * uniqueID;
@property (nonatomic, strong) NSSet *cellTimes;
@property (nonatomic, strong) NSSet *searches;
@property (nonatomic, strong) NSSet *themes;
@property (nonatomic, strong) Cell *predecesor;
@property (nonatomic, strong) NSSet *successors;

@property (nonatomic, strong) PSMesh *mesh;

@end

@interface Cell (CoreDataGeneratedAccessors)

- (void)addCellTimesObject:(CellTime *)value;
- (void)removeCellTimesObject:(CellTime *)value;
- (void)addCellTimes:(NSSet *)values;
- (void)removeCellTimes:(NSSet *)values;

- (void)addSearchesObject:(Search *)value;
- (void)removeSearchesObject:(Search *)value;
- (void)addSearches:(NSSet *)values;
- (void)removeSearches:(NSSet *)values;

- (void)addThemesObject:(ColorScheme *)value;
- (void)removeThemesObject:(ColorScheme *)value;
- (void)addThemes:(NSSet *)values;
- (void)removeThemes:(NSSet *)values;

- (void)addSuccessorsObject:(Cell *)value;
- (void)removeSuccessorsObject:(Cell *)value;
- (void)addSuccessors:(NSSet *)values;
- (void)removeSuccessors:(NSSet *)values;

@end
