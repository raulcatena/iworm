//
//  UISearchController.h
//  iWorm
//
//  Created by Raul Catena on 4/15/13.
//
//

#import <UIKit/UIKit.h>
#import "PSDataBaseUtility.h"
#import "GAITrackedViewController.h"
#import "PSSettingsSingleton.h"

@protocol SearchCellProtocol;

@class ColorScheme;
@class Search;

@interface SearchController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITextFieldDelegate, Database>{
    BOOL compoundClosed;
    
    PSDataBaseUtility *database;
    
    BOOL confirmGeneSearch;
}

@property (nonatomic, weak) id<SearchCellProtocol>delegate;
@property (nonatomic, strong) IBOutlet UITextField *searchField;
@property (nonatomic, strong) IBOutlet UISlider *fadeSlider;
@property (nonatomic, strong) NSString *previousSearchString;

@property (nonatomic, strong) IBOutlet UITableView *resultsTable;
@property (nonatomic, strong) IBOutlet UITableView *compoundTable;

@property (nonatomic, strong) UIPopoverController *popover;

@property (nonatomic,strong) NSArray *arrayOfSearchResults;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) IBOutlet UIButton *colorUpPanel;
@property (nonatomic, strong) IBOutlet UIButton *colorDownPanel;
@property (nonatomic, strong) IBOutlet UIButton *selectedButtonForColor;

@property (nonatomic, strong) IBOutlet UIButton *toogleListOfResults;
@property (nonatomic, strong) IBOutlet UIButton *toogleSearchOptions;
@property (nonatomic, strong) IBOutlet UIView *searchOptions;

@property (nonatomic, strong) ColorScheme *currentColorScheme;
@property (nonatomic, strong) Search *currentSearch;
@property (nonatomic, strong) IBOutlet UIView *subViewIPhone;

@property (nonatomic, weak) PSSettingsSingleton *searchSettings;

@property (nonatomic, strong) IBOutlet UISwitch *searchCellSwitch;
@property (weak, nonatomic) IBOutlet UILabel *searchCellLabel;
@property (nonatomic, strong) IBOutlet UISwitch *searchAncestorsSwitch;
@property (weak, nonatomic) IBOutlet UILabel *searchAncestorsLabel;
@property (nonatomic, strong) IBOutlet UISwitch *searchDescendantsSwitch;
@property (weak, nonatomic) IBOutlet UILabel *searchDescendantsLabel;
@property (nonatomic, strong) IBOutlet UISwitch *searchProperSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *searchSystSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *searchDescriptSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *searchGeneSwitch;

@property (nonatomic, strong) IBOutlet UISegmentedControl *iphoneOpts;

@property (nonatomic, strong) IBOutlet UIButton *plusButton;
@property (nonatomic, strong) IBOutlet UILabel *badge;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andDelegate:(id<SearchCellProtocol>)delegate;
-(IBAction)searchedCell:(UITextField *)sender;
-(IBAction)searchGene:(UITextField *)sender;
-(IBAction)switchChanged:(id)sender;
-(IBAction)fade:(UISlider *)sender;
-(IBAction)toggleCompoundSearch:(UIButton *)sender;
-(IBAction)toggleResultsList:(UIButton *)sender;
-(IBAction)toggleSearchOptions:(UIButton *)sender;
-(IBAction)addSearchToColorScheme:(UIButton *)sender;
-(IBAction)toogleIphoneTabs:(UISegmentedControl *)sender;
-(void)updateBadgeAndThumb;
-(IBAction)help;

@end

@protocol SearchCellProtocol <NSObject>

-(void)searchVC:(SearchController *)searchController didSearchForCell:(NSString *)searchString withFadeOthersValue:(float)newFadeValue;// andOptionsArray:(NSArray *)array;
-(void)searchVC:(SearchController *)searchController didGeneSearch:(Search *)search;
-(void)searchVC:(SearchController *)searchController createSearch:(NSString *)searchString withColor:(UIColor *)color;//  andOptionsArray:(NSArray *)array;
-(void)searchVC:(SearchController *)searchController didDissapearWithSearch:(NSString *)searchString withColor:(UIColor *)color;// andOptionsArray:(NSArray *)array;

//The options array will describe at position 0: propagation 1: search Proper 2:search systematic 3:search description will bool values
-(NSArray *)lastSearch;
-(UIImage *)viewBehind;

@end
