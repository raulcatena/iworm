//
//  PSDataBaseUtility.h
//  WormGUIDES
//
//  Created by Raul Catena on 5/30/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Cell;
@class Search;

@protocol Database <NSObject>

-(void)doneSearching;

@end

@interface PSDataBaseUtility : NSObject <UIAlertViewDelegate>

@property (nonatomic, assign) id<Database>delegate;
@property (nonatomic, strong) NSArray *allCells;

-(void)createDataBase;
-(void)emparentar;
-(void)addPartsListToDB;
//-(void)conditionalDatabaseForV1_7;
-(void)conditionalDatabaseForV3;

-(NSArray *)executeSearchWithString:(NSString *)searchString;
-(void)formatCell:(UITableViewCell *)cell withSearch:(Search *)search;
-(void)doGeneSearchWithTermToRule:(Search *)search withColor:(UIColor *)color;

@end
