//
//  LegendViewController.m
//  iWorm
//
//  Created by Raul Catena on 5/9/13.
//
//

#import "LegendViewController.h"
#import "ColorScheme.h"
#import "Search.h"
#import "PSDataBaseUtility.h"

@interface LegendViewController ()

@end

@implementation LegendViewController

@synthesize currentColorScheme = _currentColorScheme;
@synthesize severalSearches = _severalSearches;
@synthesize missingGeneSearch = _missingGeneSearch;
@synthesize delegate;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithStyle:(UITableViewStyle)style andCurrentColorScheme:(ColorScheme *)colorScheme andDimValue:(float)dimValue{
    self = [self initWithStyle:style];
    if (self) {
        self.currentColorScheme = colorScheme;
        
        if (dimValue <= 1.0) {
            UISlider *fadeSlider = [[UISlider alloc]initWithFrame:CGRectMake(70, 12, 170, 20)];
            UIImage *stetchLeftTrack = [[UIImage imageNamed:@"redslider.png"]
                                        stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
            [fadeSlider setMinimumTrackImage:stetchLeftTrack forState: UIControlStateNormal];
            fadeSlider.value = 0.8;
            //CGAffineTransform trans = CGAffineTransformMakeRotation(-M_PI * 0.5);
            //fadeSlider.transform = trans;
            self.dimOthersSlider = fadeSlider;
            self.dimOthersSlider.minimumValue = 0.0f;
            self.dimOthersSlider.maximumValue = 1.0f;
            self.dimOthersSlider.value = dimValue;
        }
    }
    return self;
}

-(void)adjustIOS7{
    if ([[[[[UIDevice currentDevice] systemVersion]
           componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self adjustIOS7];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor darkGrayColor];
    self.contentSizeForViewInPopover = CGSizeMake(300, 300);
    self.tableView.separatorColor = [UIColor clearColor];

    if (self.navigationController) {
        [self updateThumb];
        return;
    }
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = 0;
    [button addTarget:self action:@selector(pin:) forControlEvents:UIControlEventTouchUpInside];
    button.titleLabel.textColor = [UIColor whiteColor];
    [button setBackgroundImage:[UIImage imageNamed:self.popoverPresentationController?@"pin1.png":@"pin2.png"] forState:UIControlStateNormal];
    button.frame = CGRectMake(self.contentSizeForViewInPopover.width - 40, 5, 30, 30);
    [self.view addSubview:button];
    
    [self.dimOthersSlider addTarget:self action:@selector(newFade) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.tintColor = [UIColor whiteColor];
}

-(void)newFade{
    [self.delegate fadeValue:self.dimOthersSlider.value];
}

-(void)pin:(UIButton *)sender{
    if (sender.tag == 0) {
        sender.tag = 1;
        [self.delegate pin];
        [sender setBackgroundImage:[UIImage imageNamed:@"pin2.png"] forState:UIControlStateNormal];
        return;
    }
    [self.delegate unpin];
}

-(void)updateThumb{
    UIImage *image = [self.delegate viewBehind];
    UIImageView *iv = [[UIImageView alloc]initWithImage:image highlightedImage:image];
    iv.bounds = CGRectMake(0, 0, 50, 50);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:iv];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return self.currentColorScheme.searches.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Others";
        cell.detailTextLabel.text = nil;
        cell.textLabel.textColor = [UIColor colorWithWhite:0.9 alpha:1];
        [cell addSubview:self.dimOthersSlider];
    }else{
        for (UIView *subV in cell.subviews) {
            if ([subV isMemberOfClass:[UISlider class]]) {
                [subV removeFromSuperview];
            }
        }
        Search *search = (Search *)[self.currentColorScheme.searches.allObjects objectAtIndex:indexPath.row - 1];
        [[[PSDataBaseUtility alloc]init]formatCell:cell withSearch:search];
        
        if ([self.severalSearches containsObject:search]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    return cell;
}

-(NSMutableArray *)severalSearches{
    if (!_severalSearches) {
        self.severalSearches = [NSMutableArray arrayWithCapacity:self.currentColorScheme.searches.count];
    }
    return _severalSearches;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        return;
    }
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    Search *search = (Search *)[self.currentColorScheme.searches.allObjects objectAtIndex:indexPath.row -1];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        if ([self.severalSearches containsObject:search]) {
            [_severalSearches removeObject:search];
        }
    }else{
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        if (![self.severalSearches containsObject:search]) {
            [_severalSearches addObject:search];
        }
    }
    
    if (_severalSearches.count > 1) {NSLog(@"VARIAS");
        [self.delegate several:[NSArray arrayWithArray:self.severalSearches]];
        [self updateThumb];
        return;
    }
    
    [self.delegate searchSelected:[self.severalSearches lastObject]];
    [self updateThumb];
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    [[[UIAlertView alloc]initWithTitle:@"Gene search failed" message:@"Gene search requires to be online. This rule has not been successfully tested with online databases for C.elegans gene expression. Do you want to execute the search now" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil]show];
    Search *missing = (Search *)[self.currentColorScheme.searches.allObjects objectAtIndex:indexPath.row -1];
    self.missingGeneSearch = missing;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [[[PSDataBaseUtility alloc]init]doGeneSearchWithTermToRule:self.missingGeneSearch withColor:nil];
    }
}

@end
