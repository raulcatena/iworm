//
//  PSSceneGenerator.m
//  WormGUIDES
//
//  Created by Raul Catena on 10/7/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import "PSSceneGenerator.h"
#import "PSViewController.h"
#import "Search.h"
#import "ColorScheme.h"

@implementation PSSceneGenerator

+(NSString *)hexStringColorFromUnitaryString:(NSString *)colorString {
    NSArray *comps = [colorString componentsSeparatedByString:@","];
    NSString *result = [NSString stringWithFormat:@"%2x%2x%2x", (int)([[comps objectAtIndex:0]floatValue]*255), (int)([[comps objectAtIndex:1]floatValue]*255), (int)([[comps objectAtIndex:2]floatValue]*255)];
    result = [result stringByReplacingOccurrencesOfString:@" 0" withString:@"00"];
    if ([result isEqualToString:@"ffffff"]) {
        result = @"eeeeee";
    }
    return result;
    
}

+(NSString*)stringToHex:(NSString*)str
{
    NSData* nsData = [str dataUsingEncoding:NSUTF8StringEncoding];
    const char* data = [nsData bytes];
    NSUInteger len = nsData.length;
    NSMutableString* hex = [NSMutableString string];
    for(int i = 0; i < len; ++i)[hex appendFormat:@"%02X", data[i]];
    return hex;
}

+(NSString *)generateSceneForView:(PSViewController *)view withCount:(int)count distance:(float)distance rotation:(CGPoint)rotation translation:(CGPoint)translation andDimValue:(float)dimOthers{
    
    NSString *headeriOs = @"Use this link for the iOS app<br>";
    NSString *headeriAndroid = @"Use this link for the Android app<br>";
    
    NSString *iOS = @"wormguides://wormguides/testurlscript?/set/";//@"http://scene.wormguides.org/wormguides/testurlscript?/set/";
    if (FLY) {
        iOS = [iOS stringByReplacingOccurrencesOfString:@"wormguides" withString:@"flyguides"];
    }
    
    NSString *details = @"<p>";
    if (view.currentColorScheme.searches.allObjects.count > 0) {
        
        NSSortDescriptor *sortRating = [[NSSortDescriptor alloc] initWithKey:@"searchString" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        
        NSArray *descriptors = [NSArray arrayWithObjects:sortRating, nil];
        
        for (Search *search in [view.currentColorScheme.searches sortedArrayUsingDescriptors:descriptors]) {
            
            iOS = [iOS stringByAppendingString:[[search searchString]stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
            NSLog(@"search type %@ and search in %@", search.type, search.searchIn);
            if ([[search type] hasPrefix:@"1"] && [[search type] length]==3) {
                iOS = [iOS stringByAppendingString:@"-d"];
            }
            if (search.type.intValue == 110 || search.type.intValue == 111 || search.type.intValue == 11 || search.type.intValue == 10){
                iOS = [iOS stringByAppendingString:@"-s"];
            }
            if ([[search type] hasSuffix:@"1"]) {
                iOS = [iOS stringByAppendingString:@"-n"];
            }
            
            if ([[search type]isEqualToString:@"Gene"]) {
                iOS = [iOS stringByAppendingString:@"-g"];
            }
            
            NSMutableString *searchTypePrint = [NSMutableString stringWithFormat:@" "];
            if (search.searchIn.intValue > 99) {
                iOS = [iOS stringByAppendingString:@"$"];
                [searchTypePrint appendFormat:@"Cell,"];
            }
            if (search.searchIn.intValue == 111 || search.searchIn.intValue == 110 || search.searchIn.intValue == 11 || search.searchIn.intValue == 10) {
                iOS = [iOS stringByAppendingString:@"%3E"];
                [searchTypePrint appendFormat:@"Successors,"];
            }
            if (search.searchIn.intValue == 111 || search.searchIn.intValue == 101 || search.searchIn.intValue == 11 || search.searchIn.intValue == 1) {
                iOS = [iOS stringByAppendingString:@"%3C"];
                [searchTypePrint appendFormat:@"Ancestors,"];
            }
            
            
            [searchTypePrint deleteCharactersInRange:NSMakeRange(searchTypePrint.length-1, 1)];
            
            iOS = [iOS stringByAppendingString:@"+%23ff"];
            
            NSArray *colorElements = [[search color] componentsSeparatedByString:@","];
            int red = ([[colorElements objectAtIndex:0]floatValue] * 255);
            int green = ([[colorElements objectAtIndex:1]floatValue] * 255);
            int blue = ([[colorElements objectAtIndex:2]floatValue] * 255);
            iOS = [iOS stringByAppendingFormat:@"%02x%02x%02x/", red, green, blue];
            
            NSString *colorCode = [self hexStringColorFromUnitaryString:search.color];
            
            details = [details stringByAppendingString:[NSString stringWithFormat:@"<span style='background-color:#%@;'>&nbsp;</span> <u>%@</u>%@<br>", colorCode, search.searchString, searchTypePrint]];
        }
    }
    
    iOS = [iOS stringByAppendingFormat:@"view/"];
    iOS = [iOS stringByAppendingFormat:@"time=%i/", count-1<0?0:count-1];
    iOS = [iOS stringByAppendingFormat:@"rX=%f/",(float) rotation.x];
    iOS = [iOS stringByAppendingFormat:@"rY=%f/",(float) rotation.y];
    iOS = [iOS stringByAppendingFormat:@"rZ=%f/",(float) 0.0f];
    iOS = [iOS stringByAppendingFormat:@"tX=%f/",(float) 0.0f];
    iOS = [iOS stringByAppendingFormat:@"tY=%f/",(float) 0.0f];
    iOS = [iOS stringByAppendingFormat:@"scale=%f/",(float) (1+(distance/60.0f))];
    iOS = [iOS stringByAppendingFormat:@"dim=%f/",(float) dimOthers];
    
    iOS = [iOS stringByAppendingFormat:@"iOS/"];
    
    NSString *linkIOSBeggining = [NSString stringWithFormat:@"<a href='%@'>", iOS];
    
    //Here I remove the deambiguation flags introduced for specifying single cell search ($), systematic search (-s), or gene expression search (-g) in iOS, as they crash the url parsing in Android.
    NSString *android = [iOS copy];
    android = [android stringByReplacingOccurrencesOfString:@"-s" withString:@""];
    android = [android stringByReplacingOccurrencesOfString:@"$" withString:@""];
    android = [android stringByReplacingOccurrencesOfString:@"-g" withString:@""];
    android = [android stringByReplacingOccurrencesOfString:@"wormguides://" withString:@"http://scene.wormguides.org/"];
    android = [android stringByReplacingOccurrencesOfString:@"/iOS/" withString:@"/Android/"];
    
    NSString *linkIAndroidBeggining = [NSString stringWithFormat:@"<a href='%@'>", android];
    
//    NSString *browser = [android copy];
//    browser = [browser stringByReplacingOccurrencesOfString:@"/Android/" withString:@"/browser/"];
    
    
    //Compile message
    NSString *message = @"";
    message = [message stringByAppendingString: linkIOSBeggining];
    message = [message stringByAppendingString:headeriOs];
    message = [message stringByAppendingString:@"</a>"];
    message = [message stringByAppendingString:iOS];
    message = [message stringByAppendingString:@"<br><br>"];
    
    if (WORM) {
        message = [message stringByAppendingString: linkIAndroidBeggining];
        message = [message stringByAppendingString:headeriAndroid];
        message = [message stringByAppendingString:@"</a>"];
        message = [message stringByAppendingString:android];
        message = [message stringByAppendingString:@"<br><br>"];
        
    }
    
    
    message = [message stringByAppendingString:details];
        
    return message;
}

@end
