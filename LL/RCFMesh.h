//
//  RCFMesh.h
//  iWorm
//
//  Created by Raul Catena on 5/8/13.
//
//

#import <NinevehGL/NinevehGL.h>

@class CellTime;

@interface RCFMesh : NGLMesh

@property (nonatomic, retain) CellTime *cellTime;

@end
