//
//  PSVideoGenerator.h
//  WormGUIDES
//
//  Created by Raul Catena on 11/29/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>

@interface PSVideoGenerator : NSObject

@property (nonatomic, strong) AVAssetWriter *videoWriter;
@property (nonatomic, strong) AVAssetWriterInput* writerInput;
@property (nonatomic, strong) AVAssetWriterInputPixelBufferAdaptor *adaptor;

-(void)writeImageAsMovie:(NSArray *)array toPath:(NSString*)path size:(CGSize)size duration:(int)duration;
- (void)writeImagesAsMovie:(NSArray *)array;
//-(CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image;

@end
