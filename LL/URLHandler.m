//
//  URLHandler.m
//  WormGUIDES
//
//  Created by Raul Catena on 10/8/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import "URLHandler.h"
#import "ColorScheme.h"
#import "PSAppDelegate.h"
#import "Search.h"
#import "Cell.h"
#import "HTMLParser.h"
#import "PSSettingsSingleton.h"


@interface URLHandler()

-(NSManagedObjectContext *)managedObjectContext;
@property (nonatomic, strong) PSDataBaseUtility *database;

@end

@implementation URLHandler

@synthesize searchArray = _searchArray;
@synthesize viewArray = _viewArray;
@synthesize isIOS = _isIOS;
@synthesize database;

-(id)init{
    self = [super init];
    if(self){
        self.database = [[PSDataBaseUtility alloc]init];
    }
    return self;
}

-(NSManagedObjectContext *)managedObjectContext{
    PSAppDelegate *appDelegate = (PSAppDelegate *)[[UIApplication sharedApplication]delegate];
    return appDelegate.managedObjectContext;
}

-(BOOL)isValidURL:(NSURL *)url{
    if ([[url absoluteString]hasPrefix:@"wormguides://"] || [[url absoluteString]hasPrefix:@"http://scene.wormguides.org"] || [[url absoluteString]hasPrefix:@"flyguides://"]) {
        return YES;
    }
    return NO;
}

-(BOOL)decomposeURL:(NSURL *)url toURLHandler:(URLHandler *)handler{
    if (![self isValidURL:url]) {
        return NO;
    }
    
    //NSString *test = @"wormguides://wormguides/testurlscript?/set/ABa%3E+%23ffff0000/ABp%3E+%23ff00ff00/C%3E+%23ffffffff/caapap%3C%3E+%23ff7f007f/D%3E+%23ffb2b2bf/E%3E+%23ffffff00/MS%3E+%23ff0000ff/view/time=-1/rX=0.000000/rY=0.000000/rZ=0.000000/tX=0.000000/tY=0.000000/scale=1.000000/dim=0.800000/";
    NSString *path = [url absoluteString];//test;
    
    path = [path stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    path = [path stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    path = [path stringByReplacingOccurrencesOfString:@"%23" withString:@"#"];
    path = [path stringByReplacingOccurrencesOfString:@"%3C" withString:@"<"];
    path = [path stringByReplacingOccurrencesOfString:@"%3E" withString:@">"];
    path = [path stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    NSLog(@"2 %@", path);
    
    NSArray *segments = [path componentsSeparatedByString:@"/set/"];
    NSString *stringSegments = [segments lastObject];
    NSArray *subSegments = [stringSegments componentsSeparatedByString:@"/view/"];
    if (subSegments.count > 1) {
        NSString *setString = [subSegments objectAtIndex:0];
        NSString *viewString = [subSegments lastObject];
        handler.searchArray = [setString componentsSeparatedByString:@"/"];
        handler.viewArray = [viewString componentsSeparatedByString:@"/"];
        
        //This is a flag to check if URL is coming from iOS
        if ([path rangeOfString:@"iOS"].length > 0) {
            handler.isIOS = YES;
        }else{
            handler.isIOS = NO;
        }
        return YES;
    }else{
        return NO;
    }
}

float randomFloat(float Min, float Max){
    return ((arc4random()%RAND_MAX)/(RAND_MAX*1.0))*(Max-Min)+Min;
}

-(NSString *)stringColorFromRGBHexString:(NSString *)colorString {
    if (!colorString) {
        float r = arc4random() % 11 * 0.1;
        float g = arc4random() % 11 * 0.1;
        float b = arc4random() % 11 * 0.1;
        return [NSString stringWithFormat:@"%f,%f,%f,1.0", r, g, b];
    }
    if(colorString.length == 8) {
        const char *colorUTF8String = [colorString UTF8String];
        int a, r, g, b;
        sscanf(colorUTF8String, "%2x%2x%2x%2x", &a, &r, &g, &b);
        return [NSString stringWithFormat:@"%f,%f,%f,%f", (float)((float)r/255), (float)((float)g/255), (float)((float)b/255), (float)((float)a/255)];
    }
    return nil;
}

-(NSArray *)loadScene:(URLHandler *)aHandler{
    
    NSMutableArray *arrayToReturn = [NSMutableArray arrayWithCapacity:9];
    
    if (aHandler.viewArray) {NSLog(@"Array is %@ and count %lu", aHandler.viewArray, (unsigned long)aHandler.viewArray.count);
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        for (NSString *string in aHandler.viewArray) {
            if ([string isKindOfClass:[NSString class]]) {
                NSArray *comps = [string componentsSeparatedByString:@"="];
                if (comps.count == 2) {
                    [dictionary setObject:[comps lastObject] forKey:[comps objectAtIndex:0]];
                }
            }
        }
        [arrayToReturn addObject:[dictionary valueForKey:@"rX"]];
        [arrayToReturn addObject:[dictionary valueForKey:@"rY"]];
        [arrayToReturn addObject:@"0"];
        [arrayToReturn addObject:[dictionary valueForKey:@"scale"]];
        [arrayToReturn addObject:[dictionary valueForKey:@"time"]];
        [arrayToReturn addObject:[dictionary valueForKey:@"dim"]];
        [arrayToReturn addObject:[dictionary valueForKey:@"tX"]];
        [arrayToReturn addObject:[dictionary valueForKey:@"tY"]];
        aHandler.viewArray = nil;
    }
    if (aHandler.searchArray) {NSLog(@"Array is %@ and count %lu", aHandler.searchArray, (unsigned long)aHandler.searchArray.count);
        
        ColorScheme *colorScheme = [NSEntityDescription insertNewObjectForEntityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
        colorScheme.name = @"Shared scene ";
        colorScheme.isCurrent = [NSNumber numberWithBool:NO];//Will wait till the method is finished in PSViewController loadScene. Just in case it crashes, won't accumulate current searches. Bug fixed
        
        for (NSString *string in aHandler.searchArray) {
            
            NSArray *searchColor = [string componentsSeparatedByString:@"#"];
            Search *search = [NSEntityDescription insertNewObjectForEntityForName:@"Search" inManagedObjectContext:self.managedObjectContext];
            search.theme = colorScheme;
            [self.managedObjectContext save:nil];
            
            //Prepare the search term by removing all symbols
            NSString *searchStringClean = [searchColor objectAtIndex:0];
            searchStringClean = [searchStringClean stringByReplacingOccurrencesOfString:@"<" withString:@""];
            searchStringClean = [searchStringClean stringByReplacingOccurrencesOfString:@">" withString:@""];
            searchStringClean = [searchStringClean stringByReplacingOccurrencesOfString:@"+" withString:@""];
            searchStringClean = [searchStringClean stringByReplacingOccurrencesOfString:@"$" withString:@""];
            searchStringClean = [searchStringClean stringByReplacingOccurrencesOfString:@"#" withString:@""];
            searchStringClean = [searchStringClean stringByReplacingOccurrencesOfString:@"-d" withString:@""];
            searchStringClean = [searchStringClean stringByReplacingOccurrencesOfString:@"-s" withString:@""];
            searchStringClean = [searchStringClean stringByReplacingOccurrencesOfString:@"-n" withString:@""];
            searchStringClean = [searchStringClean stringByReplacingOccurrencesOfString:@";" withString:@""];
            search.searchString = searchStringClean;
            NSLog(@"Search sting is %@ from precur %@", searchStringClean, string);
            
            //Define the search type
            int searchType = 0;
            if ([string rangeOfString:@"<"].length > 0) {
                searchType = searchType + 1;
            }
            if ([string rangeOfString:@">"].length > 0) {
                searchType = searchType + 10;
            }
            if ([string rangeOfString:@"$"].length > 0) {
                searchType = searchType + 100;
            }
            //This will fix the absence of flag for Cell search in Android's URLs
            if (aHandler.isIOS == NO && searchType < 100) {
                searchType = searchType + 100;
            }
            
            search.searchIn = [NSString stringWithFormat:@"%i", searchType];
            
            //Defining the search containers
            int searchProper = (BOOL)[string rangeOfString:@"-n"].length;if (searchProper > 1)searchProper = 1;
            int searchSyst = (BOOL)[string rangeOfString:@"-s"].length;if (searchSyst > 1)searchSyst = 1;
            int searchDescript = (BOOL)[string rangeOfString:@"-d"].length;if (searchDescript > 1)searchDescript = 1;

            int total = (searchProper + searchSyst * 10 + searchDescript * 100);
            //Correct Android disambiguation issue
            if (aHandler.isIOS == NO && total != 110 && total != 10 && total != 111 && total != 11) {
                total = total + 10;
                searchSyst = 1;
            }
            
            
            
            search.type = [NSString stringWithFormat:@"%i", total];
            if (search.type.intValue == 0){search.type = @"10";searchSyst = 1;}
            
            //Override for gene search
            if ([string rangeOfString:@"-g"].length > 0) {
                search.searchIn = @"Gene";
                //search.type = @"100";
                search.searchString = [search.searchString stringByReplacingOccurrencesOfString:@"-g" withString:@""];
            }
            //For Android legacies
            if (search.searchString.length == 5) {
                NSArray *array = [search.searchString componentsSeparatedByString:@"-"];
                if (array.count > 1) {
                    NSArray * arr = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
                    
                    if ([[array objectAtIndex:0]length] == 3) {
                        for (NSString *str in arr) {
                            if ([search.searchString rangeOfString:str].location == 4) {
                                search.searchIn = @"Gene";
                            }
                        }
                    }
                }
            }
    
            //Defining the color, if there is no color defined (after breaking the array with #) Then I pass nil, the stringColorFromRGBHexStringFunction will return then a random color
            if (searchColor.count > 1) {
                search.color = [self stringColorFromRGBHexString:[searchColor lastObject]];
            }else{
                search.color = [self stringColorFromRGBHexString:nil];
            }
            
            //Setting search type
            [[PSSettingsSingleton sharedInstance]setSearchProper:searchProper];
            [[PSSettingsSingleton sharedInstance]setSearchSystematic:searchSyst];
            [[PSSettingsSingleton sharedInstance]setSearchDescription:searchDescript];
            
            if(search.searchIn.intValue == 1 || search.searchIn.intValue == 11 || search.searchIn.intValue == 111 || search.searchIn.intValue == 101)
                [[PSSettingsSingleton sharedInstance]setSearchAncestors:YES];
            else [[PSSettingsSingleton sharedInstance]setSearchAncestors:NO];
            
            if (search.searchIn.intValue == 110 || search.searchIn.intValue == 10 || search.searchIn.intValue == 111 || search.searchIn.intValue == 11)
                [[PSSettingsSingleton sharedInstance]setSearchDescendants:YES];
            else [[PSSettingsSingleton sharedInstance]setSearchDescendants:NO];
            
            if(search.searchIn.intValue > 99)[[PSSettingsSingleton sharedInstance]setSearchCell:YES];
            else [[PSSettingsSingleton sharedInstance]setSearchCell:NO];
            
            //Do seacrhes
            NSArray *foundCells = nil;
            if ([search.searchIn isEqualToString:@"Gene"]) {
                //Newwww
                if (!database) {
                    database = [[PSDataBaseUtility alloc]init];
                }
                [database doGeneSearchWithTermToRule:search withColor:nil];
                //foundCells = [self doGeneSearchWithSearch:search];
            }else{
                foundCells = [self.database executeSearchWithString:search.searchString];
            }
            //NSLog(@"Found %i", foundCells.count);
            
            for (Cell *cell in foundCells) {
                NSMutableSet *set = [NSMutableSet setWithSet:cell.searches];
                [set addObject:search];
                cell.searches = set;
            }
            NSLog(@"Search in %@ and type %@", search.searchIn, search.type);
            [self.managedObjectContext save:nil];
        }
        aHandler.searchArray = nil;
        [arrayToReturn addObject:colorScheme];
    }
    return [NSArray arrayWithArray:arrayToReturn];
}


@end
