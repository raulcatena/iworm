//
//  InfoViewcontrollerViewController.m
//  iWorm
//
//  Created by Raul Catena on 5/1/13.
//
//

#import "InfoViewcontrollerViewController.h"

@interface InfoViewcontrollerViewController ()

@end

@implementation InfoViewcontrollerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.contentSizeForViewInPopover = [[[UIApplication sharedApplication]delegate]window].bounds.size;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
