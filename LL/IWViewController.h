/*
 *	IWViewController.h
 *	iWorm
 *	
 *	Created by Raul Catena on 2/26/13.
 *	Copyright 2013 __MyCompanyName__. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import <NinevehGL/NinevehGL.h>
#import <MessageUI/MessageUI.h>
#import "SearchController.h"
#import "LineageTracing.h"

@class ColorScheme;

@interface IWViewController : UIViewController <NGLViewDelegate, NGLMeshDelegate, SearchCellProtocol, UIPopoverControllerDelegate, LineageDelegate, MFMailComposeViewControllerDelegate>
{
    //Declare private pointers (instance variables)
    //No getters or setters will be implemented for any of them in this class
@private
	NGLMesh *_mesh;
	NGLCamera *_camera;
    NGLGroup3D *_group;
    
    UIView *transf;
    IBOutlet UIView *transpar;
    
    float distance;
    CGPoint position;
    
    NSMutableArray *dataComponentsArray;
    NSArray *dataOfMObjectsInTime;
    NSArray *allMOObjects;
    int count;
    float dimOthers;
    
    UILabel *labelDay;
    UILabel *labelCell;
    
    UILabel *labelTouchedCell;
    
    BOOL respondingToTap;
    BOOL initial;
}

@property (nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) UIPopoverController *popover;
@property (nonatomic, retain) NSString *searchText;
@property (nonatomic, retain) NSArray *searchArray;

@property (nonatomic, retain) ColorScheme *currentColorScheme;
@property (nonatomic, retain) ColorScheme *nextColorScheme;
@property (nonatomic, retain) NSMutableArray *itemsInCurrentSearch;

@property (nonatomic, retain) IBOutlet NGLView *ngView;



@end
