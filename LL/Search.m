//
//  Search.m
//  iWorm
//
//  Created by Raul Catena on 5/18/13.
//
//

#import "Search.h"
#import "Cell.h"
#import "ColorScheme.h"


@implementation Search

@dynamic color;
@dynamic propagationType;
@dynamic searchString;
@dynamic type;
@dynamic searchIn;
@dynamic cells;
@dynamic theme;

@end
