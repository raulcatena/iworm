//
//  PSOptionsCellViewController.m
//  WormGUIDES
//
//  Created by Raul Catena on 6/6/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import "PSOptionsCellViewController.h"
#import "SearchController.h"

@interface PSOptionsCellViewController ()

-(void)showMore;
-(void)searchMore;

@end

@implementation PSOptionsCellViewController

@synthesize second;
@synthesize cell;
@synthesize delegate;
@synthesize popover;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)determineSize{
    if (self.second) {
        self.preferredContentSize = CGSizeMake(self.view.bounds.size.width, 350);
    }else{
        self.preferredContentSize = CGSizeMake(250, 1);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self determineSize];
    self.screenName = @"OPTIONS";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adjustIOS7{
    if ([[[[[UIDevice currentDevice] systemVersion]
           componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self adjustIOS7];
    
    if (self.second) {
        self.navigationItem.hidesBackButton = YES;
        float height = 0;
        if (self.cell.descriptionOfCell.length > 0 || self.cell.nameProper.length > 0) {NSLog(@"HERE");
            UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, self.contentSizeForViewInPopover.width, 100)];
            textView.font = [UIFont systemFontOfSize:15];
            textView.textAlignment= NSTextAlignmentCenter;
            textView.text = [NSString stringWithFormat:@"%@\n%@", self.cell.nameProper, self.cell.descriptionOfCell];
            //textView.textColor = [UIColor whiteColor];
            //textView.backgroundColor = [UIColor clearColor];
            [self.view addSubview:textView];
            textView.bounds = CGRectMake(0, 0, textView.bounds.size.width, MAX(textView.contentSize.height,100));
            height = textView.bounds.size.height;
        }
        NSArray *titles = [NSArray arrayWithObjects:@"WormBase", @"Google", @"Worm Atlas", @"Text-presso", nil];
        int x = 0;
        for(NSString *title in titles){
            UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            button.tag = x;
            [button addTarget:self action:@selector(visit:) forControlEvents:UIControlEventTouchUpInside];
            button.backgroundColor = [UIColor whiteColor];
            [button setTitle:title forState:UIControlStateNormal];
            [button setFrame:CGRectMake(0, height + x*40, self.contentSizeForViewInPopover.width, 40)];
            [self.view addSubview:button];
            x++;
        }
        self.view.bounds = CGRectMake(0, 0, self.contentSizeForViewInPopover.width, height + 13 + x*40);
        self.preferredContentSize = self.view.bounds.size;
    }
    [self determineSize];
}


-(void)visit:(UIButton *)sender{
    NSString *string = nil;
    if (sender.tag == 0) {
        string = [NSString stringWithFormat:@"http://www.wormbase.org/db/get?name=%@;class=Anatomy_term", self.title];
    }else if (sender.tag == 1){
        string = [NSString stringWithFormat:@"http://www.google.com/search?q=%@+c.elegans", self.title];
    }else if (sender.tag == 2){
        string = [NSString stringWithFormat:@"http://wormatlas.org/search_results.html?cx=016220512202578422943%%3Amikvfhp2nri&cof=FORID%%3A10&ie=UTF-8&q=%@&siteurl=wormatlas.org%%252F", self.title];
    }else if (sender.tag == 3){
        string = [NSString stringWithFormat:@"http://www.textpresso.org/cgi-bin/celegans/query?textstring=%@", self.title];
    }
    NSString *noSpace = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    UIWebView *webView = [[UIWebView alloc]init];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:noSpace]]];
    UIViewController *viewCont = [[UIViewController alloc]init];
    viewCont.contentSizeForViewInPopover = viewCont.view.frame.size;
    [viewCont.view addSubview:webView];
    viewCont.title = [NSString stringWithFormat:@"%@ | %@", self.title, sender.titleLabel.text];
    webView.frame = viewCont.view.frame;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        [self.navigationController pushViewController:viewCont animated:YES];
    }else{
        [self presentViewController:[[UINavigationController alloc]initWithRootViewController:viewCont] animated:YES completion:^{
            viewCont.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(cancel)];
        }];
    }
    /*
    self.popover = [[UIPopoverController alloc]initWithContentViewController:viewCont];
    [popover presentPopoverFromRect:CGRectMake(0, 0, 0, 0) inView:self.view.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    NSLog(@"url is %@", string);*/
}

-(void)cancel{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)showMore{
    PSOptionsCellViewController *other = [[PSOptionsCellViewController alloc]init];
    other.view.frame = CGRectMake(0, 0, 300, 300);
    other.contentSizeForViewInPopover = other.view.frame.size;
    other.title = self.title;
    other.cell = self.cell;
    other.second = YES;
    other.navigationItem.leftBarButtonItem = self.navigationItem.leftBarButtonItem;
    [self.navigationController pushViewController:other animated:YES];
}

-(void)searchMore{
    [self.delegate optionsClickedSearchCell:self];
}

@end
