//
//  ColorScheme.h
//  iWorm
//
//  Created by Raul Catena on 5/1/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cell;

@interface ColorScheme : NSManagedObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *isCurrent;
@property (nonatomic, strong) NSNumber *saved;
@property (nonatomic, strong) NSSet *searches;
@property (nonatomic, strong) NSSet *cells;
@end

@interface ColorScheme (CoreDataGeneratedAccessors)

- (void)addSearchesObject:(NSManagedObject *)value;
- (void)removeSearchesObject:(NSManagedObject *)value;
- (void)addSearches:(NSSet *)values;
- (void)removeSearches:(NSSet *)values;

- (void)addCellsObject:(Cell *)value;
- (void)removeCellsObject:(Cell *)value;
- (void)addCells:(NSSet *)values;
- (void)removeCells:(NSSet *)values;

@end
