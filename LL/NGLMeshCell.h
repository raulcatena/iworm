//
//  NGLMeshCell.h
//  iWorm
//
//  Created by Raul Catena on 4/27/13.
//
//

#import <NinevehGL/NinevehGL.h>
#import "Cell.h"

@interface NGLMeshCell : NGLMesh

@property (nonatomic, retain) Cell *cell;

-(id)initWithFile:(NSString *)named settings:(NSDictionary *)dict delegate:(id<NGLMeshDelegate>)target andCell:(Cell *)cell;

@end
