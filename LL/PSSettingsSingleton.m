//
//  PSSettingsSingleton.m
//  WormGUIDES
//
//  Created by Raul Catena on 9/5/14.
//  Copyright (c) 2014 Raul Catena. All rights reserved.
//

#import "PSSettingsSingleton.h"

@implementation PSSettingsSingleton

+(PSSettingsSingleton *)sharedInstance{
    static dispatch_once_t pred = 0;
    __strong static PSSettingsSingleton* _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; // or some other init method
    });
    return _sharedObject;
}

-(void)searchAllCells{
    _searchCell = YES;
    _searchAncestors = YES;
    _searchDescendants = YES;
}

-(void)setSearchGene:(BOOL)searchGene{
    _searchGene = searchGene;
    if (searchGene) {
        if (_searchSystematic || _searchProper || _searchDescription) {
            _searchSystematic = NO;
            _searchProper = NO;
            _searchDescription = NO;
        }
    }
}

-(void)setSearchSystematic:(BOOL)searchSystematic{
    _searchSystematic = searchSystematic;
    if (searchSystematic) {
        if (_searchGene) {
            [self searchAllCells];
        }
        _searchGene = NO;
    }
}

-(void)setSearchProper:(BOOL)searchProper{
    _searchProper = searchProper;
    if (searchProper) {
        if (_searchGene) {
            [self searchAllCells];
        }
        _searchGene = NO;
    }
}

-(void)setSearchDescription:(BOOL)searchDescription{
    _searchDescription = searchDescription;
    if (searchDescription) {
        if (_searchGene) {
            [self searchAllCells];
        }
        _searchGene = NO;
    }
}

-(void)setSearchCell:(BOOL)searchCell{
    _searchCell = searchCell;

}

-(void)setSearchDescendants:(BOOL)searchDescendants{
    _searchDescendants = searchDescendants;

}

-(void)setSearchAncestors:(BOOL)searchAncestors{
    _searchAncestors = searchAncestors;

}

-(BOOL)areOptionsValid{
    if (!_searchCell && !_searchDescendants && !_searchAncestors) {
        return NO;
    }
    if (!_searchSystematic && !_searchProper && !_searchDescription && !_searchGene) {
        [[[UIAlertView alloc]initWithTitle:@"You must turn on at least one search option" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        return NO;
    }
    return YES;
}

-(int)propagationType{
    NSString *propagationType = [NSString stringWithFormat:@"%i", ((int)_searchCell * 100 + (int)_searchDescendants * 10 + (int)_searchAncestors)];
    return  propagationType.intValue;
}

-(int)typeOfSearch{
    NSString *propagationType = [NSString stringWithFormat:@"%i", ((int)_searchDescription * 100 + (int)_searchSystematic * 10 + (int)_searchProper)];
    return  propagationType.intValue;
}

-(void)defaults{
    if (WORM) {
        _searchSystematic = YES;
        _searchProper = NO;
        _searchDescription = NO;
        _searchGene = NO;
        _searchCell = YES;
        _searchAncestors = NO;
        _searchDescendants = YES;
    }
    if (FLY) {
        _searchSystematic = YES;
        _searchProper = YES;
        _searchDescription = NO;
        _searchGene = NO;
        _searchCell = YES;
        _searchAncestors = NO;
        _searchDescendants = NO;
    }
}

@end
