//
//  PSSceneGenerator.h
//  WormGUIDES
//
//  Created by Raul Catena on 10/7/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PSViewController;

@interface PSSceneGenerator : NSObject

+(NSString *)generateSceneForView:(PSViewController *)view withCount:(int)count distance:(float)distance rotation:(CGPoint)rotation translation:(CGPoint)translation andDimValue:(float)dimOthers;

@end
