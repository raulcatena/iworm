//
//  ColorScheme.m
//  iWorm
//
//  Created by Raul Catena on 5/1/13.
//
//

#import "ColorScheme.h"
#import "Cell.h"


@implementation ColorScheme

@dynamic name;
@dynamic isCurrent;
@dynamic saved;
@dynamic searches;
@dynamic cells;

@end
