//
//  Shader.fsh
//  LL
//
//  Created by Raul Catena on 5/26/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
