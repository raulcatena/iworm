//
//  URLHandler.h
//  WormGUIDES
//
//  Created by Raul Catena on 10/8/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSDataBaseUtility.h"

@class Search;

@interface URLHandler : NSObject <UIAlertViewDelegate>{
    PSDataBaseUtility *database;
}

@property (nonatomic, strong) NSArray *searchArray;
@property (nonatomic, strong) NSArray *viewArray;
@property (nonatomic, assign) BOOL isIOS;

-(BOOL)isValidURL:(NSURL *)url;
-(BOOL)decomposeURL:(NSURL *)url toURLHandler:(URLHandler *)handler;
//Returns an array with RotationX, RotationY, RotationZ, Distance, Time for slides, Dim and ColorScheme
-(NSArray *)loadScene:(URLHandler *)aHandler;

@end
