//
//  LineageTracing.h
//  iWorm
//
//  Created by Raul Catena on 4/15/13.
//
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "GAITrackedViewController.h"

@protocol LineageDelegate;
@class ColorScheme;

@interface LineageTracing : GAITrackedViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) NSDictionary *structureDictionary;
@property (nonatomic, strong) IBOutlet UILabel *nameOfCurrent;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) ColorScheme *currentColorScheme;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIButton *save;
@property (nonatomic, strong) IBOutlet UIButton *newwBlankBut;
@property (nonatomic, strong) IBOutlet UIButton *newwLineageBut;

@property (nonatomic, weak) id<LineageDelegate>delegate;

-(IBAction)newButtonPressed:(UIButton *)sender;

@end

@protocol LineageDelegate <NSObject>

-(void)lineageVC:(LineageTracing *)lineageTVC didChooseTheme:(ColorScheme *)theme;
-(void)lineageVC:(LineageTracing *)lineageTVC didSaveCurrentWithName:(NSString *)name;
-(void)lineageVC:(LineageTracing *)lineageTVC didSaveAsWithName:(NSString *)name;
-(void)lineageVC:(LineageTracing *)lineageTVC create:(int)schemeType WithName:(NSString *)name;

@end
