//
//  LineageTracing.m
//  iWorm
//
//  Created by Raul Catena on 4/15/13.
//
//

#import "LineageTracing.h"
#import "LegendViewController.h"
#import "PSAppDelegate.h"
#import "ColorScheme.h"

#define WIDTH_LINEAGE 300
#define HEIGHT_LINEAGE 700

@interface LineageTracing ()

@end

@implementation LineageTracing

@synthesize structureDictionary;
@synthesize nameOfCurrent = _nameOfCurrent;
@synthesize currentColorScheme = _currentColorScheme;
@synthesize tableView = _tableView;
@synthesize save = _save;
@synthesize newwBlankBut = _newwBlankBut;
@synthesize newwLineageBut = _newwLineageBut;


-(void)viewDidUnload{
    [super viewDidUnload];
    self.tableView = nil;
    self.nameOfCurrent = nil;
    self.save = nil;
    self.delegate = nil;
    self.fetchedResultsController.delegate = nil;
    self.newwBlankBut = nil;
    self.newwLineageBut = nil;
}

-(NSManagedObjectContext *)managedObjectContext{
    PSAppDelegate *appDelegate = (PSAppDelegate *)[[UIApplication sharedApplication]delegate];
    return appDelegate.managedObjectContext;
}

#pragma mark FRC

-(NSFetchedResultsController *)fetchedResultsController{
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ColorScheme" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isCurrent = NO"];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        [NSFetchedResultsController deleteCacheWithName:@"Themes"];
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Themes"];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
        
    }
    return _fetchedResultsController;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super init];
    if (self) {
        self.view.frame = CGRectMake(0, 0, WIDTH_LINEAGE, HEIGHT_LINEAGE);
        self.contentSizeForViewInPopover = CGSizeMake(WIDTH_LINEAGE, HEIGHT_LINEAGE);
    }
    return self;
}

-(void)adjustIOS7{
    if ([[[[[UIDevice currentDevice] systemVersion]
           componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self adjustIOS7];
    self.title = @"Themes";

    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Error %@ with infoOfError %@", error, error.userInfo);
        abort();
    }
    
    if (self.currentColorScheme) {
        _nameOfCurrent.text = self.currentColorScheme.name;
    }
    self.screenName = @"Lineage tracing";
    
    _tableView.tintColor = [UIColor whiteColor];
}

/*-(void)calculateSizeRoutine{
    //if (self.tableA.bounds.size.height + self.tableView.contentSize.height + 200 > 800) {NSLog(@"Retuners");
    //    return;
    //}
    self.contentSizeForViewInPopover = CGSizeMake(self.view.bounds.size.width, self.tableA.bounds.size.height + self.tableView.contentSize.height + 200);
    //self.tableView.frame = CGRectMake(0, self.tableA.bounds.size.height, self.view.bounds.size.width, self.tableView.contentSize.height);
    [self.view setNeedsLayout];
}*/

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    NSArray *butTitles;
    if (WORM) {
        butTitles = @[@"Blank...", @"Lineage...", @"Duplicate..."];
    }
    if (FLY) {
        butTitles = @[@"Blank...", @"Cell type...", @"Duplicate..."];
    }
    NSArray *buttons = @[self.newwBlankBut, self.newwLineageBut, self.save];
    for(int x = 0; x<buttons.count; x++){
        UIButton *button = [buttons objectAtIndex:x];
        [button setTitle:[butTitles objectAtIndex:x] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];//colorWithRed:35.0f/255.0f green:121.0f/255.0f blue:250.0f/255.0f alpha:1] forState:UIControlStateNormal];
         button.layer.borderColor = [UIColor whiteColor].CGColor;//colorWithRed:35.0f/255.0f green:121.0f/255.0f blue:250.0f/255.0f alpha:1].CGColor;
        button.layer.borderWidth = 1.0f;
        button.layer.cornerRadius = 5.0f;
        switch (x){
            case 0:
                [button addTarget:self action:@selector(newButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case 1:
                [button addTarget:self action:@selector(newButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case 2:
                [button addTarget:self action:@selector(saveCurrent) forControlEvents:UIControlEventTouchUpInside];
                break;
            default:
                break;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    int rows = 0;
    
    rows = (int)[_fetchedResultsController fetchedObjects].count;
    if(rows == 0)rows = 1;
    return rows;
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    return @"Saved Themes";
//}

- (UITableViewCell *)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *kCellID = @"cellID";
	
	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellID];
	if (cell == nil)
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellID];
	}
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    ColorScheme *colorScheme = [_fetchedResultsController objectAtIndexPath:indexPath];
    NSLog(@"Name %@", colorScheme.name);
    cell.textLabel.text = colorScheme.name;
    if (colorScheme.isCurrent.boolValue == YES) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.textLabel.textColor = [UIColor colorWithRed:0.2 green:0.7 blue:0.2 alpha:1];
    }else{
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self tableView:self.tableView cellForRowAtIndexPath:indexPath];//[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}


/*-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}*/


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        ColorScheme *theme = [_fetchedResultsController objectAtIndexPath:indexPath];
        if (theme.isCurrent.boolValue == YES) {
            return;
        }
        [self.managedObjectContext deleteObject:theme];
        [self.managedObjectContext save:nil];
        [self.tableView reloadData];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

#pragma mark - Table view delegate

-(void)newButtonPressed:(UIButton *)sender{
    [self promptForNewTheme:(int)sender.tag];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ColorScheme *theme = [_fetchedResultsController objectAtIndexPath:indexPath];
    if (theme.isCurrent.boolValue == YES) return;
    self.nameOfCurrent.text = theme.name;
    [self.delegate lineageVC:self didChooseTheme:theme];
    
    [self.tableView reloadData];
}

-(void)promptForNewTheme:(int)schemeType{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"New theme" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    if (schemeType == 0) {
        alert.tag = 100;
    }else if(schemeType == 1){
        alert.tag = 101;
    }
    [alert show];
}

-(void)saveCurrent{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Save theme as" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (alertView.tag > 99) {
        if (buttonIndex == 1) {
            self.nameOfCurrent.text = [alertView textFieldAtIndex:0].text;
            [self.delegate lineageVC:self create:(int)alertView.tag - 100 WithName:_nameOfCurrent.text];
            
            [[[UIAlertView alloc]initWithTitle:@"Theme saved" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
            [self.tableView reloadData];

        }
        return;
    }
    if (buttonIndex == 1) {
        //[self.delegate lineageVC:self didSaveCurrentWithName:[alertView textFieldAtIndex:0].text];
        [self.delegate lineageVC:self didSaveAsWithName:[alertView textFieldAtIndex:0].text];
        _nameOfCurrent.text = [alertView textFieldAtIndex:0].text;
        [self.tableView reloadData];
        [[[UIAlertView alloc]initWithTitle:@"Theme saved" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 15)];
    view.backgroundColor = [UIColor clearColor];//colorWithRed:0.2 green:0.2 blue:0.8 alpha:1];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13, 3, 200, 20)];
    label.text = @"Saved Themes";
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];//colorWithRed:35.0f/255.0f green:121.0f/255.0f blue:250.0f/255.0f alpha:1];
    [view addSubview:label];
    return view;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    ColorScheme *theme = [_fetchedResultsController objectAtIndexPath:indexPath];
    LegendViewController *legendVC = [[LegendViewController alloc]initWithStyle:UITableViewStylePlain andCurrentColorScheme:theme andDimValue:2.0];
    [self.navigationController pushViewController:legendVC animated:YES];
}

@end
