//
//  Time.h
//  iWorm
//
//  Created by Raul Catena on 5/1/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CellTime;

@interface TimeWorm : NSManagedObject

@property (nonatomic, strong) NSNumber * timePoint;
@property (nonatomic, strong) NSString * timePoint_str;
@property (nonatomic, strong) NSSet *cellTimes;
@end

@interface TimeWorm (CoreDataGeneratedAccessors)

- (void)addCellTimesObject:(CellTime *)value;
- (void)removeCellTimesObject:(CellTime *)value;
- (void)addCellTimes:(NSSet *)values;
- (void)removeCellTimes:(NSSet *)values;

@end
