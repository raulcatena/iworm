/*
 *	IWAppDelegate.h
 *	iWorm
 *	
 *	Created by Raul Catena on 2/26/13.
 *	Copyright 2013 __MyCompanyName__. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import "IWViewController.h"
#import <NinevehGL/NinevehGL.h>

@interface IWAppDelegate : UIResponder <UIApplicationDelegate, UITabBarDelegate>
{
	UIWindow *_window;
	IWViewController *_viewController;
    
@private
    NSManagedObjectContext *managedObjectContext_;
    NSManagedObjectModel *managedObjectModel_;
    NSPersistentStoreCoordinator *persistentStoreCoordinator_;
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) IWViewController *viewController;

@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

