//
//  main.m
//  LL
//
//  Created by Raul Catena on 5/26/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PSAppDelegate class]));
    }
}
