//
//  PSViewController.h
//  LL
//
//  Created by Raul Catena on 5/26/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "SearchController.h"
#import "LineageTracing.h"
#import "LegendViewController.h"
#import <MessageUI/MessageUI.h>
#import "PSSettingsSingleton.h"
#import "PSOptionsCellViewController.h"
#import "URLHandler.h"

@class AGLKVertexAttribArrayBuffer;
@class ColorScheme;
@class PSVideoGenerator;

@interface PSViewController : GLKViewController <LineageDelegate, SearchCellProtocol, UIPopoverControllerDelegate, MFMailComposeViewControllerDelegate, UIGestureRecognizerDelegate, UIActionSheetDelegate, LegendProtocol, PanelProtocol, UIAlertViewDelegate>{
    
    PSVideoGenerator *videoGen;
    
}

@property (strong, nonatomic) AGLKVertexAttribArrayBuffer
*vertexPositionBuffer;
@property (strong, nonatomic) AGLKVertexAttribArrayBuffer
*vertexNormalBuffer;
@property (strong, nonatomic) AGLKVertexAttribArrayBuffer
*vertexTextureCoordBuffer;

@property (strong, nonatomic) NSArray * points;//These are the strings for a cell from the raw file for each time point
@property (nonatomic, strong) NSFetchRequest *loaderRequest;

//From iWorm Nineveh
@property (weak, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) NSString *searchText;

@property (nonatomic, strong) ColorScheme *currentColorScheme;
@property (nonatomic, strong) NSArray *severalSearches;
@property (nonatomic, strong) NSMutableArray *itemsInCurrentSearch;

//New
@property (nonatomic, strong) NSMutableArray *_group;//To hold the meshes
@property (nonatomic, strong) IBOutlet UISlider *sliderTime;//To hold the meshes
@property (nonatomic, strong) IBOutlet UILabel *nuclei;//To hold the meshes
@property (nonatomic, strong) IBOutlet UILabel *timePointLabel;//To hold the meshes
@property (nonatomic, strong) IBOutlet UIToolbar *toolbar;//To hold the meshes

@property (nonatomic, strong) Search *individualSearch;

//SearchSettings
@property (nonatomic, weak) PSSettingsSingleton *searchSettings;

@property (nonatomic, strong) LegendViewController *legend;

@property (nonatomic, strong) URLHandler *urlHandler;
@property (nonatomic, strong) PSMesh *selectedMesh;

@property (nonatomic, strong) UIView *wheelerView;


-(IBAction)playMovie:(UIBarButtonItem *)sender;
-(IBAction)forward:(id)sender;
-(IBAction)back:(id)sender;
-(void)interactionBySlider:(id)sender;
-(void)finishedInteractionBySlider:(id)sender;
-(void)loadScene:(URLHandler *)aHandler;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andURLHandler:(URLHandler *)handler;

@end
