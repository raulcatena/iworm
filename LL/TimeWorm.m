//
//  Time.m
//  iWorm
//
//  Created by Raul Catena on 5/1/13.
//
//

#import "TimeWorm.h"
#import "CellTime.h"


@implementation TimeWorm

@dynamic timePoint;
@dynamic timePoint_str;
@dynamic cellTimes;

@end
