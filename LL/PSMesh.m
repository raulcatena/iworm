//
//  PSMesh.m
//  WormGUIDES
//
//  Created by Raul Catena on 5/30/13.
//  Copyright (c) 2013 Raul Catena. All rights reserved.
//

#import "PSMesh.h"
#import "CellTime.h"


@implementation PSMesh

@synthesize hidden, active, posible, foundInSearch;
@synthesize cellTime;
@synthesize x, y, z, scaleX, scaleY, scaleZ;


@end
